import colors from 'vuetify/es5/util/colors'

export default {
  ssr: false,
  target: 'server',
  env: {
    appName: process.env.npm_package_name,
  },

  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s',
    title: 'CC-QTL @ Pasteur',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        defer: true, "data-domain": "cc-qtl.pasteur.cloud", src: "https://plausible.pasteur.cloud/js/script.js"
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: colors.amber.darken3, height: '4px' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {
      src: '~/plugins/highcharts.js',
      ssr: true,
    },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    // baseURL: process.env.BASE_URL || 'http://localhost:8889'
    proxy: true,
  },
  proxy: {
    '/api': process.env.BASE_URL || 'http://localhost:8889',
  },
  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/projects',
    },
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'access',
          maxAge: 15,
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'refresh',
          data: 'refresh',
          maxAge: false,
        },
        user: {
          property: false,
          autoFetch: true,
        },
        clientId: false,
        grantType: false,
        endpoints: {
          login: { url: '/api/token/', method: 'post' },
          refresh: { url: '/api/token/refresh/', method: 'post' },
          user: { url: '/api/current-user/', method: 'get' },
          logout: false,
        },
      },
    },
  },
  router: {
    middleware: ['auth'],
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      options: {
        customProperties: true,
      },
      dark: false,
      themes: {
        dark: {
          //     primary: colors.blue.darken2,
          //     accent: colors.grey.darken3,
          //     secondary: colors.amber.darken3,
          //     info: colors.teal.lighten1,
          //     warning: colors.amber.base,
          //     error: colors.deepOrange.accent4,
          //     success: colors.green.darken3,
        },
        light: {
          //     primary: '#1976D2',
          secondary: {
            base: '#424242',
            lighten6: colors.grey.lighten3,
            lighten5: '#c1c1c1',
            lighten4: '#a6a6a6',
            lighten3: '#8b8b8b',
            lighten2: '#727272',
            lighten1: '#595959',
            darken1: '#2c2c2c',
            darken2: '#171717',
            darken3: '#000000',
            darken4: '#000000',
          },
          //     accent: '#82B1FF',
          //     error: '#FF5252',
          //     info: '#2196F3',
          //     success: '#4CAF50',
          //     warning: '#FFC107',
        },
      },
    },
  },

  /*
   ** Build configuration
   */
  build: {
    // Add exception
    transpile: ['vee-validate/dist/rules'],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) { },
  },
}
