import Vue from 'vue'
import Highcharts from 'highcharts'
import Histogram from 'highcharts/modules/histogram-bellcurve'
import exportingInit from 'highcharts/modules/exporting'
import offlineExportingInit from 'highcharts/modules/offline-exporting'
import exportDataInit from 'highcharts/modules/export-data'
import boostInit from 'highcharts/modules/boost'
import More from 'highcharts/highcharts-more'
import HighchartsVue from 'highcharts-vue'

Histogram(Highcharts)
exportingInit(Highcharts)
offlineExportingInit(Highcharts)
exportDataInit(Highcharts)
boostInit(Highcharts)
More(Highcharts)
Vue.use(HighchartsVue)
