import { mount, createLocalVue } from '@vue/test-utils'
import ErrorAlert from '@/components/ErrorAlert'
import Vuetify from 'vuetify'

const localVue = createLocalVue()

describe('ErrorAlert', () => {
  let vuetify
  beforeEach(() => {
    vuetify = new Vuetify()
  })
  test('is a Vue instance', () => {
    const wrapper = mount(ErrorAlert, {
      localVue,
      vuetify,
      propsData: {
        errorMessage: 'message',
      },
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
