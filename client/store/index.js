function emptyExperimentToAdd() {
  return {
    projectId: null,
    experiment: {
      name: '',
      description: '',
      file: null,
    },
  }
}

export const state = () => ({
  theme: {
    darkIcon: 'mdi-weather-night',
    lightIcon: 'mdi-white-balance-sunny',
  },
  projects: { count: 0 },
  analysis: { count: 0 },
  experiments: { count: 0 },
  emptyExperimentToAdd: {
    projectId: null,
    experiment: {
      name: '',
      description: '',
      file: null,
    },
  },
  experimentToAdd: {},
})

export const getters = {
  getExperimentToAdd: (state) => (id) => {
    return state.experimentToAdd[id]
  },
}
export const mutations = {
  projectsCount(state, count) {
    state.projects.count = count
  },
  analysisCount(state, count) {
    state.analysis.count = count
  },

  // experiments
  experimentsCount(state, count) {
    state.experiments.count = count
  },
  initExperimentToAdd(state, projectId) {
    state.experimentToAdd[projectId] = emptyExperimentToAdd()
  },
  updateExperiment(state, { experiment, projectId }) {
    state.experimentToAdd[projectId] = experiment
  },
  clearExperiment(state, projectId) {
    state.experimentToAdd[projectId] = emptyExperimentToAdd()
  },
}

export const actions = {
  async updateProjectsCount({ commit }) {
    const { count } = await this.$axios.$get('/api/projects/count/')
    commit('projectsCount', count)
  },
  async updateAnalysisCount({ commit }) {
    const { count } = await this.$axios.$get('/api/analysis/count/')
    commit('analysisCount', count)
  },
  async updateExperimentsCount({ commit }) {
    const { count } = await this.$axios.$get('/api/experiments/count/')
    commit('experimentsCount', count)
  },
}
