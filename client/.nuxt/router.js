import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _5ef0bca8 = () => interopDefault(import('../pages/analysis.vue' /* webpackChunkName: "pages/analysis" */))
const _11140676 = () => interopDefault(import('../pages/experiments.vue' /* webpackChunkName: "pages/experiments" */))
const _2ff6773a = () => interopDefault(import('../pages/inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _57062649 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _2f3c4d9e = () => interopDefault(import('../pages/phenotype-categories.vue' /* webpackChunkName: "pages/phenotype-categories" */))
const _908e1218 = () => interopDefault(import('../pages/phenotype-categories/index.vue' /* webpackChunkName: "pages/phenotype-categories/index" */))
const _8ef73c7a = () => interopDefault(import('../pages/phenotype-categories/add.vue' /* webpackChunkName: "pages/phenotype-categories/add" */))
const _81ca4644 = () => interopDefault(import('../pages/phenotypes/index.vue' /* webpackChunkName: "pages/phenotypes/index" */))
const _71c8884a = () => interopDefault(import('../pages/projects.vue' /* webpackChunkName: "pages/projects" */))
const _acc7a166 = () => interopDefault(import('../pages/projects/index.vue' /* webpackChunkName: "pages/projects/index" */))
const _7b484748 = () => interopDefault(import('../pages/projects/add.vue' /* webpackChunkName: "pages/projects/add" */))
const _6117a0f5 = () => interopDefault(import('../pages/projects/_id.vue' /* webpackChunkName: "pages/projects/_id" */))
const _e5fa4890 = () => interopDefault(import('../pages/projects/_id/index.vue' /* webpackChunkName: "pages/projects/_id/index" */))
const _26a13366 = () => interopDefault(import('../pages/projects/_id/analysis.vue' /* webpackChunkName: "pages/projects/_id/analysis" */))
const _2191d52e = () => interopDefault(import('../pages/projects/_id/analysis/index.vue' /* webpackChunkName: "pages/projects/_id/analysis/index" */))
const _931ccd10 = () => interopDefault(import('../pages/projects/_id/analysis/add.vue' /* webpackChunkName: "pages/projects/_id/analysis/add" */))
const _ce15abe6 = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId" */))
const _53299a60 = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId/index.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId/index" */))
const _3491ea67 = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId/copy.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId/copy" */))
const _aeb8d71c = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId/peaks.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId/peaks" */))
const _1ef77d75 = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId/peaks/index.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId/peaks/index" */))
const _6e946448 = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId/peaks/_peakId.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId/peaks/_peakId" */))
const _d7e10142 = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId/peaks/_peakId/index.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId/peaks/_peakId/index" */))
const _8f5adb18 = () => interopDefault(import('../pages/projects/_id/analysis/_analysisId/results.vue' /* webpackChunkName: "pages/projects/_id/analysis/_analysisId/results" */))
const _cacd3b08 = () => interopDefault(import('../pages/projects/_id/experiments.vue' /* webpackChunkName: "pages/projects/_id/experiments" */))
const _59d91bff = () => interopDefault(import('../pages/projects/_id/experiments/index.vue' /* webpackChunkName: "pages/projects/_id/experiments/index" */))
const _05f04d8e = () => interopDefault(import('../pages/projects/_id/experiments/add.vue' /* webpackChunkName: "pages/projects/_id/experiments/add" */))
const _406cc784 = () => interopDefault(import('../pages/projects/_id/experiments/_experimentId.vue' /* webpackChunkName: "pages/projects/_id/experiments/_experimentId" */))
const _cd005eb8 = () => interopDefault(import('../pages/projects/_id/member.vue' /* webpackChunkName: "pages/projects/_id/member" */))
const _97db5bd4 = () => interopDefault(import('../pages/teams.vue' /* webpackChunkName: "pages/teams" */))
const _3d227019 = () => interopDefault(import('../pages/teams/index.vue' /* webpackChunkName: "pages/teams/index" */))
const _5cb9942a = () => interopDefault(import('../pages/users/index.vue' /* webpackChunkName: "pages/users/index" */))
const _470ff27a = () => interopDefault(import('../pages/users/add.vue' /* webpackChunkName: "pages/users/add" */))
const _4d7809cb = () => interopDefault(import('../pages/phenotypes/_phenotypeId/index.vue' /* webpackChunkName: "pages/phenotypes/_phenotypeId/index" */))
const _7252dabe = () => interopDefault(import('../pages/users/_userId.vue' /* webpackChunkName: "pages/users/_userId" */))
const _24767851 = () => interopDefault(import('../pages/users/_userId/password-change.vue' /* webpackChunkName: "pages/users/_userId/password-change" */))
const _e542f99c = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/analysis",
    component: _5ef0bca8,
    name: "analysis"
  }, {
    path: "/experiments",
    component: _11140676,
    name: "experiments"
  }, {
    path: "/inspire",
    component: _2ff6773a,
    name: "inspire"
  }, {
    path: "/login",
    component: _57062649,
    name: "login"
  }, {
    path: "/phenotype-categories",
    component: _2f3c4d9e,
    children: [{
      path: "",
      component: _908e1218,
      name: "phenotype-categories"
    }, {
      path: "add",
      component: _8ef73c7a,
      name: "phenotype-categories-add"
    }]
  }, {
    path: "/phenotypes",
    component: _81ca4644,
    name: "phenotypes"
  }, {
    path: "/projects",
    component: _71c8884a,
    children: [{
      path: "",
      component: _acc7a166,
      name: "projects"
    }, {
      path: "add",
      component: _7b484748,
      name: "projects-add"
    }, {
      path: ":id",
      component: _6117a0f5,
      children: [{
        path: "",
        component: _e5fa4890,
        name: "projects-id"
      }, {
        path: "analysis",
        component: _26a13366,
        children: [{
          path: "",
          component: _2191d52e,
          name: "projects-id-analysis"
        }, {
          path: "add",
          component: _931ccd10,
          name: "projects-id-analysis-add"
        }, {
          path: ":analysisId",
          component: _ce15abe6,
          children: [{
            path: "",
            component: _53299a60,
            name: "projects-id-analysis-analysisId"
          }, {
            path: "copy",
            component: _3491ea67,
            name: "projects-id-analysis-analysisId-copy"
          }, {
            path: "peaks",
            component: _aeb8d71c,
            children: [{
              path: "",
              component: _1ef77d75,
              name: "projects-id-analysis-analysisId-peaks"
            }, {
              path: ":peakId",
              component: _6e946448,
              children: [{
                path: "",
                component: _d7e10142,
                name: "projects-id-analysis-analysisId-peaks-peakId"
              }]
            }]
          }, {
            path: "results",
            component: _8f5adb18,
            name: "projects-id-analysis-analysisId-results"
          }]
        }]
      }, {
        path: "experiments",
        component: _cacd3b08,
        children: [{
          path: "",
          component: _59d91bff,
          name: "projects-id-experiments"
        }, {
          path: "add",
          component: _05f04d8e,
          name: "projects-id-experiments-add"
        }, {
          path: ":experimentId",
          component: _406cc784,
          name: "projects-id-experiments-experimentId"
        }]
      }, {
        path: "member",
        component: _cd005eb8,
        name: "projects-id-member"
      }]
    }]
  }, {
    path: "/teams",
    component: _97db5bd4,
    children: [{
      path: "",
      component: _3d227019,
      name: "teams"
    }]
  }, {
    path: "/users",
    component: _5cb9942a,
    name: "users"
  }, {
    path: "/users/add",
    component: _470ff27a,
    name: "users-add"
  }, {
    path: "/phenotypes/:phenotypeId",
    component: _4d7809cb,
    name: "phenotypes-phenotypeId"
  }, {
    path: "/users/:userId",
    component: _7252dabe,
    name: "users-userId",
    children: [{
      path: "password-change",
      component: _24767851,
      name: "users-userId-password-change"
    }]
  }, {
    path: "/",
    component: _e542f99c,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
