export default {
  methods: {
    phenotypeCategoryDataTableCustomFilter(value, search) {
      if (value != null && search != null) {
        const lowerCaseSearch = search.toLocaleLowerCase()
        if (Array.isArray(value)) {
          return value
            .reduce((acc, { term_id: termId, description }) => {
              return `${acc} ${termId} ${description}`.toLocaleLowerCase()
            }, '')
            .includes(lowerCaseSearch)
        } else {
          return value.toString().toLocaleLowerCase().includes(lowerCaseSearch)
        }
      }
      return false
    },
    getSearchablePhenotypeString(phenotype) {
      const phenotypeKeysSet = new Set()
      phenotypeKeysSet.add(phenotype.name)
      for (const phenotypeCategory of phenotype?.categories) {
        phenotypeKeysSet.add(phenotypeCategory.name)
        phenotypeKeysSet.add(phenotypeCategory.description)
        for (const ontology of phenotypeCategory?.ontologies ?? []) {
          phenotypeKeysSet.add(ontology.term_id)
          phenotypeKeysSet.add(ontology.description)
        }
      }
      return Array.from(phenotypeKeysSet.values()).join(' ').toLocaleLowerCase()
    },
    customFilterOnSearchableString(value, search, item) {
      if (value != null && search != null && typeof value !== 'boolean') {
        const lowerCaseSearch = this.normalizeStr(search).toLocaleLowerCase()

        // check if there is a searchableKeys and search against it
        if (item?.searchableString) {
          return this.normalizeStr(
            `${item?.searchableString} ${value.toString()}`
          )
            .toLocaleLowerCase()
            .includes(lowerCaseSearch)
        } else {
          return this.normalizeStr(value.toString())
            .toLocaleLowerCase()
            .includes(lowerCaseSearch)
        }
      }
      return false
    },
    normalizeStr(string) {
      return string.normalize('NFD').replace(/\p{Diacritic}/gu, '')
    },
  },
}
