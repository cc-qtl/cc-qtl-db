# CCQTL: making QTL mapping in Collaborative Cross mice accessible to a broader users community

[![pipeline status](https://gitlab.pasteur.fr/hub/cc-qtl-db/badges/dev/pipeline.svg)](https://gitlab.pasteur.fr/hub/cc-qtl-db/commits/dev)
[![Anaconda-Server Badge](https://anaconda.org/bioconda/r-ccqtl/badges/version.svg)](https://anaconda.org/bioconda/r-ccqtl)

<!-- [![coverage report](https://gitlab.pasteur.fr/hub/cc-qtl-db/badges/dev/coverage.svg)](https://gitlab.pasteur.fr/hub/cc-qtl-db/commits/dev) -->

[![Anaconda-Server Badge](https://anaconda.org/bioconda/r-ccqtl/badges/latest_release_date.svg)](https://anaconda.org/bioconda/r-ccqtl)
[![Anaconda-Server Badge](https://anaconda.org/bioconda/r-ccqtl/badges/license.svg)](https://anaconda.org/bioconda/r-ccqtl)
[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db.git/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db.git)
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:2acedd6f278157b5b0112245f0febc23a45937ef/)](https://archive.softwareheritage.org/swh:1:dir:2acedd6f278157b5b0112245f0febc23a45937ef)

[[_TOC_]]

## Introduction

Dissecting the genetic basis of complex traits (eg. height, weight, disease gravity,..), namely, identifying which regions of the genome explain part of the heritable phenotypic variation, is a grand question in biology that brings together two complementary approaches carried over by two often distinct communities, GWAS on natural populations on the one hand and QTL mapping in so-called mapping populations on the other hand.

<!-- Analysis of GWAS data is indeed purely computational, and performed by scientists immersed into a quantitative environment thus well-aware of good code management practice. Conversely, no small part of QTL mapping approaches remain performed by experimental geneticists, building up on tedious phenotyping experiments that leave little time to catch up with both the statistical and reproducible analysis requirements. -->

While GWAS are typically carried out by statistical genetics groups well-versed in quantitative environments and code management, experimental geneticists  performing QTL mapping focus on labor-intensive phenotyping experiments thus requiring further support, both for code and statistics, to benefit from best practices in the field.

In this broader context, CCQTL positions itself as a one-stop shop interface dedicated to QTL mapping on [Collaborative Cross](https://www.nature.com/articles/ng1104-1133) data, an increasingly used mouse mapping population that derives from 8 founder strains.

CCQTL embarks both a user-friendly GUI allowing end-to-end QTL mapping analysis (from data transformation to exploration of the QTL interval, eg, identifying candidate genes) and a database structure guaranteeing the safe and organized storage of phenotypic data along with an advanced permissions system.

CCQTL’s main goal is to allow non-specialists (mouse experimental geneticists on their own or publicly available data, trainers for demo or teaching purposes) to explore and analyze data by themselves. However, the added bonuses of Galaxy-powered analyses reproducibility and permission system makes CCQTL also relevant for more experienced users, eg. facilities.

CCQTL's analytical component leverages R/qtl2 tools integrated into preconfigured Galaxy workflows designed explicitly for the CC. Specifically, we set up a R package, [ccqtl](https://anaconda.org/bioconda/r-ccqtl), available on Conda. On top of facilitating integration into the Galaxy ecosystem, it also fits the needs of users familiar with R. This companion package has a dedicated [gitlab](https://gitlab.pasteur.fr/cc-qtl/ccqtl), it is encouraged that you use it (rather than the current gitlab) for all matters regarding this analytical component.

CCQTL is still under active development and its [documentation](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/tree/master#cc-qtl-making-qtl-mapping-in-collaborative-cross-mice-accessible-to-a-broader-users-community) is being updated regularly. It is thus strongly encouraged that you always use the latest release of CCQTL.

## News

Current version of CCQTL (as of latest update) features the following:

- QTL mapping can be performed on several phenotypes at a time (with or without adjustment for multiple testing)
- Covariates (categorical eg sex or continuous eg mouse weight) can be provided to the QTL model. These will be treated as additive.
- Users can choose between 3 sets of genetic markers (MegaMUGA, GigaMUGA, MegaMUGA and GigaMUGA interleaved).
- Only m38 genome version is available at present.

## Installation

### Foreword

As of now, it is not possible for external users to create an account on the [Institut Pasteur CCQTL interface](https://cc-qtl.pasteur.cloud/) and try it.

Instead, users will need to first deploy the interface components (client, backend, DB), either locally using Docker compose or on a Kubernetes cluster using Helm charts. They also need to find a Galaxy instance where the analysis workflow can run (meaning where the [suite_ccqtl](https://toolshed.g2.bx.psu.edu/view/rplanel/suite_ccqtl/e125a17ec1fb) is installed).
Step-by-steps instructions for deployment using Docker Compose are provided [here](#installation-self-hosting-with-docker).
Helm charts for Kubernetes can be found [here](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/tree/master/chart). The specific configuration of the Kubernetes cluster used may trigger PostgreSQL-related errors (we are using [postgres-operator](https://github.com/zalando/postgres-operator) that needs to be installed on Kubernetes cluster), we thus prefer not to provide detailed instructions as the procedure may vary from one case to the next. Prospective users should however feel free to drop an issue!

### Prerequisites

In order to run CCQTL locally you need to install [Docker Engine >= 20.10.0](https://docs.docker.com/engine/install/) and [Docker Compose V2](https://docs.docker.com/compose/install/).

### Installation (local deployment with Docker)

#### 1. Clone gitlab repository

You first need to get the source code of CCQTL.

```shell
git clone https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db.git
```

#### 2. Configure Galaxy instance and CCQTL backend

You need to make the CCQTL backend communicates with a Galaxy instance of your choice. To do so, you have to indicate to the backend the Galaxy instance URL as well as the Galaxy user API key. By default, a Galaxy user doesn't have any API key generated. You first need to generate this API key and let the CCQTL backend know about it. Most of the configuration can be set in the file `server/server/.env`. A sample of this file (`server/server/.env.sample`) is available and will help you getting started.

The sole constraints on the choice of the Galaxy instance is that the ccqtl utilities must be installed. One such instance is [Galaxy@Pasteur](https://galaxy.pasteur.fr/). Informations regarding account creation on Galaxy@Pasteur is to be found [here](#installing-all-that-on-my-galaxy-seems-a-burden-how-can-i-use-galaxy-pasteur).

<!-- The instructions below are given for the case of using the Galaxy instance deployed locally, but these are very similar in case you wish to use a "real" Galaxy instance (eg the one from your research institute - in this case, that means you deploy the interface and all its components locally, yet the analyses eg QTL mapping are being run on the server behind the Galaxy rather than on your laptop), see step 2 and 3 for the variations. -->

1. Copy the the sample `.env` file

```shell
cp server/server/.env.sample server/server/.env
```

2. Create (or retrieve) the API key.

<!-- For a server-based Galaxy instance (the one from your lab/institute, a public one on which you already have an account, etc): -->

The API key can be found on the Galaxy interface, in User > Preferences > Manage API key. You can then create a new one or retrieve the one already existing.

![create-galaxy-api-key](docs/img/create-galaxy-api-key.png)

1. Copy this key and paste it to the file `server/server/.env`. You only need to set `GALAXY_API_KEY` and `GALAXY_USERNAME` if you are using the Pasteur Galaxy instance. If it is not the case, you also need to set : `GALAXY_BASE_URL`, `GALAXY_NAME` and `GALAXY_DESCRIPTION`.

```shell
# Set debug to false in production mode
DEBUG=True
TEMPLATE_DEBUG=%(DEBUG)s

# You must change this key for production
SECRET_KEY="qhxt=w+!2jnm)+prpd6)95ws@#f+-pfy-)@&hf-2m)p85)s4g@"

ALLOWED_HOSTS=*

# You must change this values if
# you are considering use it for production

# database username
# DATABASE_USER=cc-qtl
# DATABASE_NAME=cc-qtl-db
# DATABASE_HOST=postgresql
# DATABASE_PORT=5432
# DATABASE_PASSWORD=password

# this name will be used in galaxy history tags and name
# HOSTLABEL=local

# Galaxy variables

# The galaxy api key generated for the username
GALAXY_API_KEY="must-be-set" # <------- copy the newly api key here

# the galaxy instance need to have the ccqtl_suite installed
# url of the galaxy instance
# default : https://galaxy.pasteur.fr
# GALAXY_BASE_URL=<a-galaxy-instance-url> # <------- if not Galaxy@Pasteur, copy the URL of your selected instance here

# the name of the Galaxy instance
# default: "Galaxy@Pasteur"
# GALAXY_NAME=<galaxy-instance-name>

# Brief description of galaxy instance
# default : "Pasteur Galaxy instance"
# GALAXY_DESCRIPTION="<galaxy-instance-description>"

# user's credential on selected galaxy instance
GALAXY_USERNAME="must-be-set"
```

#### 3. Build and run your app with Compose

Then, you have to build the docker images and run them. The easiest way is to proceed using the docker compose file provided.

```shell
cd cc-qtl-db
docker compose up --build
```

#### 4. Login with the default admin account

You need to have an account in order to use the CCQTL interface. A default admin account is created.

Go to [http://localhost:8889/](http://localhost:8889/) and login with the default admin account.

- username : `admin`
- password : `password`

You can configure these default values with the environment variables define in the `.env` file:

- `DJANGO_SUPERUSER_USERNAME`
- `DJANGO_SUPERUSER_PASSWORD`
- `DJANGO_SUPERUSER_EMAIL`

## Usage

### Foreword

CCQTL embarks both a database ready to be populated and an analysis framework.

The CCQTL interface is built around the concept of project. This holds both in terms of permissions (you need to be a member of a project to view/modify objects attached to a project, more on permissions [here](#permission-systems)) and in terms of data organisation.

Each project can host multiple experiments, with one or several analyses associated to each experiment (as such, each analysis belongs to an experiment, which itselfs belongs to a project).

An example organisation, also depicting what CCQTL permits, is shown here.

![InterfaceOrga](docs/img/InterfaceOrga2.png)

As a rule of thumb, each project can be considered as a "biological question" (involving several people, the "project members"), which is being addressed in several steps, each being an experiment. As such, to each experiment corresponds a given set of phenotypes which will be subjected to QTL mapping following its upload into the interface. For each experiment, it will then be possible to run analyses several times, tuning the parameters, eg by enlarging the QTL support interval (confidence interval around the peak), being more or less stringent on the statistical threshold, re-running it all on a new verion of the genome, etc. In line with the strong requirement for reproduciblity while doing such back and forth analyses, all this is being wrapped into Galaxy-powered workflow thus guaranteeing proper tracking of the parameters that got used.

To allow the user to take his/her first steps with CCQTL, the [testdata](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/tree/master/testdata) folder contains an example of phenotypic data to be passed as input.

The recommended way to proceed (described in more details below) is as follows: creating a project and a first experiment withing this project, uploading phenotype data, annotating the data, exploring the data, running the QTL analysis.

### Getting started: project creation

Upon deployment, the user arrives on a blank interface. To start with, a first project must be created, by going to "Projects" in the left panel menu, then "+", then filling the project name and description. It is possible to edit project name and description later on, by selecting the pen icon on the project tile.

![FirstProjCreation](docs/img/FirstProjCreation.png)

The blue bubbles, empty on first deployment, document how many projects, experiments and analysis are existing in the interface, on the basis of what the user is allowed to see (see [permissions](#permission-systems)).

### Creating an experiment and uploading data

Once a project has been created, it is needed to create a first experiment, as described in the following picture.

![ExperimentCreationDataUpload](docs/img/ExperimentCreationDataUpload.png)

[testdata](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/tree/master/testdata) folder contains TestData.csv file, an example file to be passed as Experiment file input.
This example file contains two phenotype columns, additional phenotypes should be provided as additionnal columns.

Input file is expected to be of csv format, either comma or semicolon-separated, formatted as follows:
The headers of the four first fields (Individual_ID, Mother_Line, Father_Line, Sex) shall remain untouched.

| Individual_ID | Mother_Line | Father_Line | Sex | Pheno1 | Pheno2 | Covar1 |
| ------------- | ----------- | ----------- | --- | ------ | ------ | ------ |
| 1             | CC001       | CC001       | M   | 100.54 | 50.3   | 1      |
| 2             | CC001       | CC001       | F   | 99.35  | 21.2   | 2      |

- Individual_ID: An integer. It provides the unique identifier of this mouse (or line value) for this experiment.
- Mother_Line, Father_Line: "short" identifier of the CC line (eg, CCOO1 rather than CC001/Unc, /suffix referring to the institution where the line got developed).

In the case of CC lines, dam and sire (Mother_Line and Father_Line, respectively) are by design expected to be identical, however both fields needs to be filled. Albeit seemingly daunting, this structure is critical to allow extension to other mapping populations (see [FAQ](#can-i-use-cc-qtl-on-another-mapping-population-))

- Sex: should be provided as M or F. Note that current verion of ccqtl only supports datasets that contain both Male and Female data, or all Male, but not Female only (bug will be corrected in the next release, v0.0.1.beta.2).
- Pheno1: A float, using dot and not commas for decimals. Phenotypic value for phenotype "Pheno1".

Data can be provided either as mean values (one single value per line or line x sex) or individual values (one single value per animal), at user's discretion (see [FAQ](#shall-i-provide-individual-phenotypic-values-or-mean-values-at-the-line-level-)).

Do note that "Pheno1" string will be auto-detected as variable name upon upload in the interface (also applies to Pheno2, Covar1, etc). Phenotype (and covariate) naming should thus avoid spaces (to be substitued with dots, underscore, capitalization, etc) and special characters. Abbreviations or acronyms can be used, since the critical phenotypic informations (proper description of what exactly is being profiled) can/should be provided with controlled vocabulary, as described [here](#annotating-phenotype-data-using-controlled-vocabulary).

- Pheno2: A float, using dot and not commas for decimals. Phenotypic value for phenotype "Pheno2".

- Covar1: Per-individual value regarding covariate "Covar1" (here, 1 vs 2 indicate dummy covariate values). In case of a float, use dot instead of commas for decimals.

The four first columns aside, there is no constraint on the order/content of the further columns (said otherwise, all covariates can be provided first then all phenotypes, the reverse, phenotypes and covariates can be interleaved, etc).

Formatting requirements regarding phenotypic input data (input file format, use of means or individual values) are as well provided as help buttons in the interface.

### Annotating phenotype data using controlled vocabulary

To make full use of the database, it is strongly recommended that you annotate your phenotypes since the sole phenotype label can hardly capture exactly what got measured.
To do so, CCQTL offers free text zone, preselected values (is it organ specific vs whole body measurements, are the phenotypic values floats or integers, continuous or categoricals) but also controlled vocubulary, namely, ontologies.

The ontology terms associated to a given phenotype are to be provided in the "Phenotype category" menu of the interface. Selection of appropriate ontology is performed through an autocompletion field that uses the description of the ontology term (rather that the ontology term identifier, eg MP:xxxx) to propose matching terms to the user. As many terms as deemed relevant can be selected.

CCQTL ships with a minimal ontology (1248 terms total), that corresponds to the different terms currently used for phenotype annotation in the [Mouse Phenome Database](https://phenome.jax.org/about/ontologies). It is a merge of the three main ontologies used in the field: Mouse Anatomy ontology (MA, subsetted to 153 terms), Mammalian Phenotype ontology (MP, subsetted to 585 terms) and Vertebrate Trait ontology (VT, subsetted to 510 terms).

Along with the ontology terms, users can also make use of the free text zone which can be handy for providing experimental details (eg, in the case of genetic resistance to a given pathogen, amount of pathogen injected to the mouse) in a more user-friendly (altough less searchable) manner. Overall, we recommend to use ontology terms as much as possible.

### Exploring the phenotypic data

Distribution of phenotypic values must be somewhat gaussian to satisfy the statistical hypothesis behind QTL mapping. To allow the user to verify easily distributions for each phenotype, CCQTL embarks a phenotype value distribution plotting and transformation module.

To do so, user selects in the drop-down menu the phenotype of interest to be plotted, thus allowing to check easily if the distribution grosso modo fits a gaussian. Several common mathematical transformations can then be applied to look at whether this improves the pattern ; a help menu points to some indications on what would be the most appropriate transformation given the data.

If satisfactory, the user can then save the transformed data, eg to reload these as a new experiment file. This step is not yet embedded in the Galaxy workflow, users need to be cautious upon proper documentation of the mathematical transformations performed on the data.

![RawDistri](docs/img/RawDistri.png)

![TransformedDistri](docs/img/TransformedDistri.png)

Note that this plotting module allows to look at data distribution not only overall but also for each genotype, in the form of ordered means barplots (a common way of representating phenotypic data in CC lines or other RIL sets) and boxplots. All plots can be saved (png, svg, ..) to be used out of the interface (eg filling the user's lab book).

![PhenoExploration](docs/img/PhenoExploration.png)

### Running an analysis (QTL mapping) on the aforementioned experiment

Now that an experiment has been created and loaded with phenotypic data which distribution is compatible with QTL mapping, it is now possible to run an analysis, namely, a QTL mapping Galaxy worflow.
One analysis corresponds to a given QTL mapping workflow, launched with specific parameters.

To launch an analysis, the user must: create a new analysis in the corresponding Project (by clicking on the Analysis icon), select an experiment on which to run this analysis, select an analysis worklow, and select the parameters for that workflow. The user needs as well to give a name to the analysis (similarly to what has been done for Project and Experiment), and document it in the free text zone (eg by stating this is the very first analysis aimed at trying the pipeline/discovering the parameters).

![RunAnalysis1](docs/img/RunAnalysis1.png)

![RunAnalysis2](docs/img/RunAnalysis2v2.png)

QTL mapping and all related tasks are performed using a series of tools from the [R/qtl2 suite](https://kbroman.org/qtl2/index.html). At present, CCQTL only handles QTL mapping on continuous phenotype data (p. opp. binary traits, discussed [here](#is-my-data-appropriate-for-qtl-mapping-with-cc-qtl-)) on CC mice (p. opp. others populations, see [here](#can-i-use-cc-qtl-on-another-mapping-population-)): there is as such only one Galaxy workflow at present, more will become available upon later releases.

The workflow consists of the following steps. Some steps require the user to specify some parameters, others don't. The user does not have the hand on parameters that have been tailored for the CC (although the values that were used appear in the Galaxy recaps). For parameters that can be modified at user's discretion, default value are indicated and most of the drop-down menus only allow a limited, appropriate number of choices.

#### 1. Step 1: make_inputs

Given the phenotypes selected by the user (out of all the phenotypes provided in the input file uploaded in the interface) and the selected parameters (set of genetic markers, etc), creation of all the input files needed for qtl2: properly formatted phenotype and covariate files, yaml file (used by qtl2 to create the cross object) ; as well as the genotype probabilities and kinship that will then be used in the LMM.
At present, CC-QTL offers 3 sets of markers (MegaMUGA "[MM_m38](https://figshare.com/articles/dataset/MM_processed_files_zip/5404750)", GigaMUGA "[GM_m38](https://figshare.com/articles/dataset/GM_processed_files_zip/5404759/2)" and MM and GM markers interleaved "[MMnGM_m38](https://figshare.com/articles/dataset/MMnGM_processed_files_zip/5404762)"), and provides the "reference" genotype information for the 69 CC mine currently available.
Both the marker and genotype information currently provided on CCQTL derive from prior work from Karl Broman who made them available (see hyperlinks).
m39 versions of the markers (physical maps) will become available from next release onwards.
The user can as well provide again a description of the experiment.

![ParamInput](docs/img/ParamInput.png) -->

#### 2. Step 2: genome_scan

QTL mapping per se, in the form of single-marker regression using a LMM to account for kinship: at each marker, fits a full (phenotype as function of genotype & kinship) vs a null model (phenotype as a function of kinship) and computes the LOD​. Should the user wish to add covariate(s), these needs to be specified at this step. Selection of the covariates is performed through a drop-drown menu that lists all the possible variables that were passed in the experiment input file yet not previously selected by the user as phenotype. Here, sex is the covariate chosen by the user.

![ParamScan](docs/img/ParamScan2.png)

#### 3. Step 3: permutations

Permutation based strategy to define the maximal LOD score expected at random. Note that the permuation strategy accomodates (through the kinship) for unequal number of individuals per lines. The user needs to specify how many permutations are to be performed. In case covariates were passed to the QTL model at the previous step, it is advised that the user also incorporates these in the permutation strategy (stratification of the permutations). To do so, the user can pick in the drop-down menu (that only lists the covariates previously used in the genome scan) the chosen covariate(s) for permutations stratification. Note this can only be applied to categorical covariates (eg sex) - should you have a continuous covariate (eg nunmber of pups) it is advised that prior to loading the experiment files you convert such values into categorical ones (eg <5 pups, 5-10 pups, ... ).

![ParamPerm](docs/img/ParamPerm2.png)

#### 4. Step 4: find_peaks

Given the LOD values and the threshold defined by permutation, identify which peaks are statistically supported QTL. For each QTL, a confidence interval (also called "QTL support interval", which is the region of the genome in which the QTL is likely to be found, since the bona fide QTL is seldom under the peak) is also produced, its size can be defined by the user using a drop-off approach. The effects (how much of phenotypic heritable variation is explained by this QTL) and direction of effects (out of the 8 parental origins, which one is associated with the phenotype under scrutniy) are also estimated. User needs to provide the statistical alpha, whether or not correction for multiple phenotypes considered at once should be performed and drop-off value.

![ParamFindPeaks](docs/img/ParamFindPeaks.png)

#### 5. Step 5: refine_regions

in the region surrounding the QTL peak, extract all genes annotations and perform local association study using DNA sequence variants that distinguish the founder strains. The objective of this step is to perform a first level characterization of the genomic regions that contains the QTL: extracting the genes and assessing which SNPs are most strongly associated with the phenotype. Combination of the two forms a first, easy to perfom, candidate gene selection. For this step, the user needs to provide the size of the region to be further analysed as well as annotation data (sqlite files, actually) for both mouse genome annotation and DNA sequence variants segregating among the 8 founder strains (namely, SNPs, indels and SV that are present/absent across the 8 founders). As of now, only annotation files corresponding to mouse genome version m38 are available, m39 files will become available in a later release.

![ParamRefine](docs/img/ParamRefine2.png)

Depending on the workflow step and the type of Galaxy (deployed locally on laptop or cluster-based), the workflow can take some time - status for each step (pending, running, ran, queueing) as well as of the whole analysis workflow is available in the Analysis menu on the interface.

Similarly to what can be done on Galaxy, at the end of the analysis, it is possible to either make a copy of the analysis (eg to re-run it at such, using all same parameters but on a different experiment eg updated phenotyping experiment) or download the data files (as rds files) that were generated.

#### 6. Example results

Once an analysis has ran (green tick), it is possible to go and look at the results.

The very first plot that is provided is the regular genome scan LOD score plot, with or without making the threshold visible on the plot. QTL support interval is provided as orange shading around the peak ; by hovering over the LOD profile the marker and LOD score at this position appears.

![PlotGenomeScan](docs/img/PlotGenomeScan.png)

Behind the LOD profile the list of peaks is provided as a table, with all revelant informations (upper and lower bound to the QTL interval, p-value). The table can, as all tables displayed in the interface, be downloaded by the user. _Note that on the illustration below, no significant peak was detected (note the high p-value) - in such cases, the peak with the highest LOD score along with a minimal support interval is outputeed._

By clicking on each peak, the user can access the following informations, for that particular peak:

- **Founder effects in QTL interval**: effects follows the color code of the CC. Effect plots are easier to interpret along with the LOD score plot below. Effects are estimated as coefficients of regression.

  ![PlotEffects](docs/img/PlotEffects.png)

- **Phenotype as a function of genotype at peak marker**: plotting the phenotype values as a function of the genotype at the QTL peak marker is a more intuitive way than effect estimation to asses the association between phenotype and genotype. Here, being of blue (founder strain XX) and gray (founder strain YY) haplotypes at the QTL peak is associated with higher vs. lower phenotypic values for that trait.

  ![PlotHaplotypes](docs/img/PlotHaplotypes.png)

- **SNP association in peak region**: local association study. The list of SNPs and their pvalue are provided in the table below the plot.

  ![PlotSNPassoc](docs/img/PlotSNPassoc.png)

- **Phenotype as a function of genotype at top SNPs**: for the selected "top SNPs" (highest LOD scores following the local association study). Note the "NA" category in case the CC line genotype could not be measured at that position.

  ![PlotTopSNPs](docs/img/PlotTopSNPs.png)

- **List of genes in QTL interval**: note this table is interactive and hyperlinks point to gene annotation databases.

  ![PlotGeneList](docs/img/PlotGeneList.png)

## More advanced features

### Permission systems

Permission is thought at the interface level as well at the project level.

- At the interface level, users can have regular user rights or be superusers. The latter are allowed to create/remove user accounts and have all rights on all projects.
- At the project level, users can be manager or maintainer. Manager is the default permission set for the user who creates the project. Manager can add new users to the project, either as managers or as maintainers ; or remove users, on top of all the additionnal rights (creating an experiment, running an analysis, etc). Maintainer/Manager is project specific, a given user can be manager on project 1 but maintainer on project 2.

The permissions of the different users can be accessed as follows:

- via the "Users" interface menu. It is accessible to all users of the interface and recaps who is user vs. superuser.
- via the users icon for each project. It recaps who, for the given project, is manager vs maintainer. This is only accessible to the users' allowed to see this given project.
- for each user, a recap of his/hers permissions, both at the interface level (user/superuser) and at the project level (maintainer/manager for the different projects), is accessible on his "My account" menu.

### Deleting data

Projects, or experiments within a project, can be deleted using the thrashbin icon. It translates into permanent data loss and thus requires confirmation. This feature has been enabled to allow users to discover the interface (eg. by creating dummy projects without filling the database permanently). It is however strongly discouraged for real data as it conflicts with the needs of reproducibility.

### Modifying/extending the ontologies used

CCQTL ships with a minimal ontology described [earlier](#annotating-phenotype-data-using-controlled-vocabulary).

There exists, however, many other ontologies that can be used to annotate (mouse) phenotypic data, either the full Mouse Anatomy, Vertebrate Trait or Mammalian Phenotype ontologies, or others such as [MIQAS-TAB (Minimal Information for QTLs and Association Studies Tabular)](http://miqas.sourceforge.net/specification/MIQAS_TAB/MIQAS_TAB_specification.html), [MMO (Measurement Method Ontology)](http://rgd.mcw.edu/rgdweb/ontology/view.html?acc_id=MMO:0000000) or [CMO (Clinical Measurement Ontology)](http://rgd.mcw.edu/rgdweb/ontology/view.html?acc_id=CMO:0000000).

Should the user wish to modify the ontologies associated with CCQTL (remove some ontology terms, add a different ontology, etc), it is possible to do so by editing the [corresponding json](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/blob/master/server/api/fixtures/ontology.json).

<!--
### Removing all the data

```shell
docker compose down -v --rmi all
```

If you want to start a fresh new install of cc-qtl, you need to remove some volumes.
-->

## FAQ

### Is my data appropriate for QTL mapping with CCQTL ?

Current statistical framework underlying CCQTL uses linear regression for QTL mapping (LMM with kinship matrix, actually), which is appropriate fo continuous traits that exhibit a roughly gaussian distribution. To validate this assumption, it is highly advised that the user explores the distribution of phenotypic values and possibly proceeds with mathematical (log, inv, sqrt) transformation of the data. In that respect, a plotting/transformation module (one phenotype at a time) is proposed in CCQTL.
Should the trait under scrutinity departs from such distribution, even after mathematical transformation of the data, QTL detection will be greatly impeded, possibly raising fallacious QTL peaks.
QTL mapping can be performed on non-gaussian data, treating the trait as binary (eg infection score > or < to a given score, instead of a continuum of score values) rather than continuous ; in which case it uses logistic rather than linear regression, not yet implemented in CCQTL.
In the meantime, a tinkered way of going to QTL mapping could be to to remove phenotypic values that create strong outliers in the distribution, by re-uploading a "cleaned" input file. This will thus corresponds to a new experiment, to which will be attached its corresponding analyses, thus guaranteeing tracking of the changes that were made and analysis reproducibility.

### Shall I provide individual phenotypic values or mean values at the line level ?

One critical parameter to keep in mind to answer this question is the number of individuals phenotyped for each line. In case there are very heterogeneous number of individual across the different lines (eg, 3 individuals for CC001 and 12 for CC002), mean phenotypic values devised at the line level are not readily comparable since the degree of precision of the mean estimate are not the same (also called variance heterogeneity).

To accomodate for this real-life scenario (which can be frequent in preliminary experiments), CCQTL allows the user to perform the QTL mapping using individual phenotypic values rather that mean values, thus allowing to deal properly with unequal number of individual accross lines. This will be handled in the LMM and permutation strategy afterwards.

Note however it is always preferred that the phenotyping experiments involve balanced number of individuals across genotype categories and use adjusted mean values accounting for known covariates (cage effects, etc). In that case, the user can provide one value per line (taking or not sex into account, to be defined depending on the phenotype expectations).

### Can I use CCQTL on another mapping population ?

CCQTL has been designed with the Collaborative Cross in mind. By tinkering a few parameters however (on the R-ccqtl part), it can get applicable to others 8-way RIL mapping populations eg the MAGIC lines in plants. We can provide guidance on which parameters to modify, yet prospective users may find their way in the [R/qtl2 documentation](https://kbroman.org/qtl2/assets/vignettes/user_guide.html). <!--such a way it is tailored for so-called 8-way RIL at both the analytic (parameters, input files) and database levels. As such, CCQTL could be used not only for QTL mapping in the Collaborative Cross, but also for others 8-way RIL mapping population such as MAGIC lines for instance. It will however require some tinkering for some parameters (eg tweaking the Sex attribute for dioicous plant species).-->

The analysis parameters, however, are not compatible with two-way RIL (namely, a mapping population that derives only from two parents, such as the mouse BxD mapping population) or more generally speaking mapping populations deriving only from 2 parental strains. This builds from the fact QTL mapping in two-parents mapping population is less complex analysis-wise than in multiparent populations, thus more accessible to experimental geneticists. Of course strictly speaking that does not prevent users to try to adapt, but this will most likely require some more work. Do note that the GPLv3 licence of CCQTL (both DB and R utilities) is permissive towards users implementing their own modifications to suit their own needs.

One key feature of CCQTL is that given its database structure, it is thought in such a way that phenotypic data can be acquired for multiple traits, over different experiments performed by different users, with the objective in the long run to assess traits correlations. That requires the mouse genotypes to be reproducible, which is the case for CC lines, but not for DO lines, another mouse multiparental mapping population deriving from the same 8 founder lines than the CC, in which by design each line has a unique genetic makeup which cannot be reproduced.

Ongoing work on CCQTL is to extend it to so-called CC-RIX, which are intercrosses between CC lines. CC-RIX are genetically reproducible like CCs, yet benefit from the buffering effect of heterozygosity and allow to assess parental effects.

### How can I install the analysis workflow on (my) Galaxy ?

We made the analysis workflow Galaxy-powered to both guarantee reproducibility and make it easily available to the community. It can thus be used on any Galaxy instance (eg the one from your institution or a public, larger-scale one) as long as the ccqtl utilities are installed.

To facilitate installation, we made the wrappers available on the [Galaxy Toolshed](https://toolshed.g2.bx.psu.edu/), the ccqtl R package itself being available on [bioconda](https://anaconda.org/bioconda/r-ccqtl).
These two informations (wrappers + path to conda package) should be sufficient for a Galaxy admin ; process of installation of a given tools varies from one Galaxy instance to the next.

<!-- In case you are solely interested in deploying and running CCQTL locally on your computer, eg for test purposes, nothing is required from your side else than following the installation instructions, as the Docker will deploy a local Galaxy along with all the interface components.
If you want, however, to install it on a pre-existing Galaxy instance (eg, Galaxy instance from your institute), the easiest way to proceed is to use the conda package available [here](https://anaconda.org/bioconda/r-ccqtl).-->
<!-- If you want, however, to install it on a pre-existing Galaxy instance (eg, Galaxy instance from your institute): you can do so using the [wrappers](https://gitlab.pasteur.fr/galaxy-team/galaxy-tools/-/tree/master/tools/cc_qtl) we use on the Galaxy@Pasteur instance. The current version of CCQTL is indeed not yet compatible for an installation using the Galaxy Toolshed, this will be the case from the next release onwards. -->

It is thus possible to run QTL analysis on Galaxy, by using the sole analysis component, without deploying the interface - it is however discouraged to do so, since you won't benefit from all the functionalities of CCQTL eg plots or organised storage (same applies if you use the ccqtl R package).

<!-- Keep in mind, though, that by using CCQTL directly through Galaxy (without deploying the interface components), you will get the analysis workflow yet not the database, nor the interface ; thus not benefiting from all the functionalities of CCQTL eg plots (same applies if you use the ccqtl R package).-->

### Installing all that on my Galaxy seems a burden, how can I use Galaxy Pasteur?

Galaxy@Pasteur is not restricted to Institut Pasteur and can host external users accounts as well. The constraint will be that external accounts can only benefit from 20 Go of disk space for the storage of their data.
It is up to the user to assess, given its prospective amount of data, whether Galaxy@Pasteur can suffice or whether it is preferable to look into another Galaxy instance.
See [here](https://galaxy.pasteur.fr/login) for account creation. Do note that multi-accounts are tracked and deleted (along with all data).

### Where exactly is my data stored ?

Great point! Data storage is interconnected between the physical partition of the DB per se and the Galaxy:

- phenotypic (and covariate) data by themselves are stored and organised in the DB per se, at the entry point specified for the PostgreSQL in the `.env` file. This can be locally on your computer or on a dedicated partition on your Kubernetes cluster.
- analysis data, however, are not stored in the DB in itself, but on the Galaxy instance of your choice - these are linked to the corresponding project:experiment objects in the DB, yet not stored there.

The implications are two-fold:

- should you erase your Galaxy history (directly from the Galaxy interface), the corresponding analysis results will disappear from the CCQTL interface.
- should you lose your DB, since the very first step of the analysis workflow consists in determining the covariate and phenotypes of your choice, you will still be in a position to re-run your analyses and reconstruct a new DB (from what you already had).

## Help

Feel free to [write an issue](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/issues) in case you could not find what you needed in the [FAQ](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/tree/master#faq).

## About CCQTL

CCQTL is being developed at the [Bioinformatics and Biostatistics Hub](https://research.pasteur.fr/fr/team/bioinformatics-and-biostatistics-hub/) of Institut Pasteur.
CCQTL is licensed under [GPLv3](https://www.r-project.org/Licenses/GPL-3).

The most appropriate way to cite CCQTL is provided in the CITATION.cff file.

CCQTL is based on the idea of Xavier Montagutelli (Mouse Genetics unit, Institut Pasteur).

Its development has been involving Rémi Planel, Victoire Baillet, Vincent Guillemot, Pascal Campagne and Rachel Torchet on the Hub side, as well as Jean Jaubert, Christian Vosshenrich, Marion Rincel and Xavier Montagutelli as key contributors (conception and testing).
