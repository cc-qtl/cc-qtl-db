from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import (
    Analysis,
    Phenotype,
    User,
    Project,
    PhenotypeCategory,
    GalaxyUser,
    GalaxyWorkflow,
)
from guardian.admin import GuardedModelAdmin

# Register your models here.


class ProjectAdmin(GuardedModelAdmin):
    list_display = ("project_name", "creation_date")


class AnalysisAdmin(GuardedModelAdmin):
    list_display = ("name", "creation_date")


class PhenotypeAdmin(admin.ModelAdmin):
    pass


class PhenotypeCategoryAdmin(admin.ModelAdmin):
    pass


class GalaxyUserAdmin(admin.ModelAdmin):
    list_display = ("username", "galaxy_instance")
    pass


class GalaxyWorkflowAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Analysis, AnalysisAdmin)
admin.site.register(PhenotypeCategory, PhenotypeCategoryAdmin)
admin.site.register(Phenotype, PhenotypeAdmin)
admin.site.register(GalaxyUser, GalaxyUserAdmin)
admin.site.register(GalaxyWorkflow, GalaxyWorkflowAdmin)
