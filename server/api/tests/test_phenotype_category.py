import pytest
from .utils import authenticate
import json
from pytest_factoryboy import register
from api.factories import PhenotypeCategoryFactory

register(PhenotypeCategoryFactory)


@pytest.mark.django_db
def test_manager_permissions(
    api_client,
    api_client_with_superuser_credential,
    role_users_created_with_api,
    user_password,
    phenotype_category_factory,
):
    for role_user in role_users_created_with_api:
        with open("api/fixtures/role-permissions.json", "r") as f:
            role_permissions = json.loads(f.read())
            fixture_permissions = set(
                [
                    perm["fields"]["permission"][0].replace("_phenotypecategory", "")
                    for perm in role_permissions
                    if perm["fields"]["role"][0] == role_user["role"]
                    and perm["fields"]["permission"][2] == "phenotypecategory"
                ]
            )

            phenotype_category = phenotype_category_factory.build()

            # get phenotype category permissions
            client = authenticate(api_client, role_user["user"], user_password)
            response = client.get("/api/phenotype-categories/permissions/")
            response_permissions = set(response.data)
            assert response_permissions == fixture_permissions
            phenotype_category_db = None
            if "add" in response_permissions:
                response = client.post(
                    "/api/phenotype-categories/",
                    data={
                        "name": phenotype_category.name,
                        "description": phenotype_category.description,
                        "datatype": phenotype_category.datatype,
                        "dataclass": phenotype_category.dataclass,
                        "nature": phenotype_category.nature,
                        "location": phenotype_category.location,
                    },
                )
                assert response.status_code == 201
                phenotype_category_db = response.data
            else:
                # create a phenotype category anyway
                response = api_client_with_superuser_credential.post(
                    "/api/phenotype-categories/",
                    data={
                        "name": phenotype_category.name,
                        "description": phenotype_category.description,
                        "datatype": phenotype_category.datatype,
                        "dataclass": phenotype_category.dataclass,
                        "nature": phenotype_category.nature,
                        "location": phenotype_category.location,
                    },
                )
                assert response.status_code == 201
                phenotype_category_db = response.data

            if "change" in response_permissions:
                response = client.put(
                    f"/api/phenotype-categories/{phenotype_category_db['id']}/",
                    data={
                        "name": phenotype_category.name,
                        "description": "new description",
                        "datatype": phenotype_category.datatype,
                        "dataclass": phenotype_category.dataclass,
                        "nature": phenotype_category.nature,
                        "location": phenotype_category.location,
                    },
                )
                assert response.status_code == 200

            if "view" in response_permissions:
                response = client.get(
                    f"/api/phenotype-categories/{phenotype_category_db['id']}/"
                )
                assert response.status_code == 200

            if "delete" in response_permissions:
                response = client.delete(
                    f"/api/phenotype-categories/{phenotype_category_db['id']}/"
                )
                assert response.status_code == 204
