import json

# Django
from django.urls import reverse
from django.core.management import call_command


# Pytest
import pytest
from pytest_factoryboy import register

# django app
from api.factories import SuperuserFactory, UserFactory
from api.models import User, RoleGroup
from .utils import authenticate

register(SuperuserFactory)
register(UserFactory)


# @pytest.fixture(scope="session")
# def celery_config():
#     return {
#         "broker_url": "amqp://",
#         "result_backend": "rpc",
#     }


# @pytest.fixture(scope="session")
# def celery_worker_pool():
#     return "prefork"


@pytest.fixture
def user_password():
    yield "strong-test-pass"


@pytest.fixture(scope="session")
def django_db_init(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command("initdb")
        # call_command("filldb")


@pytest.fixture
def api_client(django_db_init):
    from rest_framework.test import APIClient

    yield APIClient()


@pytest.mark.django_db
@pytest.fixture
def normaluser_build(user_factory):
    yield user_factory.build()


@pytest.mark.django_db
@pytest.fixture
def normaluser_build_batch(user_factory):
    yield user_factory.build_batch(5)


@pytest.mark.django_db
@pytest.fixture
def superuser(superuser_factory, user_password):
    superuser = superuser_factory(password=user_password)
    yield superuser
    User.objects.get(id=superuser.id).delete()


@pytest.mark.django_db
@pytest.fixture
def api_client_with_superuser_credential(superuser, api_client, user_password):
    url = reverse("token_obtain_pair")
    resp = api_client.post(
        url,
        {"username": superuser.username, "password": user_password},
        format="json",
    )
    assert resp.status_code == 200
    api_client.credentials(HTTP_AUTHORIZATION="Bearer " + resp.data["access"])
    yield api_client


@pytest.mark.django_db
@pytest.fixture
def user_created_with_api(
    api_client_with_superuser_credential, normaluser_build, user_password
):
    response = api_client_with_superuser_credential.post(
        "/api/users/",
        data={
            "username": normaluser_build.username,
            "first_name": normaluser_build.first_name,
            "last_name": normaluser_build.last_name,
            "email": normaluser_build.email,
            "is_active": normaluser_build.is_active,
            "password": user_password,
        },
    )
    assert response.status_code == 201
    user_id = response.data["id"]
    user = User.objects.get(pk=user_id)
    assert normaluser_build.username == user.username
    yield user
    user.delete()


# Create fixture that get the role groups
# Load fixture and extract information
@pytest.mark.django_db
@pytest.fixture
def role_groups():
    with open("api/fixtures/groups.json", "r") as f:
        role_groups_fixture = json.loads(f.read())
        role_groups = [
            {
                "group_name": group["fields"]["group_ptr"][0],
                "role": group["fields"]["role"][0],
            }
            for group in role_groups_fixture
            if group["model"] == "api.rolegroup"
        ]
        yield role_groups


@pytest.mark.django_db
@pytest.fixture
def role_users_created_with_api(
    api_client_with_superuser_credential,
    user_factory,
    user_password,
    role_groups,
):
    for role_group in role_groups:
        db_group = RoleGroup.objects.get(name__exact=role_group["group_name"])
        normaluser_build = user_factory.build()
        response = api_client_with_superuser_credential.post(
            "/api/users/",
            data={
                "username": normaluser_build.username,
                "first_name": normaluser_build.first_name,
                "last_name": normaluser_build.last_name,
                "email": normaluser_build.email,
                "is_active": normaluser_build.is_active,
                "password": user_password,
                "appGroup": {"id": db_group.id, "name": db_group.name},
            },
            format="json",
        )
        assert response.status_code == 201
        user_id = response.data["id"]
        user = User.objects.get(pk=user_id)
        assert normaluser_build.username == user.username
        role_group["user"] = user
    yield role_groups
    for role_group in role_groups:
        role_group["user"].delete()


@pytest.mark.django_db
@pytest.fixture
def user_created_with_api_factory(api_client, superuser, user_factory, user_password):
    created_records = []

    def _user_created_with_api_factory():
        normaluser_build = user_factory.build()
        api_client_with_superuser_credential = authenticate(
            api_client, superuser, user_password
        )
        response = api_client_with_superuser_credential.post(
            "/api/users/",
            data={
                "username": normaluser_build.username,
                "first_name": normaluser_build.first_name,
                "last_name": normaluser_build.last_name,
                "email": normaluser_build.email,
                "is_active": normaluser_build.is_active,
                "password": user_password,
            },
        )
        assert response.status_code == 201
        user_id = response.data["id"]
        user = User.objects.get(pk=user_id)
        assert normaluser_build.username == user.username
        created_records.append(user)
        return user

    yield _user_created_with_api_factory

    for user in created_records:
        user.delete()


@pytest.mark.django_db
@pytest.fixture
def user_batch_created_with_api(
    api_client,
    normaluser_build_batch,
    user_password,
    superuser,
):
    api_client_superuser = authenticate(api_client, superuser, user_password)
    users = []
    for normaluser_build in normaluser_build_batch:
        response = api_client_superuser.post(
            "/api/users/",
            data={
                "username": normaluser_build.username,
                "first_name": normaluser_build.first_name,
                "last_name": normaluser_build.last_name,
                "email": normaluser_build.email,
                "is_active": normaluser_build.is_active,
                "password": user_password,
            },
        )
        assert response.status_code == 201
        user_id = response.data["id"]
        user = User.objects.get(pk=user_id)
        assert normaluser_build.username == user.username
        users.append(user)
    yield users
    for user in users:
        user.delete()


@pytest.mark.django_db
@pytest.fixture
def api_client_with_credentials(user_created_with_api, api_client, user_password):
    url = reverse("token_obtain_pair")
    resp = api_client.post(
        url,
        {"username": user_created_with_api.username, "password": user_password},
        format="json",
    )
    assert resp.status_code == 200
    assert resp.data["access"] is not None
    assert resp.data["access"] != ""
    api_client.credentials(HTTP_AUTHORIZATION="Bearer " + resp.data["access"])
    yield api_client


@pytest.mark.django_db
@pytest.fixture
def user_logged(api_client_with_credentials):
    current_user_resp = api_client_with_credentials.get("/api/current-user/")
    assert current_user_resp.status_code == 200
    user_id = current_user_resp.data["id"]
    user = User.objects.get(pk=user_id)
    yield user
    user.delete()
