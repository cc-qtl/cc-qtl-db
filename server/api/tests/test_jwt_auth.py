from django.urls import reverse
import pytest

from .utils import authenticate


@pytest.mark.parametrize("status_code_expected", [401])
def test_not_logged(api_client, status_code_expected):
    resp = api_client.get("/api/")
    assert resp.status_code == status_code_expected


@pytest.mark.django_db
@pytest.mark.parametrize("status_code_expected", [200])
def test_auth_superuser(superuser, api_client, status_code_expected, user_password):
    url = reverse("token_obtain_pair")
    resp = api_client.post(
        url,
        {"username": superuser.username, "password": user_password},
        format="json",
    )
    assert resp.status_code == status_code_expected
    assert resp.data["access"] is not None
    assert resp.data["access"] != ""
    assert resp.data["refresh"] is not None
    assert resp.data["refresh"] != ""


@pytest.mark.django_db
@pytest.mark.parametrize("status_code_expected", [200])
def test_auth_normaluser(
    user_created_with_api, api_client, status_code_expected, user_password
):
    url = reverse("token_obtain_pair")
    resp = api_client.post(
        url,
        {"username": user_created_with_api.username, "password": user_password},
        format="json",
    )
    assert resp.status_code == status_code_expected
    assert resp.data["access"] is not None
    assert resp.data["access"] != ""
    assert resp.data["refresh"] is not None
    assert resp.data["refresh"] != ""


@pytest.mark.django_db
@pytest.mark.parametrize("status_code_expected", [200])
def test_superuser_auth(api_client, superuser, status_code_expected, user_password):
    auth_client = authenticate(api_client, superuser, user_password)
    # test current-user
    response = auth_client.get("/api/current-user/")
    assert response.status_code == status_code_expected
    assert response.data["id"] == superuser.id

    # test same user
    response = auth_client.get(f"/api/users/{superuser.id}/")
    assert response.status_code == status_code_expected
    assert response.data["username"] == superuser.username


@pytest.mark.django_db
@pytest.mark.parametrize("status_code_expected", [200])
def test_normaluser_auth(
    api_client, user_created_with_api, status_code_expected, user_password
):
    auth_client = authenticate(api_client, user_created_with_api, user_password)
    # test current-user
    response = auth_client.get("/api/current-user/")
    assert response.status_code == status_code_expected
    assert response.data["id"] == user_created_with_api.id

    # test same user
    response = auth_client.get(f"/api/users/{user_created_with_api.id}/")
    assert response.status_code == status_code_expected
    assert response.data["username"] == user_created_with_api.username
