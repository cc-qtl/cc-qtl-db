import pytest
from api.tests.utils import authenticate
from api.models import ProjectGroup, ProjectRole


# test a member is part of one group project at the same time


@pytest.mark.django_db
def test_member_to_one_projectgroup(
    api_client,
    user_created_with_api,
    project_created_from_api,
    user_batch_created_with_api,
    user_password,
):
    api_client_with_credentials = authenticate(
        api_client, user_created_with_api, user_password
    )
    maintainer_role = ProjectRole.objects.get(name="maintainer")
    reader_role = ProjectRole.objects.get(name="reader")
    # add user to maintainer group
    maintainer_project_group = ProjectGroup.objects.get(
        project=project_created_from_api, project_role=maintainer_role
    )
    reader_project_group = ProjectGroup.objects.get(
        project=project_created_from_api, project_role=reader_role
    )

    users = [user.id for user in user_batch_created_with_api]
    assert maintainer_project_group.user_set.count() == 0
    response = api_client_with_credentials.patch(
        f"/api/projects/{project_created_from_api.id}/groups/{maintainer_project_group.id}/",
        {"users": users},
        format="json",
    )
    assert response.status_code == 200
    assert maintainer_project_group.user_set.count() == 5
    assert reader_project_group.user_set.count() == 0

    # add user to reader groups

    response = api_client_with_credentials.patch(
        f"/api/projects/{project_created_from_api.id}/groups/{reader_project_group.id}/",
        {"users": users},
        format="json",
    )
    assert response.status_code == 200
    assert maintainer_project_group.user_set.count() == 0
    assert reader_project_group.user_set.count() == 5

    # test delete member
    response = api_client_with_credentials.patch(
        f"/api/projects/{project_created_from_api.id}/groups/{reader_project_group.id}/",
        {"userToRemove": users[0]},
        format="json",
    )
    assert response.status_code == 200
    assert maintainer_project_group.user_set.count() == 0
    assert reader_project_group.user_set.count() == 4

    # Remove an other one
    response = api_client_with_credentials.patch(
        f"/api/projects/{project_created_from_api.id}/groups/{reader_project_group.id}/",
        {"userToRemove": users[1]},
        format="json",
    )
    assert response.status_code == 200
    assert maintainer_project_group.user_set.count() == 0
    assert reader_project_group.user_set.count() == 3
