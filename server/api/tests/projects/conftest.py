import json
from api.models import ProjectRole
import pytest
from pytest_factoryboy import register

from rest_framework.response import Response
from rest_framework import status

# django app
from api.factories import ProjectFactory
from api.models import Project
from api.tests.utils import authenticate

register(ProjectFactory)


permission_to_http_status_code = {"add": 201, "delete": 204, "change": 200, "view": 200}


def getEmptyModelPermission():
    return {"view": 403, "change": 403, "delete": 403, "add": 403}


def getEmptyRole(role):
    return dict(
        zip(
            [
                "role",
                "project",
                "experiment",
                "analysis",
                "lodscore",
                "peak",
                "projectgroup",
                "coefficient",
                "haplotype",
                "team",
                "topsnp",
                "snpsassociation",
                "peakgene",
            ],
            [
                role,
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
                getEmptyModelPermission(),
            ],
        )
    )


def load_project_role():
    with open("api/fixtures/project-reader-permissions.json", "r") as reader:
        with open(
            "api/fixtures/project-maintainer-permissions.json", "r"
        ) as maintainer:
            with open("api/fixtures/project-manager-permissions.json", "r") as manager:
                project_reader_permissions_fix = json.loads(reader.read())
                project_maintainer_permissions_fix = json.loads(maintainer.read())
                project_manager_permissions_fix = json.loads(manager.read())
                project_role_permissions_fix = (
                    project_reader_permissions_fix
                    + project_maintainer_permissions_fix
                    + project_manager_permissions_fix
                )
                permissions_dict = dict()
                permissions_dict[None] = getEmptyRole(None)

                for pr_permission in project_role_permissions_fix:
                    print(pr_permission)
                    role = pr_permission["fields"]["projectrole"][0]
                    if role not in permissions_dict:
                        permissions_dict[role] = getEmptyRole(role)

                    # like project, experiement, lodscore
                    model_permission = pr_permission["fields"]["permission"][2]
                    # add, change, view, delete
                    permission = pr_permission["fields"]["permission"][0].split("_", 2)[
                        0
                    ]
                    permissions_dict[role][model_permission][
                        permission
                    ] = permission_to_http_status_code[permission]
                    # since it is control by project permissions some model need to have
                    # the same permissions as project
                    if model_permission == "project":
                        for model in ["lodscore", "peak"]:
                            permissions_dict[role][model][
                                permission
                            ] = permission_to_http_status_code[permission]

                # need to be 404 when not member
                permissions_dict[None]["project"]["view"] = 404
                import pprint

                pprint.pprint(permissions_dict)
                return list(permissions_dict.values())


@pytest.mark.django_db
@pytest.fixture
def project_build(project_factory):
    yield project_factory.build()


@pytest.mark.django_db
@pytest.fixture
def project_created_from_api_reponse(api_client_with_credentials, project_build):
    load_project_role()
    resp = api_client_with_credentials.post(
        "/api/projects/",
        {
            "project_name": project_build.project_name,
            "description": project_build.description,
            "creation_date": project_build.creation_date,
        },
        format="json",
    )
    yield resp


@pytest.mark.django_db
@pytest.fixture
def project_created_from_api(project_created_from_api_reponse):
    project_id = project_created_from_api_reponse.data["id"]
    project = Project.objects.get(id=project_id)
    yield project
    project.delete()


@pytest.fixture
def new_project_values():
    yield {"project_name": "new", "description": "new_desc"}


@pytest.mark.django_db
@pytest.fixture
def project_changed_from_api_reponse(
    api_client_with_credentials, project_created_from_api, new_project_values
):
    resp = api_client_with_credentials.put(
        f"/api/projects/{project_created_from_api.id}/",
        {
            "project_name": new_project_values["project_name"],
            "description": new_project_values["description"],
        },
        format="json",
    )
    yield resp


@pytest.mark.django_db
@pytest.fixture
def project_changed_from_api(project_changed_from_api_reponse):
    project = Project.objects.get(id=project_changed_from_api_reponse.data["id"])
    yield project
    project.delete()


@pytest.mark.django_db
@pytest.fixture(
    params=load_project_role(),
    ids=["not-member", "reader", "maintainer", "manager"],
)
def project_with_members_response(
    api_client,
    user_created_with_api,
    project_created_from_api,
    user_batch_created_with_api,
    user_password,
    request,
):
    # get available roles
    api_client_with_credentials = authenticate(
        api_client, user_created_with_api, user_password
    )

    if request.param["role"]:
        role = ProjectRole.objects.get(name=request.param["role"])
        # add user as member of a project
        project_group = role.project_group_set.get(
            project__id=project_created_from_api.id
        )
        users = [user.id for user in user_batch_created_with_api]
        # client = authenticate(api_client, user_that_create_project, user_password)
        response = api_client_with_credentials.patch(
            f"/api/projects/{project_created_from_api.id}/groups/{project_group.id}/",
            {"users": users},
            format="json",
        )
        yield (response, request.param)
    else:
        yield (Response({"status": "ok"}, status=status.HTTP_200_OK), request.param)
