def add_members_to_project(client, project_id, members, role):
    # get available roles
    resp = client.get("/api/project-roles/")
    assert resp.status_code == 200
    selected_role = [r for r in resp.data if r["name"] == role][0]
    resp = client.put(
        f"/api/projects/{project_id}/members/",
        {"users": members, "role": selected_role["id"]},
        format="json",
    )
