from api.models import ProjectGroup, ProjectRole
import pytest
from ..utils import authenticate


@pytest.mark.django_db
def test_projects_list(
    api_client, project_created_from_api, user_created_with_api, user_password
):
    api_client_with_credentials = authenticate(
        api_client, user_created_with_api, user_password
    )

    response = api_client_with_credentials.get("/api/projects/")
    assert response.status_code == 200
    assert len(response.data) == 1
    response = api_client_with_credentials.get(
        f"/api/projects/{project_created_from_api.id}/"
    )
    assert response.status_code == 200
    assert response.data["id"] == project_created_from_api.id


@pytest.mark.django_db
def test_create_project(project_created_from_api_reponse, project_build):
    assert project_created_from_api_reponse.status_code == 201
    assert (
        project_created_from_api_reponse.data["project_name"]
        == project_build.project_name
    )

    project_groups = ProjectGroup.objects.filter(
        project__id=project_created_from_api_reponse.data["id"]
    )
    project_roles = ProjectRole.objects.all()

    project_group_names_in_db = set([pg.name for pg in project_groups])

    expected_project_group_names = set(
        [f"{project_build.project_name}_{role.name}" for role in project_roles]
    )
    assert project_group_names_in_db == expected_project_group_names
    # assert False


@pytest.mark.django_db
def test_get_project(
    api_client,
    project_created_from_api,
    user_created_with_api,
    user_password,
    user_created_with_api_factory,
):
    api_client_with_credentials = authenticate(
        api_client, user_created_with_api, user_password
    )

    resp = api_client_with_credentials.get(
        f"/api/projects/{project_created_from_api.id}/"
    )
    assert resp.status_code == 200
    assert resp.data["project_name"] == project_created_from_api.project_name

    # test other user cannot see project
    other_user = user_created_with_api_factory()
    api_client_with_credentials = authenticate(api_client, other_user, user_password)
    resp = api_client_with_credentials.get(
        f"/api/projects/{project_created_from_api.id}/"
    )
    assert resp.status_code == 404


@pytest.mark.django_db
def test_change_project(
    project_changed_from_api_reponse, new_project_values, project_changed_from_api
):
    assert project_changed_from_api_reponse.status_code == 200
    assert project_changed_from_api.project_name == new_project_values["project_name"]
    assert project_changed_from_api.description == new_project_values["description"]


@pytest.mark.django_db
def test_role_permissions(
    api_client,
    project_with_members_response,
    user_batch_created_with_api,
    project_created_from_api,
    user_password,
):
    role_param = project_with_members_response[1]
    assert project_with_members_response[0].status_code == 200
    for i, member in enumerate(user_batch_created_with_api):
        # test view project
        client = authenticate(api_client, member, user_password)
        resp = client.get(f"/api/projects/{project_created_from_api.id}/")
        assert resp.status_code == role_param["project"]["view"]
        # test change project
        new_project_name = f"new-proj-{i}"
        resp = client.put(
            f"/api/projects/{project_created_from_api.id}/",
            {"project_name": new_project_name},
        )
        assert resp.status_code == role_param["project"]["change"]
        if role_param["project"]["change"] == 200:
            assert resp.data["project_name"] == new_project_name

    # test delete project
    client = authenticate(api_client, user_batch_created_with_api[0], user_password)
    resp = client.delete(f"/api/projects/{project_created_from_api.id}/")
    resp.status_code == role_param["project"]["delete"]
    if role_param["project"]["delete"] == 204:
        assert resp.data is None
        # Check if project group had been deleted
        project_group = ProjectGroup.objects.filter(
            project_id=project_created_from_api.id
        )
        assert project_group.exists() is False


@pytest.mark.django_db
def test_get_project_members(
    api_client, project_created_from_api, user_created_with_api, user_password
):
    api_client_with_credentials = authenticate(
        api_client, user_created_with_api, user_password
    )
    resp = api_client_with_credentials.get(
        f"/api/projects/{project_created_from_api.id}/members/",
        format="json",
    )
    assert resp.status_code == 200
    assert len(resp.data) == 1
    resp_data = list(resp.data)
    assert resp_data[0]["username"] == user_created_with_api.username
    assert resp_data[0]["id"] == user_created_with_api.id
