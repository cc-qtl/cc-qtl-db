import pytest
from api.tests.utils import authenticate


@pytest.mark.django_db
def test_coefficient_creation(
    analysis_created_from_api, coefficients_created_from_task_result
):
    assert coefficients_created_from_task_result[0] == analysis_created_from_api.id


@pytest.mark.django_db
def test_coefficent_permissions(
    api_client,
    user_batch_created_with_api,
    project_created_from_api,
    project_with_members_response,
    analysis_created_from_api,
    user_password,
    coefficient_created_from_task,
):
    if coefficient_created_from_task is None:
        pass
    else:
        for coeff in coefficient_created_from_task:
            role_param = project_with_members_response[1]
            expected = role_param["peak"]
            base_url = (
                f"/api/projects/{project_created_from_api.id}/analysis"
                + f"/{analysis_created_from_api.id}/peaks"
            )
            for i, member in enumerate(user_batch_created_with_api):
                client = authenticate(api_client, member, user_password)
                # test view experiment
                peak = coeff.peak
                url = f"{base_url}/{peak.id}/coefficients/"
                resp = client.get(url)
                assert resp.status_code == expected["view"]
                if expected["view"] == 200:
                    assert len(resp.data["coefficients"]) == 215
