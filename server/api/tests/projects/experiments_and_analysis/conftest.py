import json
import pytest

from api.factories import (
    AnalysisFactory,
    ExperimentFactory,
    PeakFactory,
    GalaxyInstanceFactory,
    GalaxyUserFactory,
    GalaxyWorkflowFactory,
)
from api.models import (
    Analysis,
    Coefficient,
    Experiment,
    GalaxyHistory,
    GalaxyWorkflow,
    GalaxyWorkflowInvocation,
    Haplotype,
    LodScore,
    Peak,
    SnpsAssociation,
    TopSnp,
)

# from api.tasks import (
#     load_coefficient,
#     load_haplotypes,
#     load_lod_score,
#     load_refine_peaks,
#     load_snps_association,
#     load_top_snps,
# )
from api.tests.utils import authenticate
from django.core.files.uploadedfile import SimpleUploadedFile
from pytest_factoryboy import register

register(ExperimentFactory)
register(AnalysisFactory)
register(PeakFactory)
register(GalaxyInstanceFactory)
register(GalaxyUserFactory)
register(GalaxyWorkflowFactory)

# EXPERIMENT PART


@pytest.fixture
def experiment_build(experiment_factory):
    yield experiment_factory.build()


@pytest.fixture
def experiment_build_factory(experiment_factory):
    def _experiment_build_factory():
        return experiment_factory.build()

    yield _experiment_build_factory


@pytest.mark.django_db
@pytest.fixture
def experiment_created_from_api_response(
    request,
    api_client_with_credentials,
    project_created_from_api,
    shared_datadir,
    experiment_build,
):
    marker = request.node.get_closest_marker("experiment_filename")
    if marker is None:
        # Handle missing marker in some way...
        filename = "only_lungs.csv"
    else:
        filename = marker.args[0]

    contents = shared_datadir / filename
    # TODO
    with open(contents, "rb") as fp:
        file = SimpleUploadedFile(
            content=fp.read(), name="experiment", content_type="multipart/form-data"
        )
        response = api_client_with_credentials.post(
            f"/api/projects/{project_created_from_api.id}/experiments/",
            {
                "file": file,
                "name": experiment_build.name,
                "description": experiment_build.description,
                "creation_date": experiment_build.creation_date,
            },
        )
    yield response


@pytest.mark.django_db
@pytest.fixture
def experiment_created_from_api(experiment_created_from_api_response):
    experiment = Experiment.objects.get(
        id=experiment_created_from_api_response.data["id"]
    )
    yield experiment
    experiment.delete()


@pytest.fixture
def analysis_build(analysis_factory):
    yield analysis_factory.build()


@pytest.fixture
def analysis_build_factory(analysis_factory):
    def _analysis_build_factory():
        return analysis_factory.build()

    yield _analysis_build_factory


@pytest.fixture
def galaxy_instance(galaxy_instance_factory):
    yield galaxy_instance_factory()


@pytest.fixture
def galaxy_user(galaxy_user_factory):
    yield galaxy_user_factory()


@pytest.fixture
def galaxy_workflow(galaxy_workflow_factory):
    yield galaxy_workflow_factory()


@pytest.fixture
def mock_analysis_wf(mocker):
    def mock_run_analysis(*args, **kwargs):
        analysis_id = args[5]
        analysis_db = Analysis.objects.get(pk=analysis_id)
        galaxy_history_db = GalaxyHistory(
            name=analysis_db.get_galaxy_history_name(),
            galaxy_user=analysis_db.galaxy_workflow.galaxy_user,
        )
        galaxy_history_db.save()
        analysis_db.galaxy_history = galaxy_history_db
        analysis_db.save()
        galaxy_workflow = GalaxyWorkflow.objects.first()
        wf_invocation_db = GalaxyWorkflowInvocation(
            galaxy_workflow=galaxy_workflow,
            galaxy_history=galaxy_history_db,
        )
        wf_invocation_db.save()
        return {
            "galaxy_history": galaxy_history_db.id,
            "galaxy_workflow": galaxy_workflow.id,
        }

    mocker.patch("api.models.Analysis.run_analyis", mock_run_analysis)
    yield mocker


@pytest.fixture(scope="function", params=["ok"])
def mock_show_history_factory(request, mocker):
    def _factory(*datasets):
        def _mock_show_history(*args, **kwargs):
            if kwargs["contents"]:
                return [
                    {
                        "name": dataset["name"],
                        "id": dataset["id"],
                        "state": request.param,
                    }
                    for dataset in datasets
                ]
            else:
                return {"name": "fake-history-name", "state": "error"}

        mocker.patch("api.tasks.gi.histories.show_history", _mock_show_history)
        # return mocker

    yield _factory


@pytest.fixture
def mock_mkdtemp_factory(mocker, request, shared_datadir):
    def _factory():
        def _mock_mkdtemp(*args, **kwargs):
            # TODO
            # message = f"Dataset state is not 'ok'. Dataset id: {dataset_id}, current state: {dataset['state']}"
            # if require_ok_state:
            #     raise DatasetStateException(message)
            return shared_datadir

        mocker.patch("api.galaxy.mkdtemp", _mock_mkdtemp)
        # yield mocker

    yield _factory


@pytest.fixture
def mock_download_dataset_factory(
    mocker,
    request,
    shared_datadir,
):
    def _factory():
        def _mock_download_dataset(*args, **kwargs):
            # TODO
            # message = f"Dataset state is not 'ok'. Dataset id: {dataset_id}, current state: {dataset['state']}"
            # if require_ok_state:
            #     raise DatasetStateException(message)
            file_path = shared_datadir / args[0]
            with open(file_path, "rb") as fp:
                return fp.read()

        mocker.patch("api.tasks.gi.datasets.download_dataset", _mock_download_dataset)
        # yield mocker

    yield _factory


@pytest.fixture
def mock_download_dataset_in_tmp_factory(
    mocker,
    request,
    shared_datadir,
):
    def _factory():
        def _mock_download_dataset(*args, **kwargs):
            # TODO
            return kwargs["file_path"]

        mocker.patch("api.tasks.gi.datasets.download_dataset", _mock_download_dataset)

    yield _factory


@pytest.fixture
def mock_get_history_status(mocker):
    def _mock_get_history_status(*args, **kwargs):
        return {"history_status": {"state": "ok"}}

    mocker.patch("api.views.get_history_status", _mock_get_history_status)
    yield mocker


@pytest.fixture
def mock_get_history_status_in_model(mocker):
    def _mock_get_history_status(*args, **kwargs):
        return {"history_status": {"state": "ok"}}

    mocker.patch("api.models.runner.get_history_status", _mock_get_history_status)
    yield mocker


@pytest.fixture
def mock_delete_history(mocker):
    def _mock_delete_history(*args, **kwargs):
        return {}

    mocker.patch("api.views.runner.delete_history", _mock_delete_history)
    yield mocker


@pytest.fixture
def analysis_tool_params():
    yield {
        "2": {"indiv_marker": "CC_MMnGM", "marker_set": "GM_m38", "xp_desc": "desc"},
        "3": {"add_covar": "Sex"},
        "4": {"n_perm": "5", "strat_covar": "Sex"},
        "5": {"adjust": "false", "level": "95", "user_drop": "1.5"},
        "6": {
            "annotations_path": "cc_annotations_m38",
            "k_top_snps": "3",
            "width_p": "1",
        },
    }


@pytest.fixture
def workflow_id():
    yield "657f009ad32a3f27"


@pytest.fixture
def analysis_post_payload(
    analysis_build,
    experiment_created_from_api,
    analysis_tool_params,
    galaxy_workflow,
):
    # analysis_build = analysis_build_factory()
    phenotype = experiment_created_from_api.phenotypes.get(name="logNKnb.lungs")

    yield {
        "name": analysis_build.name,
        "galaxy_history": None,
        "description": analysis_build.description,
        "experiments": [experiment_created_from_api.id],
        "toolsParams": analysis_tool_params,
        "galaxy_workflow": galaxy_workflow.id,
        "phenotypes": [phenotype.id],
    }


@pytest.fixture(
    params=["1"],
    ids=["v0.0.0_beta.2"],
)
def analysis_created_from_api_response(
    api_client,
    user_created_with_api,
    project_created_from_api,
    galaxy_workflow,
    experiment_created_from_api,
    analysis_build,
    user_password,
    analysis_tool_params,
    # mock_analysis_wf,
    analysis_factory,
    request,
):
    client = authenticate(api_client, user_created_with_api, user_password)
    phenotype = experiment_created_from_api.phenotypes.get(name="logNKnb.lungs")
    url = f"/api/projects/{project_created_from_api.id}/analysis/"

    analysis = analysis_factory()
    print(analysis)

    response = client.post(
        url,
        {
            "name": analysis_build.name,
            "galaxy_history": None,
            "description": analysis_build.description,
            "experiments": [experiment_created_from_api.id],
            "toolsParams": analysis_tool_params,
            "galaxy_workflow": galaxy_workflow.id,
            "phenotypes": [phenotype.id],
        },
        format="json",
    )
    yield response


@pytest.fixture
def analysis_created_from_api(analysis_created_from_api_response):
    analysis = Analysis.objects.get(id=analysis_created_from_api_response.data["id"])
    yield analysis
    analysis.delete()


@pytest.fixture
def lodscores_created_from_task_result(
    analysis_created_from_api,
    mock_mkdtemp_factory,
    mock_download_dataset_in_tmp_factory,
    mock_show_history_factory,
):
    mock_mkdtemp_factory()
    mock_show_history_factory({"name": "lod.csv", "id": "lod"})
    mock_download_dataset_in_tmp_factory()
    # yield load_lod_score(
    #     (
    #         analysis_created_from_api.id,
    #         analysis_created_from_api.galaxy_history_id,
    #         "",
    #     )
    # )


@pytest.fixture
def lodscores_created_from_task(lodscores_created_from_task_result):
    if lodscores_created_from_task_result[2] is not None:
        yield LodScore.objects.get(id=lodscores_created_from_task_result[2])
    else:
        yield lodscores_created_from_task_result[2]


@pytest.fixture
def peak_build(peak_factory):
    yield peak_factory.build()


@pytest.fixture
def peaks_created_from_task_result(
    analysis_created_from_api,
    mock_show_history_factory,
    mock_mkdtemp_factory,
    mock_download_dataset_in_tmp_factory,
):
    mock_mkdtemp_factory()
    mock_show_history_factory({"name": "refine-peaks.csv", "id": "peaks.csv"})
    mock_download_dataset_in_tmp_factory()

    # yield load_refine_peaks(
    #     (
    #         analysis_created_from_api.id,
    #         analysis_created_from_api.galaxy_history_id,
    #         "",
    #     )
    # )


@pytest.fixture
def peaks_created_from_task(peaks_created_from_task_result):
    peaks = Peak.objects.filter(id__in=peaks_created_from_task_result[2])
    yield peaks


@pytest.fixture
def coefficients_created_from_task_result(
    analysis_created_from_api,
    peaks_created_from_task_result,
    mock_show_history_factory,
    # mock_download_dataset_factory,
    mock_mkdtemp_factory,
    mock_download_dataset_in_tmp_factory,
):
    mock_mkdtemp_factory()
    # show_history
    # download_dataset
    mock_show_history_factory(
        {
            "name": "var--matched_data.PHENO...input.phenocol.--chr--13--pos--93912931--coef.csv",
            "id": "coef.csv",
        }
    )
    mock_download_dataset_in_tmp_factory()
    # yield load_coefficient(
    #     (analysis_created_from_api.id, analysis_created_from_api.galaxy_history_id, "")
    # )


@pytest.fixture
def coefficient_created_from_task(coefficients_created_from_task_result):
    if len(coefficients_created_from_task_result[2]) > 0:
        yield Coefficient.objects.filter(
            id__in=coefficients_created_from_task_result[2]
        )
    else:
        yield None


@pytest.fixture
def haplotypes_created_from_task_result(
    analysis_created_from_api,
    peaks_created_from_task_result,
    mock_show_history_factory,
    mock_mkdtemp_factory,
    mock_download_dataset_in_tmp_factory,
):
    mock_mkdtemp_factory()
    # show_history
    # download_dataset
    mock_show_history_factory(
        {
            "name": "var--matched_data.PHENO...input.phenocol.--chr--13--pos--93912931--haplo.csv",
            "id": "haplo.csv",
        }
    )
    mock_download_dataset_in_tmp_factory()
    # yield load_haplotypes(
    #     (analysis_created_from_api.id, analysis_created_from_api.galaxy_history_id, "")
    # )


@pytest.fixture
def haplotypes_created_from_task(haplotypes_created_from_task_result):
    if len(haplotypes_created_from_task_result[2]) > 0:
        yield Haplotype.objects.filter(id__in=haplotypes_created_from_task_result[2])

    else:
        yield None


@pytest.fixture
def snps_assoc_created_from_task_result(
    analysis_created_from_api,
    peaks_created_from_task_result,
    mock_mkdtemp_factory,
    mock_show_history_factory,
    mock_download_dataset_in_tmp_factory,
):
    mock_mkdtemp_factory()
    # show_history
    # download_dataset
    mock_show_history_factory(
        {
            "name": "var--matched_data.PHENO...input.phenocol.--chr--13--pos--93912931--snps_assoc.csv",
            "id": "snps_assoc.csv",
        }
    )
    mock_download_dataset_in_tmp_factory()
    # yield load_snps_association(
    #     (
    #         analysis_created_from_api.id,
    #         analysis_created_from_api.galaxy_history_id,
    #         "",
    #     )
    # )


@pytest.fixture
def snps_assoc_created_from_task(snps_assoc_created_from_task_result):
    if len(snps_assoc_created_from_task_result[2]) > 0:
        yield SnpsAssociation.objects.filter(
            id__in=snps_assoc_created_from_task_result[2]
        )
    else:
        yield None


# TOP SNPS
@pytest.fixture
def top_snps_created_from_task_result(
    analysis_created_from_api,
    peaks_created_from_task_result,
    mock_mkdtemp_factory,
    mock_show_history_factory,
    mock_download_dataset_in_tmp_factory,
):
    mock_mkdtemp_factory()
    # show_history
    # download_dataset
    mock_show_history_factory(
        {
            "name": "var--matched_data.PHENO...input.phenocol.--chr--13--pos--93912931--top_snps.csv",
            "id": "top_snps.csv",
        }
    )
    mock_download_dataset_in_tmp_factory()
    # yield load_top_snps(
    #     (
    #         analysis_created_from_api.id,
    #         analysis_created_from_api.galaxy_history_id,
    #         "",
    #     )
    # )


@pytest.fixture
def top_snps_created_from_task(top_snps_created_from_task_result):
    if len(top_snps_created_from_task_result[2]) > 0:
        yield TopSnp.objects.filter(id__in=top_snps_created_from_task_result[2])
    else:
        yield None
