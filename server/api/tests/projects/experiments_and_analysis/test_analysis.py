import pytest
from api.tests.utils import authenticate
from api.models import Analysis


@pytest.mark.django_db
def test_create_analysis_response(analysis_created_from_api_response):
    assert analysis_created_from_api_response.status_code == 201


@pytest.mark.django_db
def test_create_analysis(analysis_created_from_api, analysis_build):
    assert analysis_created_from_api.name == analysis_build.name


@pytest.mark.django_db
def test_analysis_permissions(
    api_client,
    user_batch_created_with_api,
    project_created_from_api,
    project_with_members_response,
    experiment_created_from_api,
    # analysis_build_factory,
    analysis_created_from_api,
    user_password,
    # analysis_tool_params,
    # analysis_created_from_api_response,
    analysis_post_payload,
):
    role_param = project_with_members_response[1]
    expected = role_param["analysis"]
    base_url = f"/api/projects/{project_created_from_api.id}/analysis"
    url = f"{base_url}/{analysis_created_from_api.id}/"
    for i, member in enumerate(user_batch_created_with_api):
        client = authenticate(api_client, member, user_password)
        # test view experiment
        resp = client.get(url)
        assert resp.status_code == expected["view"]

        # test change experiment
        new_name = f"new-name-{i}"
        resp = client.patch(
            url,
            {"name": new_name},
        )
        assert resp.status_code == expected["change"]
        if expected["change"] == 200:
            assert resp.data["name"] == new_name
        # Create analysis
        # new_analysis = analysis_build_factory()
        # analysis_created_from_api_response
        resp = client.post(
            base_url + "/",
            analysis_post_payload,
            format="json",
        )
        assert resp.status_code == expected["add"]

    # test delete experiment
    client = authenticate(api_client, user_batch_created_with_api[0], user_password)

    resp = client.delete(url)
    resp.status_code == expected["delete"]
    if expected["delete"] == 204:
        assert resp.data is None
        # Check if project group had been deleted
        record = Analysis.objects.filter(id__exact=experiment_created_from_api.id)
        assert record.exists() is False
