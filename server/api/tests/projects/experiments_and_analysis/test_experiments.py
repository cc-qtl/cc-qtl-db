from guardian.shortcuts import get_perms
from api.models import Experiment
import pytest
from api.tests.utils import authenticate
from django.core.files.uploadedfile import SimpleUploadedFile


@pytest.mark.django_db
class TestExperiment:
    def test_create_experiment(self, experiment_created_from_api_response):
        # assert False
        assert experiment_created_from_api_response.status_code == 201

    @pytest.mark.experiment_filename("only_lungs_with_bad_ccline.csv")
    def test_create_experiment_bad_ccline(self, experiment_created_from_api_response):
        assert experiment_created_from_api_response.status_code == 400

    def test_experiment_permissions(
        self,
        api_client,
        project_created_from_api,
        project_with_members_response,
        experiment_created_from_api,
        experiment_build_factory,
        user_batch_created_with_api,
        user_password,
        shared_datadir,
    ):
        role_param = project_with_members_response[1]
        expected = role_param["experiment"]
        base_url = f"/api/projects/{project_created_from_api.id}/experiments"
        url = f"{base_url}/{experiment_created_from_api.id}/"

        for i, member in enumerate(user_batch_created_with_api):
            client = authenticate(api_client, member, user_password)

            # test view experiment
            resp = client.get(url)
            assert resp.status_code == expected["view"]

            # test change experiment
            new_exp_name = f"new-exp-{i}"
            resp = client.patch(
                url,
                {"name": new_exp_name},
            )
            assert resp.status_code == expected["change"]
            if expected["change"] == 200:
                assert resp.data["name"] == new_exp_name

            # Create experiment
            filename = "only_lungs.csv"
            contents = shared_datadir / filename
            new_experiment_build = experiment_build_factory()
            with open(contents, "rb") as fp:
                file = SimpleUploadedFile(
                    content=fp.read(),
                    name="experiment",
                    content_type="multipart/form-data",
                )
                response = client.post(
                    f"/api/projects/{project_created_from_api.id}/experiments/",
                    {
                        "file": file,
                        "name": new_experiment_build.name,
                        "description": new_experiment_build.description,
                        "creation_date": new_experiment_build.creation_date,
                    },
                )
                assert response.status_code == expected["add"]
                # print(f"{member} - {role_param['role']}")
                # print(response)
                # assert False
                if expected["add"] == 201:
                    exp_id = response.data["id"]
                    # Try to get this experiment
                    resp = client.get(f"{base_url}/{exp_id}/")
                    assert resp.status_code == 200

                    # test delete experiment
                    # client = authenticate(api_client, user_batch_created_with_api[0], user_password)
                    resp = client.delete(f"{base_url}/{exp_id}/")
                    resp.status_code == expected["delete"]
                    if expected["delete"] == 204:
                        assert resp.data is None
                        # Check if project group had been deleted
                        experiment = Experiment.objects.filter(id__exact=exp_id)
                        assert experiment.exists() is False
        # test delete experiment
        client = authenticate(api_client, user_batch_created_with_api[0], user_password)
        resp = client.delete(url)
        resp.status_code == expected["delete"]
        if expected["delete"] == 204:
            assert resp.data is None
            # Check if project group had been deleted
            experiment = Experiment.objects.filter(
                id__exact=experiment_created_from_api.id
            )
            assert experiment.exists() is False

    @pytest.mark.parametrize("expected", [(403)])
    def test_not_member_user(
        self,
        api_client,
        user_created_with_api_factory,
        project_created_from_api,
        experiment_created_from_api,
        user_password,
        shared_datadir,
        experiment_build_factory,
        expected,
    ):
        other_user = user_created_with_api_factory()
        client = authenticate(api_client, other_user, user_password)
        base_url = f"/api/projects/{project_created_from_api.id}/experiments"
        url = f"{base_url}/{experiment_created_from_api.id}/"
        # try get experiment
        response = client.get(url)
        assert response.status_code == expected

        # try delete
        response = client.delete(url)
        assert response.status_code == expected

        # try change
        response = client.put(url, {"name": "change-exp"})
        assert response.status_code == expected

        # add experiment
        contents = shared_datadir / "only_lungs.csv"
        with open(contents, "rb") as fp:
            file = SimpleUploadedFile(
                content=fp.read(), name="experiment", content_type="multipart/form-data"
            )
            experiment_build = experiment_build_factory()
            response = client.post(
                f"{base_url}/",
                {
                    "file": file,
                    "name": experiment_build.name,
                    "description": experiment_build.description,
                    "creation_date": experiment_build.creation_date,
                },
            )
            assert response.status_code == expected
