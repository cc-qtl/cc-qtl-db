import pytest

from api.tests.utils import authenticate


# @pytest.mark.django_db
# def test_lodscores_not_loaded(
#     api_client,
#     user_batch_created_with_api,
#     project_created_from_api,
#     project_with_members_response,
#     analysis_created_from_api,
#     user_password,
#     mock_show_history_factory,
#     mock_download_dataset_factory,
#     celery_app,
#     celery_worker,
# ):
#     # mock the fact there is those 2 files in the Galaxy history
#     mock_show_history_factory(
#         {"name": "xxxx_lod.csv", "id": "lodscores.csv"},
#         {"name": "xxxx_significance-threshold.csv", "id": "significance-threshold.csv"},
#     )
#     mock_download_dataset_factory()
#     role_param = project_with_members_response[1]
#     expected = role_param["lodscores"]
#     base_url = (
#         f"/api/projects/{project_created_from_api.id}/analysis"
#         + f"/{analysis_created_from_api.id}/lodscores"
#     )
#     # url = f"{base_url}/{lodscores_created_from_task.id}/"
#     url = f"{base_url}/"
#     for i, member in enumerate(user_batch_created_with_api):
#         client = authenticate(api_client, member, user_password)
#         # test view experiment
#         resp = client.get(url)
#         assert resp.status_code == expected["view"]
#         if expected["view"] == 200:
#             assert len(resp.data["lod_scores"]) == 100


@pytest.mark.django_db
def test_lodscores_permissions(
    api_client,
    user_batch_created_with_api,
    project_created_from_api,
    project_with_members_response,
    analysis_created_from_api,
    user_password,
    lodscores_created_from_task,
):
    if lodscores_created_from_task is None:
        pass
    else:
        role_param = project_with_members_response[1]
        expected = role_param["lodscore"]
        base_url = (
            f"/api/projects/{project_created_from_api.id}/analysis"
            + f"/{analysis_created_from_api.id}/lodscores"
        )
        # url = f"{base_url}/{lodscores_created_from_task.id}/"
        url = f"{base_url}/"
        for i, member in enumerate(user_batch_created_with_api):
            client = authenticate(api_client, member, user_password)
            # test view experiment
            resp = client.get(url)
            assert resp.status_code == expected["view"]
            if expected["view"] == 200:
                assert len(resp.data["lod_scores"]) == 100
