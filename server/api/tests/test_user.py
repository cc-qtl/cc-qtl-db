import pytest
from .utils import authenticate


@pytest.mark.django_db
@pytest.mark.parametrize("expected", [200])
def test_with_superuser_client(api_client_with_superuser_credential, expected):
    response = api_client_with_superuser_credential.get("/api/current-user/")
    assert response.status_code == expected
    assert response.data["id"] == 2


@pytest.mark.django_db
def test_with_client(api_client_with_credentials, user_logged):
    response = api_client_with_credentials.get("/api/current-user/")
    assert response.status_code == 200
    assert response.data["id"] == user_logged.id


@pytest.mark.django_db
def test_user_permissions(
    api_client, user_batch_created_with_api, user_password, normaluser_build
):
    for user in user_batch_created_with_api:
        # authenticate as a user
        client = authenticate(api_client, user, user_password)
        # other_users = (u for u in user_batch_created_with_api if u.id != user.id)
        other_users = (u for u in user_batch_created_with_api if u.id != user.id)
        # Can view all users
        response = client.get("/api/users/")
        assert response.status_code == 200
        assert len(response.data) == 6

        # cannot add a new user
        response = client.post(
            "/api/users/",
            data={
                "username": normaluser_build.username,
                "first_name": normaluser_build.first_name,
                "last_name": normaluser_build.last_name,
                "email": normaluser_build.email,
                "is_active": normaluser_build.is_active,
                "password": user_password,
            },
        )
        assert response.status_code == 403

        # Cannot delete himself
        response = client.delete(
            f"/api/users/{user.id}/",
        )
        assert response.status_code == 403

        # TODO DIDN'T FIND HOW TO DO THAT
        # Can change himself
        # new_username = "new-username"
        # client = authenticate(api_client, user, user_password)
        # print(user.has_perm("api.change_user", user))
        # response = client.put(
        #     f"/api/users/{user.id}/",
        #     data={
        #         "username": new_username,
        #         # "first_name": normaluser_build.first_name,
        #         # "last_name": normaluser_build.last_name,
        #         # "email": normaluser_build.email,
        #         # "is_active": normaluser_build.is_active,
        #         "password": user_password,
        #     },
        # )
        # assert response.status_code == 200
        # assert response.data["username"] == new_username

        for other_user in other_users:
            # can view user
            response = client.get(f"/api/users/{other_user.id}/")
            assert response.status_code == 200
            assert response.data["username"] == other_user.username

            # Cannot change user
            response = client.put(
                f"/api/users/{other_user.id}/", {"username": "new-username"}
            )
            assert response.status_code == 403

            # cannot delete user
            response = client.delete(f"/api/users/{other_user.id}/")
            assert response.status_code == 403

            # change password other user
            response = client.put(
                f"/api/users/{other_user.id}/password_change/",
                {
                    "old_password": user_password,
                    "new_password": user_password,
                    "new_password_confirmation": user_password,
                },
            )
            assert response.status_code == 403

        # change password
        # client = authenticate(api_client, user, user_password)
        new_password = "new_password"
        response = client.put(
            f"/api/users/{user.id}/password_change/",
            {
                "old_password": user_password,
                "new_password": new_password,
                "new_password_confirmation": new_password,
            },
        )
        assert response.status_code == 200

        response = client.put(
            f"/api/users/{user.id}/password_change/",
            {
                "old_password": "wrong_password",
                "new_password": user_password,
                "new_password_confirmation": user_password,
            },
        )

        assert response.status_code == 400
        assert response.data["old_password"]["old_password"] is not None

        response = client.put(
            f"/api/users/{user.id}/password_change/",
            {
                "old_password": new_password,
                "new_password": user_password,
                "new_password_confirmation": user_password + "wrong",
            },
        )
        assert response.status_code == 400
        assert response.data["password"] is not None


@pytest.mark.django_db
def test_change_user(api_client_with_credentials, user_logged, user_password):
    current_user_resp = api_client_with_credentials.get("/api/current-user/")
    user_id = current_user_resp.data["id"]
    assert user_id == user_logged.id
    user_resp = api_client_with_credentials.get(f"/api/users/{user_id}/")
    assert user_resp.status_code == 200
    # print(user_logged.groups.all())
    assert user_logged.has_perm("api.change_user", user_logged)

    # Change current_user
    new_firstname = "foo"
    change_user_resp = api_client_with_credentials.put(
        f"/api/users/{user_id}/",
        data={
            "username": "test-user",
            "first_name": new_firstname,
            "password": user_password,
        },
    )
    assert change_user_resp.status_code == 403
    # assert change_user_resp.data["first_name"] == new_firstname

    # Try to change other user
    change_other_user_resp = api_client_with_credentials.put(
        "/api/users/5/",
        data={
            "username": "test-user",
            "first_name": new_firstname,
            "password": user_password,
        },
    )
    assert change_other_user_resp.status_code == 403


@pytest.mark.django_db
def test_add_user(api_client_with_credentials, user_logged):
    # If not manager, cannot
    response = api_client_with_credentials.post(
        "/api/users/", data={"username": "user-added", "password": "password"}
    )
    assert response.status_code == 403
    assert not user_logged.has_perm("api.add_user"), "Cannot add user"


@pytest.mark.django_db
def test_delete_user(api_client_with_credentials, user_logged):
    response = api_client_with_credentials.get("/api/current-user/")
    user_id = response.data["id"]
    # response = api_client_with_credentials.delete("/api/users/5/",)
    # assert response.status_code == 403

    response = api_client_with_credentials.delete(
        f"/api/users/{user_id}/",
    )
    assert response.status_code == 403
    assert not user_logged.has_perm("api.delete_user"), "Can delete user, problem"


@pytest.mark.django_db
def test_count_users(api_client_with_credentials):
    response = api_client_with_credentials.get("/api/users/")
    assert len(response.data) == 2
