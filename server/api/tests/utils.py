from django.urls import reverse


def authenticate(client, user, password):
    url = reverse("token_obtain_pair")
    resp = client.post(
        url, {"username": user.username, "password": password}, format="json"
    )
    client.credentials(HTTP_AUTHORIZATION="Bearer " + resp.data["access"])
    return client


def create_user_via_api(client, token, user_payload):
    client.credentials(HTTP_AUTHORIZATION="Bearer " + token)
    return client.post("/api/users/", data=user_payload)
