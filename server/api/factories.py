from django.contrib.auth import get_user_model
from factory import (
    PostGenerationMethodCall,
    Sequence,
    Iterator,
    SubFactory,
    LazyAttribute,
    Faker,
)
from factory.django import DjangoModelFactory


# Api
from .models import (
    Analysis,
    AnalysisCategory,
    Experiment,
    GalaxyInstance,
    GalaxyUser,
    GalaxyWorkflow,
    Peak,
    PhenotypeCategory,
    Project,
    Role,
    Team,
    ProjectRole,
)

from django.conf import settings


User = get_user_model()
Faker._DEFAULT_LOCALE = "fr_FR"

USER_PASSWORD = "password"


class SuperuserFactory(DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ("username", "first_name", "last_name")

    username = Faker("user_name")
    first_name = Faker("first_name")
    last_name = Faker("last_name")
    is_superuser = True
    password = PostGenerationMethodCall("set_password", USER_PASSWORD)
    is_active = True
    is_staff = True
    email = Faker("email")


class UserStaffFactory(DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ("username",)

    username = Faker("user_name")
    first_name = Faker("first_name")
    last_name = Faker("last_name")

    is_superuser = False
    password = PostGenerationMethodCall("set_password", USER_PASSWORD)
    is_active = True
    is_staff = True
    email = Faker("email")


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ("username", "first_name", "last_name")

    username = Faker("user_name")
    first_name = Faker("first_name")
    last_name = Faker("last_name")
    is_superuser = False
    password = PostGenerationMethodCall("set_password", USER_PASSWORD)
    is_active = True
    is_staff = False
    email = LazyAttribute(lambda obj: "%s@pasteur.fr" % obj.username)


class ProjectFactory(DjangoModelFactory):
    class Meta:
        model = Project
        django_get_or_create = ("project_name",)

    project_name = Faker("word")
    description = Faker("sentence", nb_words=15)
    creation_date = Faker("date_time")


class ExperimentFactory(DjangoModelFactory):
    class Meta:
        model = Experiment

    name = Faker("word")
    description = Faker("sentence", nb_words=15)
    creation_date = Faker("date_time")


class GalaxyInstanceFactory(DjangoModelFactory):
    class Meta:
        model = GalaxyInstance

    name = "Galaxy@Pasteur"
    url = "https://galaxy.pasteur.fr"
    description = "Pasteur Galaxy instance"


class GalaxyUserFactory(DjangoModelFactory):
    class Meta:
        model = GalaxyUser

    username = settings.GALAXY_USERNAME
    api_key = Faker("md5")
    galaxy_instance = SubFactory(GalaxyInstanceFactory)


class AnalysisCategoryFactory(DjangoModelFactory):
    class Meta:
        model = AnalysisCategory
        django_get_or_create = ("name",)

    name = "ccqtl"
    description = "a desc"


class GalaxyWorkflowFactory(DjangoModelFactory):
    class Meta:
        model = GalaxyWorkflow

    name = Faker("word")
    version = Faker("random_number", digits=3)
    galaxy_user = SubFactory(GalaxyUserFactory)
    analysis_category = SubFactory(AnalysisCategoryFactory)


def sequence(number):
    """
    :param number:
    :return: a dict that contains random data
    """
    return {"1": {"toto": "val"}, "2": {"foo": "bar"}}


class AnalysisFactory(DjangoModelFactory):
    class Meta:
        model = Analysis

    name = Faker("word")
    creation_date = Faker("date_time")
    description = Faker("sentence", nb_words=15)
    galaxy_workflow = SubFactory(GalaxyWorkflowFactory)
    tools_params = Sequence(sequence)


class PeakFactory(DjangoModelFactory):
    class Meta:
        model = Peak

    variable_name = Faker("word")
    chromosome = Faker("random_digit_not_null")
    position = Faker("pyfloat", positive=True, left_digits=1, max_value=1000000.0)
    lod = Faker("pyfloat", positive=True, left_digits=5, min_value=0, max_value=10)
    ci_lo = Faker("pyfloat", positive=True, min_value=0, max_value=1000.0)
    ci_hi = Faker("pyfloat", positive=True, min_value=0, max_value=1000.0)


class RoleFactory(DjangoModelFactory):
    class Meta:
        model = Role
        django_get_or_create = ("name", "priority")

    name = "reader"
    priority = 1


# ProjectRole
class ProjectRoleFactory(DjangoModelFactory):
    class Meta:
        model = ProjectRole
        django_get_or_create = ("name", "priority")


class TeamFactory(DjangoModelFactory):
    class Meta:
        model = Team
        django_get_or_create = ("name",)

    project_role = Iterator(ProjectRole.objects.filter(name__exact="reader"))


class TeamSampleFactory(TeamFactory):
    project_role = Iterator(ProjectRole.objects.filter(name__exact="reader"))
    name = Sequence(lambda n: "Team %d" % n)


class PhenotypeCategoryFactory(DjangoModelFactory):
    class Meta:
        model = PhenotypeCategory
        django_get_or_create = ("name",)

    name = Faker("word")
    description = Faker("sentence", nb_words=15)
    datatype = "Integer"
    dataclass = "Phenotype"
    nature = "Quantitative"
    location = "Organ"
