# Django
from django.db import models
from django.contrib.auth.models import Permission, Group, AbstractUser
from django.db import transaction
from django.utils import timezone
from django.db.utils import IntegrityError


# guardian
from guardian.shortcuts import assign_perm, remove_perm
from guardian.models import UserObjectPermissionBase
from guardian.models import GroupObjectPermissionBase


from api.exceptions import RessourceAlreadyExists
from api.managers import ProjectRoleManager, RoleGroupManager, RoleManager


class User(AbstractUser):
    pass


class ProjectManager(models.Manager):
    def get_by_natural_key(self, project_name):
        return self.get(project_name=project_name)

    @transaction.atomic
    def create(self, *args, **kwargs):
        creator = kwargs.pop("creator")
        project = super().create(*args, **kwargs)
        project_roles = ProjectRole.objects.all()
        for project_role in project_roles:
            project_group = ProjectGroup(
                project=project,
                project_role=project_role,
                name=project.project_name + "_" + str(project_role),
            )
            project_group.save()

            # assign group permissions to this instance
            # project_group.add_permissions()

            # add the user to a this group
            if str(project_role) == "manager":
                project_group.user_set.add(creator)

        # Add project group permissions
        # (How a role can add/remove/change project role)
        # ProjectGroup.objects.add_projectgroup_permissions_per_project(project)
        # ProjectGroup.objects.add_permissions_per_project(project)
        project.add_permissions()
        return project

    def add_permissions(self):
        for project in self.all():
            project.add_permissions()

    def clear_permissions(self):
        for project in self.all():
            project.clear_permissions()

    def update_permissions(self):
        self.clear_permissions()
        self.add_permissions()


class Project(models.Model):
    """
    Stores the cc-qtl projects
    """

    # A project belongs to one group
    # group = models.OneToOneField(Group, on_delete=models.CASCADE)
    project_name = models.CharField(max_length=200, unique=True)
    description = models.TextField(blank=True)
    creation_date = models.DateTimeField(default=timezone.now)
    # objects = models.Manager()
    objects = ProjectManager()

    def __str__(self):
        return self.project_name

    class Meta:
        ordering = ["creation_date"]

    def add_permissions(self):
        ProjectGroup.objects.add_permissions_per_project(self)

    def clear_permissions(self):
        ProjectGroup.objects.clear_permissions_per_project(self)

    def update_permissions(self):
        self.clear_permissions()
        self.add_permissions()

    def delete(self, *args, **kwargs):
        # delete the analysis
        for analysis in self.analysis.all():
            analysis.delete()
        super().delete(*args, **kwargs)  # Call the "real" delete() method.

    def natural_key(self):
        return (self.project_name,)


# Add foreign keys to handle permission deletion when object is deleted
class ProjectUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Project, on_delete=models.CASCADE)


class ProjectGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Project, on_delete=models.CASCADE)


class ProjectRole(models.Model):
    """
    Stores roles
    """

    permissions = models.ManyToManyField(Permission)
    name = models.CharField(max_length=128, unique=True)
    priority = models.PositiveIntegerField()

    objects = ProjectRoleManager()

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)


class ProjectGroupManager(models.Manager):
    def check_has_projectgroup_per_role(self, project):
        project_groups = self.filter(project=project)
        project_roles = ProjectRole.objects.all()
        # check if each role has a project_group associated
        project_group_role_names = set([pg.project_role.name for pg in project_groups])
        role_names = set([pr.name for pr in project_roles])
        if project_group_role_names != role_names:
            raise Exception(f"No group for each role in project {project}")

    def add_permissions_per_project(self, project):
        # get the project group associated to a projec
        self.check_has_projectgroup_per_role(project)
        project_groups = self.filter(project=project)
        for pg in project_groups:
            pg.add_permissions()
        self.add_projectgroup_permissions_per_project(project)

    def clear_permissions_per_project(self, project):
        project_groups = self.filter(project=project)
        for pg in project_groups:
            pg.clear_permissions()
        self.clear_projectgroup_permissions_per_project(project)

    def update_permissions_per_project(self, project):
        self.clear_permissions_per_project(project)
        self.add_permissions_per_project(project)

    # Permissions for projectgroup themselves
    def add_projectgroup_permissions(self):
        for project in Project.objects.all():
            self.add_projectgroup_permissions_per_project(project)

    def clear_projectgroup_permissions(self):
        # Get all projectgroup permissions
        for project in Project.objects.all():
            self.clear_projectgroup_permissions_per_project(project)

    def update_projectgroup_permissions(self):
        self.clear_projectgroup_permissions()
        self.add_projectgroup_permissions()

    def add_projectgroup_permissions_per_project(self, project):
        project_groups = self.filter(project=project)
        self.check_has_projectgroup_per_role(project)
        for pg_set_perm in project_groups:
            projectgroup_permissions = pg_set_perm.project_role.permissions.filter(
                content_type__model="projectgroup"
            )
            for perm in projectgroup_permissions:
                for pg_object in project_groups:
                    assign_perm(perm, pg_set_perm, pg_object)

    def clear_projectgroup_permissions_per_project(self, project):
        all_permissions = Permission.objects.filter(content_type__model="projectgroup")
        project_groups = self.filter(project=project)
        for pg_remove_perms in project_groups:
            for obj in project_groups:
                for perm in all_permissions:
                    remove_perm(perm, pg_remove_perms, obj)


class ProjectGroup(Group):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="project_group_set"
    )

    project_role = models.ForeignKey(
        ProjectRole, on_delete=models.CASCADE, related_name="project_group_set"
    )
    objects = ProjectGroupManager()

    class Meta:
        unique_together = [["project", "project_role"]]

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name, self.project, self.project_role)

    def add_permissions(self):
        models = [
            "experiment",
            "analysis",
            "peak",
            "lodscore",
            "projectgroup",
            "haplotype",
            "coefficient",
            "topsnp",
            "snpsassociation",
            "peakgene",
        ]

        permissions = self.project_role.permissions.filter(
            content_type__model="project"
        )

        for project_perm in permissions:
            assign_perm(project_perm, self, self.project)
            assign_perm(project_perm, self)

        for model in models:
            # experiment permissions
            model_permissions = self.project_role.permissions.filter(
                content_type__model=model
            )
            for perm in model_permissions:
                assign_perm(perm, self)

    def clear_permissions(self):
        # delete all permission for this group
        self.permissions.clear()

        # Delete specific permissions for linked project
        ProjectGroupObjectPermission.objects.filter(
            content_object=self.project, group=self
        ).delete()

    def update_permission(self):
        self.clear_permissions()
        self.add_permissions()


class Role(models.Model):
    """
    Stores roles
    """

    objects = RoleManager()

    permissions = models.ManyToManyField(Permission)
    name = models.CharField(max_length=128, unique=True)
    priority = models.PositiveIntegerField()

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)


class RoleGroup(Group):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    objects = RoleGroupManager()

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name, self.role)


class Team(Group):
    objects = models.Manager()
    projects = models.ManyToManyField(Project, related_name="teams")
    project_role = models.ForeignKey(
        ProjectRole, blank=True, null=True, on_delete=models.CASCADE
    )

    class Meta:
        unique_together = [["group_ptr"]]

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # get the basic permission
        # should have reader project permissions
        try:
            with transaction.atomic():
                # project_reader_role = ProjectRole.objects.get(name="reader")
                permissions = self.project_role.permissions.all()
                for perm in permissions:
                    assign_perm(perm, self)
        except IntegrityError:
            raise RessourceAlreadyExists()
