import csv
from datetime import datetime
from decimal import Decimal
import tempfile
import uuid


# Pandas
import pandas as pd

from django.db import models

from django.utils import timezone
from django.db import transaction
from django.db.utils import IntegrityError
from django.conf import settings

# Guardian
from guardian.models import UserObjectPermissionBase
from guardian.models import GroupObjectPermissionBase
from guardian.shortcuts import assign_perm

# bioblend
from bioblend import ConnectionError


from api.models import (
    GalaxyWorkflowInvocation,
    ProjectRole,
    ProjectGroup,
    Project,
    Experiment,
    Phenotype,
    GalaxyDataset,
)

from api.exceptions import RessourceAlreadyExists
from api.managers import AnalysisManager

TERMINAL_STATES = {"ok", "empty", "error", "discarded", "failed_metadata"}


class Analysis(GalaxyWorkflowInvocation):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    experiments = models.ManyToManyField(Experiment)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="analysis"
    )
    creation_date = models.DateTimeField(default=timezone.now)
    phenotypes = models.ManyToManyField(Phenotype)
    covariates = models.ManyToManyField(Phenotype, related_name="covariates")
    tools_params = models.JSONField()
    objects = AnalysisManager()

    class Meta:
        verbose_name_plural = "analyzes"

    def __str__(self):
        return f"{self.name} - wf-id: {self.galaxy_workflow.name}"

    def natural_key(self):
        return (self.galaxy_workflow, self.galaxy_history)

    def generate_galaxy_history_name(self):
        return str(uuid.uuid4())

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.
        project_roles = ProjectRole.objects.all()
        for project_role in project_roles:
            try:
                with transaction.atomic():
                    # Create a group project role entry
                    project_group = ProjectGroup.objects.get(
                        project=self.project, project_role=project_role
                    )
                    # assign group permissions to this instance
                    # Get the permissions for a role
                    permissions = project_role.permissions.filter(
                        content_type__model="analysis"
                    )
                    for perm in permissions:
                        assign_perm(perm, project_group, self)
                        assign_perm(perm, project_group)

            except IntegrityError:
                raise RessourceAlreadyExists()
                # Add reader role to the team the current user belongs

    def delete(self, *args, **kwargs):
        gi = self.galaxy_workflow.galaxy_user.bioblend_gi
        gi.histories.delete_history(self.galaxy_history.get_encoded_id(), purge=True)
        super().delete(*args, **kwargs)  # Call the "real" delete() method.

    @property
    def history_status(self):
        if self.galaxy_history:
            try:
                history_status = self.galaxy_history.status
                return history_status
            except ConnectionError:
                return None
        else:
            return None

    def run_analysis(self):
        self.galaxy_history.add_tags(
            [
                settings.GALAXY_WF_NAME,
                settings.GALAXY_WF_VERSION,
                settings.HOSTLABEL,
                f"{self.name}",
                f"{self.id}",
            ]
        )
        phenotype_file_path = self.generate_phenotype_file()
        covariate_file_path = self.generate_covariate_file()
        datamap = self.galaxy_history.upload_file(
            [phenotype_file_path, covariate_file_path], "csv"
        )
        workflow_invocation = self.galaxy_workflow.invoke(
            datamap, self.tools_params, self.galaxy_history
        )
        self.create_time = datetime.strptime(
            workflow_invocation["create_time"], settings.GALAXY_TIME_FORMAT
        )
        self.save()

    def generate_phenotype_file(self):
        if self.experiments.all().exists():
            experiment_db = self.experiments.first()
            if experiment_db:
                cc_lines_pheno_qs = experiment_db.cc_lines(self.phenotypes.all())
                df_phenotypes = pd.DataFrame(
                    data=[
                        {
                            **{
                                "Individual_ID": ccline.cc_line_id,
                                "Mother_Line": ccline.cc_line_m.id_cc,
                                "Father_Line": ccline.cc_line_f.id_cc,
                                # "Sex": ccline["sex"],
                            },
                            **dict(
                                [
                                    (pheno_val.phenotype.name, pheno_val.value)
                                    for pheno_val in ccline.phenotype_values.all()
                                ]
                            ),
                        }
                        for ccline in cc_lines_pheno_qs
                    ]
                )
                # write a tmp file
                _, pheno_file_path = tempfile.mkstemp(
                    prefix=f"cc-qtl-{self.name}-pheno-",
                    dir="/tmp-exp-files",
                    suffix=".csv",
                )
                # pheno_file_path = f"{dirpath}/{self.name}-pheno.csv"
                df_phenotypes.to_csv(
                    pheno_file_path,
                    index=False,
                )
                return pheno_file_path
            else:
                return None
        else:
            return None

    def generate_covariate_file(self):
        if self.experiments.all().exists():
            experiment_db = self.experiments.first()
            if experiment_db:
                covar_names = self.tools_params["3"]["add_covar"].split(",")
                covars_db = Phenotype.objects.filter(name__in=covar_names)
                covars_ids = [covar.id for covar in covars_db.all()]
                cc_lines_covar_qs = experiment_db.cc_lines(covars_ids)
                df_covariates = pd.DataFrame(
                    data=[
                        {
                            **{
                                "Individual_ID": ccline.cc_line_id,
                                "Mother_Line": ccline.cc_line_m.id_cc,
                                "Father_Line": ccline.cc_line_f.id_cc,
                                "Sex": ccline.sex,
                            },
                            **dict(
                                [
                                    (pheno_val.phenotype.name, pheno_val.value)
                                    for pheno_val in ccline.phenotype_values.all()
                                ]
                            ),
                        }
                        for ccline in cc_lines_covar_qs
                    ]
                )
                # write a tmp file
                _, covar_file_path = tempfile.mkstemp(
                    prefix=f"cc-qtl-{self.name}-covar-",
                    dir="/tmp-exp-files",
                    suffix=".csv",
                )
                df_covariates.to_csv(
                    covar_file_path,
                    index=False,
                )
                return covar_file_path
            else:
                return None
        else:
            return None

    # LOD scores
    def load_lod_score(self):
        phenotypes = self.phenotypes.all()
        for phenotype in phenotypes:
            if phenotype:
                lodscore_qs = LodScore.objects.filter(
                    analysis=self, phenotype=phenotype
                )
                if lodscore_qs.count() == 0:
                    dataset_pattern = "lod.csv"
                    if self.galaxy_history:
                        dataset = self.galaxy_history.get_dataset_endswith(
                            dataset_pattern, "csv"
                        )
                        if dataset["state"] not in TERMINAL_STATES:
                            continue
                        else:
                            file_path = self.galaxy_history.download_dataset_in_tmp(
                                dataset
                            )
                            # get the significance threshold
                            significance_threshold_dataset = (
                                self.galaxy_history.get_dataset_endswith(
                                    "significance-threshold.csv", "csv"
                                )
                            )
                            if (
                                significance_threshold_dataset["state"]
                                not in TERMINAL_STATES
                            ):
                                continue
                            else:
                                significance_threshold_path = (
                                    self.galaxy_history.download_dataset_in_tmp(
                                        significance_threshold_dataset
                                    )
                                )
                                significance_threshold = dict()
                                with open(
                                    significance_threshold_path, newline=""
                                ) as csvfile:
                                    st_reader = csv.DictReader(csvfile)
                                    for r in st_reader:
                                        significance_threshold = r
                                phenotype_count = self.phenotypes.count()
                                for phenotype in self.phenotypes.all():
                                    with open(file_path, newline="") as csvfile:
                                        lodscore_fh = csv.DictReader(csvfile)
                                        # skip header with next
                                        # next(lodscore_fh, None)
                                        lod_scores = list(
                                            self.lod_score_gen(
                                                lodscore_fh,
                                                phenotype.name,
                                                phenotype_count,
                                            )
                                        )
                                        st = significance_threshold[phenotype.name]
                                        lod_entry = LodScore(
                                            name=dataset["name"],
                                            extension=dataset["extension"],
                                            galaxy_history=self.galaxy_history,
                                            analysis=self,
                                            phenotype=phenotype,
                                            lod_scores=lod_scores,
                                            significance_threshold=st,
                                        )
                                        lod_entry.save()
                                        lod_entry.synchronize()

        return LodScore.objects.filter(analysis=self)

    def lod_score_gen(self, lods, phenotype_name, phenotype_count):
        column_names = ["marker", "chromosome", "position"]

        def sanitize(entry):
            sanitizedEntry = dict()
            sanitizedEntry["marker"] = entry["marker"].split(".")[1]
            sanitizedEntry["position"] = int(Decimal(entry["pos"]).shift(6))
            sanitizedEntry["lod"] = (
                float(entry["lod." + phenotype_name])
                if phenotype_count > 1
                else float(entry[phenotype_name])
            )
            sanitizedEntry["chromosome"] = entry["chr"]
            return sanitizedEntry

        for lod_dict in lods:
            if len(lod_dict.keys()) == len(column_names) + phenotype_count:
                yield (sanitize(lod_dict))
            else:
                if len(lod_dict.keys()) == 1 and lod_dict["marker"] == "":
                    continue
                else:
                    raise Exception(f"Wrong number of columns {lod_dict}")

    # Peaks
    def load_peaks(self):
        dataset_pattern = "refine-peaks.csv"

        # check if peaks for this analysis in db
        peaks = Peak.objects.filter(analysis=self)
        if peaks.count() == 0 and self.galaxy_history:
            dataset = self.galaxy_history.get_dataset_endswith(dataset_pattern, "csv")
            if dataset["state"] not in TERMINAL_STATES:
                return peaks
            else:
                file_path = self.galaxy_history.download_dataset_in_tmp(dataset)
                with open(file_path, newline="") as csvfile:
                    refine_peaks_fh = csv.reader(csvfile)
                    # skip header with next
                    next(refine_peaks_fh, None)
                    # analysis = Analysis.objects.get(pk=analysis_id)
                    # file_list = downloaded_ds.decode("utf-8").split("\n")
                    bulk = self.peaks_gen(refine_peaks_fh, self, dataset)
                    for peak in bulk:
                        peak.save()
                        peak.synchronize()
                        peak.load_coefficient()
                        peak.load_haplotypes()
                        peak.load_snps_association()
                        peak.load_top_snps()
                        peak.load_peak_genes()

        return peaks

    def peaks_gen(self, peaks, analysis, dataset):
        coef = 6
        for peak_tuple in peaks:
            # peak_tuple = peak.split(",")
            if len(peak_tuple) == 12:
                (_, lodcolumn, chr, pos, lod, ci_lo, ci_hi, *_) = peak_tuple
                phenotype = Phenotype.objects.get(name=lodcolumn)
                peak_obj = Peak(
                    name=dataset["name"],
                    extension=dataset["extension"],
                    galaxy_history=self.galaxy_history,
                    analysis=analysis,
                    variable_name=lodcolumn,
                    chromosome=chr,
                    position=int(Decimal(pos).shift(coef)),
                    lod=float(lod),
                    ci_lo=int(Decimal(ci_lo).shift(coef)),
                    ci_hi=int(Decimal(ci_hi).shift(coef)),
                    phenotype=phenotype,
                )
                yield peak_obj
            elif len(peak_tuple) == 13:
                (_, lodcolumn, chr, pos, lod, ci_lo, ci_hi, p_value, *_) = peak_tuple
                phenotype = Phenotype.objects.get(name=lodcolumn)
                peak_obj = Peak(
                    name=dataset["name"],
                    extension=dataset["extension"],
                    galaxy_history=analysis.galaxy_history,
                    analysis=analysis,
                    variable_name=lodcolumn,
                    chromosome=chr,
                    position=int(Decimal(pos).shift(coef)),
                    lod=float(lod),
                    ci_lo=int(Decimal(ci_lo).shift(coef)),
                    ci_hi=int(Decimal(ci_hi).shift(coef)),
                    p_value=float(p_value),
                    phenotype=phenotype,
                )
                yield peak_obj
            else:
                continue


# Add foreign keys to handle permission deletion when object is deleted
class AnalysisUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Analysis, on_delete=models.CASCADE)


class AnalysisGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Analysis, on_delete=models.CASCADE)


class LodScore(GalaxyDataset):
    analysis = models.ForeignKey(Analysis, on_delete=models.CASCADE)
    lod_scores = models.JSONField(blank=True)
    significance_threshold = models.FloatField(null=True)
    phenotype = models.ForeignKey(Phenotype, on_delete=models.CASCADE)

    class Meta:
        ordering = ["analysis"]
        unique_together = [["analysis", "phenotype"]]

    def __str__(self):
        return f"{self.analysis} - {self.phenotype}"


# Add foreign keys to handle permission deletion when object is deleted
class LodScoreUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(LodScore, on_delete=models.CASCADE)


class LodScoreGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(LodScore, on_delete=models.CASCADE)


class Peak(GalaxyDataset):
    # parent = models.OneToOneField(
    #     to=GalaxyDataset, parent_link=True, on_delete=models.CASCADE
    # )
    analysis = models.ForeignKey(
        Analysis, on_delete=models.CASCADE, related_name="peaks"
    )
    variable_name = models.CharField(max_length=200)
    chromosome = models.CharField(max_length=200)
    position = models.FloatField()
    lod = models.FloatField()
    ci_lo = models.FloatField()
    ci_hi = models.FloatField()
    p_value = models.FloatField(null=True)
    phenotype = models.ForeignKey(Phenotype, on_delete=models.CASCADE)

    # objects = PeakManager()

    class Meta:
        ordering = ["analysis", "variable_name", "chromosome", "position"]
        unique_together = [["analysis", "phenotype", "chromosome", "position"]]

    def __str__(self):
        return f"{self.id} - {self.variable_name} {self.chromosome} {self.position}"

    def get_dataset_name(self):
        return f"var--{self.variable_name}--chr--{self.chromosome}--pos--{int(self.position)}"

    def load_coefficient(self):
        coefficient_qs = Coefficient.objects.filter(peak_coefficient=self)
        if coefficient_qs.count() == 0:
            dataset_pattern = "coef.csv"
            if self.galaxy_history:
                datasets = self.galaxy_history.get_datasets_endswith(
                    dataset_pattern, "csv"
                )
                for dataset in datasets:
                    if dataset["state"] not in TERMINAL_STATES:
                        return coefficient_qs
                    else:
                        # check if the dataset match the peak
                        peak_dataset_name = "--".join(dataset["name"].split("--")[:-1])
                        if self.get_dataset_name() == peak_dataset_name:
                            file_path = self.galaxy_history.download_dataset_in_tmp(
                                dataset
                            )
                            with open(file_path, newline="") as csvfile:
                                coef_fh = csv.reader(csvfile)
                                # skip header with next
                                next(coef_fh, None)
                                coef_gen = list(self.coefficient_gen(coef_fh))
                                coef_db = Coefficient(
                                    name=dataset["name"],
                                    extension=dataset["extension"],
                                    galaxy_history=self.galaxy_history,
                                    peak_coefficient=self,
                                    coefficients=coef_gen,
                                )
                                coef_db.save()
                                coef_db.synchronize()
        return coefficient_qs

    def coefficient_gen(self, coefficients):
        column_names = [
            "marker",
            "chromosome",
            "position",
            "lodscore",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
        ]

        def gen_sanitize_coef_tuple(coef_tuple):
            for i, val in enumerate(coef_tuple):
                sanitized_val = val
                if i == 2:
                    sanitized_val = int(Decimal(val).shift(6))
                if i == 3:
                    sanitized_val = float(val)
                if i > 3:
                    if val == "NA":
                        sanitized_val = None
                    else:
                        sanitized_val = float(val)
                yield sanitized_val

        for coef_tuple in coefficients:
            if len(coef_tuple) == len(column_names):
                yield (dict(zip(column_names, gen_sanitize_coef_tuple(coef_tuple))))
            else:
                if len(coef_tuple) == 1 and coef_tuple[0] == "":
                    continue
                else:
                    raise Exception(f"Wrong number of columns {coef_tuple}")

    # Haplotypes
    def load_haplotypes(self):
        # peaks = self.load_peaks()
        # if peaks.count() > 0:
        haplotype_qs = Haplotype.objects.filter(peak_haplotype=self)
        if haplotype_qs.count() == 0:
            dataset_pattern = "haplo.csv"
            if self.galaxy_history:
                datasets = self.galaxy_history.get_datasets_endswith(
                    dataset_pattern, "csv"
                )
                for dataset in datasets:
                    if dataset["state"] not in TERMINAL_STATES:
                        return haplotype_qs
                    else:
                        peak_dataset_name = "--".join(dataset["name"].split("--")[:-1])
                        if self.get_dataset_name() == peak_dataset_name:
                            file_path = self.galaxy_history.download_dataset_in_tmp(
                                dataset
                            )
                            with open(file_path, newline="") as csvfile:
                                haplotype_fh = csv.reader(csvfile)
                                # skip header with next
                                next(haplotype_fh, None)
                                haplo_gen = self.haplotype_gen(haplotype_fh)
                                haplotype_db = Haplotype(
                                    name=dataset["name"],
                                    extension=dataset["extension"],
                                    galaxy_history=self.galaxy_history,
                                    peak_haplotype=self,
                                    haplotypes=list(haplo_gen),
                                )
                                haplotype_db.save()
                                haplotype_db.synchronize()
        return haplotype_qs

    def haplotype_gen(self, haplotypes):
        column_names = ["phenotype", "haplotype", "ccline"]
        for haplo_tuple in haplotypes:
            if len(haplo_tuple) == len(column_names):
                if haplo_tuple[0] == "NA":
                    continue
                sanitized_haplo = (
                    float(h) if i == 0 else h for (i, h) in enumerate(haplo_tuple)
                )
                yield (dict(zip(column_names, sanitized_haplo)))
            else:
                if len(haplo_tuple) == 1 and haplo_tuple[0] == "":
                    continue
                else:
                    raise Exception(f"Wrong number of columns {haplo_tuple}")

    # SNPs association
    def load_snps_association(self):
        snps_association_qs = SnpsAssociation.objects.filter(peak_snps_association=self)
        if snps_association_qs.count() == 0:
            if self.galaxy_history:
                dataset_pattern = "snps_assoc.csv"
                datasets = self.galaxy_history.get_datasets_endswith(
                    dataset_pattern, "csv"
                )
                for dataset in datasets:
                    if dataset["state"] not in TERMINAL_STATES:
                        return snps_association_qs
                    else:
                        peak_dataset_name = "--".join(dataset["name"].split("--")[:-1])
                        if self.get_dataset_name() == peak_dataset_name:
                            file_path = self.galaxy_history.download_dataset_in_tmp(
                                dataset
                            )
                            with open(file_path, newline="") as csvfile:
                                snps_asso_fh = csv.reader(csvfile)
                                # skip header with next
                                next(snps_asso_fh, None)
                                snps_assoc = list(
                                    self.snps_association_gen(snps_asso_fh)
                                )
                                snps_assoc_db = SnpsAssociation(
                                    name=dataset["name"],
                                    extension=dataset["extension"],
                                    galaxy_history=self.galaxy_history,
                                    peak_snps_association=self,
                                    snps_associations=snps_assoc,
                                )
                                snps_assoc_db.save()
                                snps_assoc_db.synchronize()
        return snps_association_qs

    def snps_association_gen(self, snps_associations):
        column_names = ["snp_id", "chromosome", "position", "lodscore"]

        def sanitize(assoc_tuple):
            for i, val in enumerate(assoc_tuple):
                if i == 2:
                    # postion
                    val = int(Decimal(val).shift(6))
                if i == 3:
                    # lodscore
                    val = float(val)
                yield val

        for assoc_tuple in snps_associations:
            if len(assoc_tuple) == len(column_names):
                yield dict(zip(column_names, sanitize(assoc_tuple)))
            else:
                if len(assoc_tuple) == 1 and assoc_tuple[0] == "":
                    continue
                else:
                    raise Exception(f"Wrong number of columns {assoc_tuple}")

    # Top SNPs
    def load_top_snps(self):
        top_snps_qs = TopSnp.objects.filter(peak_top_snp=self)
        if top_snps_qs.count() == 0:
            # Get the top snps from galaxy
            if self.galaxy_history:
                dataset_pattern = "top_snps.csv"
                datasets = self.galaxy_history.get_datasets_endswith(
                    dataset_pattern, "csv"
                )
                for dataset in datasets:
                    if dataset["state"] not in TERMINAL_STATES:
                        return top_snps_qs
                    else:
                        peak_dataset_name = "--".join(dataset["name"].split("--")[:-1])
                        if self.get_dataset_name() == peak_dataset_name:
                            file_path = self.galaxy_history.download_dataset_in_tmp(
                                dataset
                            )
                            with open(file_path, newline="") as csvfile:
                                top_snps_fh = csv.reader(csvfile)
                                # skip header with next
                                next(top_snps_fh, None)
                                top_snps = list(self.top_snps_gen(top_snps_fh))
                                top_snps_db = TopSnp(
                                    name=dataset["name"],
                                    extension=dataset["extension"],
                                    galaxy_history=self.galaxy_history,
                                    peak_top_snp=self,
                                    top_snps=top_snps,
                                )
                                top_snps_db.save()
                                top_snps_db.synchronize()
        return top_snps_qs

    def top_snps_gen(self, top_snps):
        column_names = ["phenotype", "ccline"]
        top_snp_column_name = "top_snp"
        for top_snp_tuple in top_snps:
            if len(top_snp_tuple) > 2:
                if top_snp_tuple[0] == "NA":
                    continue
                top_snp_cols = top_snp_tuple[2:]
                for i in range(len(top_snp_cols)):
                    column_names.append(f"{top_snp_column_name}_{i+1}")
                sanitized_top_snp = (
                    float(snp) if i == 0 else snp for i, snp in enumerate(top_snp_tuple)
                )

                yield dict(zip(column_names, sanitized_top_snp))
            else:
                if len(top_snp_tuple) == 1 and top_snp_tuple[0] == "":
                    continue
                else:
                    raise Exception(f"Wrong number of columns {top_snp_tuple}")

    # Genes
    def load_peak_genes(self):
        peak_genes_qs = PeakGene.objects.filter(peak_gene=self)
        if peak_genes_qs.count() == 0:
            if self.galaxy_history:
                dataset_pattern = "genes.csv"
                datasets = self.galaxy_history.get_datasets_endswith(
                    dataset_pattern, "csv"
                )
                for dataset in datasets:
                    if dataset["state"] not in TERMINAL_STATES:
                        return peak_genes_qs
                    else:
                        peak_dataset_name = "--".join(dataset["name"].split("--")[:-1])
                        if self.get_dataset_name() == peak_dataset_name:
                            file_path = self.galaxy_history.download_dataset_in_tmp(
                                dataset
                            )
                            with open(file_path, newline="") as csvfile:
                                peak_genes_fh = csv.reader(csvfile)
                                # skip header with next
                                next(peak_genes_fh, None)
                                peak_genes = list(self.peak_genes_gen(peak_genes_fh))
                                peak_genes_db = PeakGene(
                                    name=dataset["name"],
                                    extension=dataset["extension"],
                                    galaxy_history=self.galaxy_history,
                                    peak_gene=self,
                                    genes=peak_genes,
                                )
                                peak_genes_db.save()
                                peak_genes_db.synchronize()
        return peak_genes_qs

    def peak_genes_gen(self, genes):
        column_names = [
            "chromosome",
            "source",
            "type",
            "start",
            "stop",
            "score",
            "strand",
            "phase",
            "id",
            "name",
            "parent",
            "dbxref",
            "mgiName",
            "bioType",
            "alias",
        ]

        def sanitize(entry_values):
            for i, val in enumerate(entry_values):
                if val == "NA":
                    val = None
                if i == 3 or i == 4:
                    val = int(Decimal(val).shift(6))
                yield val

        for entry_values in genes:
            if len(entry_values) == len(column_names):
                # sanitized_values = (val if val != "NA" else None for val in entry_values)
                yield dict(zip(column_names, sanitize(entry_values)))
            else:
                if len(entry_values) == 1 and entry_values[0] == "":
                    continue
                else:
                    raise Exception(f"Wrong number of columns {entry_values}")


# Add foreign keys to handle permission deletion when object is deleted
class PeakUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Analysis, on_delete=models.CASCADE)


class PeakGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Analysis, on_delete=models.CASCADE)


class Coefficient(GalaxyDataset):
    # parent = models.OneToOneField(
    #     to=GalaxyDataset, parent_link=True, on_delete=models.CASCADE
    # )
    peak = models.OneToOneField(Peak, on_delete=models.CASCADE, name="peak_coefficient")
    coefficients = models.JSONField()


class SnpsAssociation(GalaxyDataset):
    # parent = models.OneToOneField(
    #     to=GalaxyDataset, parent_link=True, on_delete=models.CASCADE
    # )
    peak = models.OneToOneField(
        Peak,
        on_delete=models.CASCADE,
        name="peak_snps_association",
    )
    snps_associations = models.JSONField()

    def __str__(self):
        return f"{self.peak}"


class TopSnp(GalaxyDataset):
    # parent = models.OneToOneField(
    #     to=GalaxyDataset, parent_link=True, on_delete=models.CASCADE
    # )
    peak = models.OneToOneField(
        Peak,
        on_delete=models.CASCADE,
        name="peak_top_snp",
    )
    top_snps = models.JSONField()

    def __str__(self):
        return f"{self.peak}"


class PeakGene(GalaxyDataset):
    # parent = models.OneToOneField(
    #     to=GalaxyDataset, parent_link=True, on_delete=models.CASCADE
    # )
    peak = models.OneToOneField(Peak, on_delete=models.CASCADE, name="peak_gene")
    genes = models.JSONField()

    def __str__(self):
        return f"{self.peak}"


class Haplotype(GalaxyDataset):
    # parent = models.OneToOneField(
    #     to=GalaxyDataset, parent_link=True, on_delete=models.CASCADE
    # )
    peak = models.OneToOneField(Peak, on_delete=models.CASCADE, name="peak_haplotype")
    haplotypes = models.JSONField()

    def __str__(self):
        return f"{self.peak}"
