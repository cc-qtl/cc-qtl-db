# Django
from django.db import models
from django.db import transaction
from django.utils import timezone
from django.db.utils import IntegrityError
from django.utils.translation import gettext_lazy as _

# guardian
from guardian.shortcuts import assign_perm
from guardian.models import UserObjectPermissionBase
from guardian.models import GroupObjectPermissionBase

# api
from api.models import Project, ProjectRole, ProjectGroup

from api.managers import (
    FounderManager,
    MarkerManager,
    CCLineManager,
    OntologyManager,
    PhenotypeCategoryManager,
)

from api.exceptions import RessourceAlreadyExists


class PhenotypeNature(models.TextChoices):
    Quantitative = ("Quantitative",)
    Categorical = ("Categorical",)
    Measurement = ("Measurement",)
    Count = ("Count",)
    Semi_Quantitative = ("Semi_Quantitative",)
    Unassigned = ("Unassigned",)


class PhenotypeLocation(models.TextChoices):
    Organ = ("Organ",)
    Whole_Organism = ("Whole_Organism",)
    Unassigned = ("Unassigned",)


class Datatype(models.TextChoices):
    Float = ("Float",)
    Integer = ("Integer",)
    Char = ("Char",)
    Unassigned = ("Unassigned",)


class Dataclass(models.TextChoices):
    Phenotype = ("Phenotype",)
    Covariate = ("Covariate",)


class Ontology(models.Model):
    term_id = models.CharField(max_length=50, unique=True)
    description = models.TextField(blank=True)

    objects = OntologyManager()

    class Meta:
        ordering = ["term_id"]

    def natural_key(self):
        return (self.term_id,)


class PhenotypeCategory(models.Model):
    name = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=400)
    nature = models.CharField(max_length=200, choices=PhenotypeNature.choices)
    location = models.CharField(max_length=200, choices=PhenotypeLocation.choices)
    datatype = models.CharField(max_length=50, choices=Datatype.choices)
    dataclass = models.CharField(max_length=20, choices=Dataclass.choices)
    ontologies = models.ManyToManyField(Ontology)
    objects = PhenotypeCategoryManager()

    class Meta:
        ordering = ["name"]
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_nature_valid",
                check=models.Q(nature__in=PhenotypeNature.values),
            ),
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_location_valid",
                check=models.Q(location__in=PhenotypeLocation.values),
            ),
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_datatype_valid",
                check=models.Q(datatype__in=Datatype.values),
            ),
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_dataclass_valid",
                check=models.Q(dataclass__in=Dataclass.values),
            ),
        ]

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)


class Phenotype(models.Model):
    class Primitive(models.TextChoices):
        Float = "F", "Float"
        Integer = "I", "Integer"
        String = "S", "String"
        Boolean = "B", "Boolean"
        __empty__ = _("Unassigned")

    name = models.CharField(max_length=256, unique=True)
    categories = models.ManyToManyField(PhenotypeCategory)
    primitive = models.CharField(
        max_length=50,
        choices=Primitive.choices,
        default=Primitive.__empty__,
    )

    class Meta:
        ordering = ["name"]
        # unique_together = [["name", "primitive"]]

    @classmethod
    def get_primitive(cls, dtype):
        if dtype.name.startswith("float"):
            return cls.Primitive.Float
        elif dtype.name.startswith("int"):
            return cls.Primitive.Integer
        elif dtype.name.startswith("object"):
            return cls.Primitive.String
        elif dtype.name.startswith("bool"):
            return cls.Primitive.Boolean
        else:
            return cls.Primitive.__empty__

    def cast_to_python_primitive(self, value):
        if self.primitive == "F":
            return float(value)
        if self.primitive == "I":
            return int(value)
        if self.primitive == "S":
            return str(value)
        if self.primitive == "B":
            if value == "True":
                return True
            if value == "False":
                return False
            raise TypeError(f"The value {value} cannot be cast as boolean")
        if self.primitive == "Unassigned":
            return value
        raise TypeError(f"The primitive {self.primitive} is not handled")

    def __str__(self):
        return f"{self.id} - {self.name} as {self.get_primitive_display()} "

    def natural_key(self):
        return (self.name,)


class Experiment(models.Model):
    """
    Store experiments
    """

    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    creation_date = models.DateTimeField(default=timezone.now)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="experiments"
    )
    phenotypes = models.ManyToManyField(Phenotype)

    objects = models.Manager()

    class Meta:
        ordering = ["creation_date"]
        unique_together = [["name", "project"]]

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name, self.project)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.

        project_roles = ProjectRole.objects.all()
        for project_role in project_roles:
            try:
                with transaction.atomic():
                    # Create a group project role entry
                    project_group = ProjectGroup.objects.get(
                        project=self.project, project_role=project_role
                    )
                    # assign group permissions to this instance
                    # Get the permissions for a role
                    permissions = project_role.permissions.filter(
                        content_type__model="experiment"
                    )
                    for experiment_perm in permissions:
                        assign_perm(experiment_perm, project_group, self)
                        assign_perm(experiment_perm, project_group)

            except IntegrityError:
                raise RessourceAlreadyExists()
                # Add reader role to the team the current user belongs

    def cc_lines(self, phenotype_ids):
        return ExperimentCcline.objects.prefetch_related(
            models.Prefetch(
                "phenotype_values",
                queryset=PhenotypeValue.objects.filter(phenotype__in=phenotype_ids),
            )
        ).filter(experiment=self)

    def get_experiment_phenotype_types(self):
        return (
            PhenotypeValue.objects.filter(experiment_cc_line__experiment=self)
            .values(
                "category__id",
                "category__name",
                "category__description",
                "category__datatype",
                "category__dataclass",
            )
            .annotate(
                id=models.F("category__id"),
                name=models.F("category__name"),
                description=models.F("category__description"),
                datatype=models.F("category__datatype"),
                dataclass=models.F("category__dataclass"),
            )
            .values(
                "id",
                "name",
                "description",
                "datatype",
                "dataclass",
            )
            .order_by("id")
            .distinct("id")
        )


# Add foreign keys to handle permission deletion when object is deleted
class ExperimentUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Experiment, on_delete=models.CASCADE)


class ExperimentGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Experiment, on_delete=models.CASCADE)


class Founder(models.Model):
    name = models.CharField(max_length=200, unique=True)
    short_name = models.CharField(max_length=20, blank=True)
    cc_designation = models.CharField(max_length=10, blank=True, unique=True)
    color = models.CharField(max_length=10, blank=True)
    objects = FounderManager()

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

    class Meta:
        ordering = ["name"]


class Marker(models.Model):
    id_marker = models.CharField(max_length=200, unique=True)
    chromosome = models.CharField(max_length=200)
    position = models.PositiveIntegerField()
    objects = MarkerManager()

    def __str__(self):
        return self.id_marker

    def natural_key(self):
        return (self.id_marker,)

    class Meta:
        ordering = ["chromosome"]


class Ccline(models.Model):
    id_cc = models.CharField(max_length=200, unique=True)
    markers = models.ManyToManyField(Marker, through="CclineMarker")
    funnel_code = models.CharField(max_length=8)
    objects = CCLineManager()

    class Meta:
        ordering = ["id_cc"]

    def __str__(self):
        return self.id_cc

    def natural_key(self):
        return (self.id_cc,)


class IUPAC(models.TextChoices):
    ADENINE = "A", _("Adenine")
    CYTOSINE = "C", _("Cytosine")
    GUANINE = "G", _("Guanine")
    THYMINE = "T", _("Thymine")
    WEAK = "W", _("Weak")
    STRONG = "S", _("Strong")
    AMINO = "M", _("Amino")
    KETO = "K", _("Keto")
    PURINE = "P", _("Purine")
    PYRIMIDINE = "Y", _("Pyrimidine")
    NOT_A = "B", _("Not_a")
    NOT_C = "D", _("Not_c")
    NOT_G = "H", _("Not_g")
    NOT_T = "V", _("Not_t")
    ANY = "N", _("Any")
    ZERO = "Z", _("Zero")


class CclineMarker(models.Model):
    cc_line = models.ForeignKey(Ccline, on_delete=models.CASCADE)
    marker = models.ForeignKey(Marker, on_delete=models.CASCADE)
    base = models.CharField(max_length=1, choices=IUPAC.choices)

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_base_valid",
                check=models.Q(base__in=IUPAC.values),
            ),
        ]


class Sex(models.TextChoices):
    Female = "F", _("Female")
    Male = "M", _("Male")


class ExperimentCcline(models.Model):
    experiment = models.ForeignKey(
        Experiment, on_delete=models.CASCADE, related_name="cclines"
    )
    cc_line_id = models.CharField(max_length=50)
    cc_line_m = models.ForeignKey(
        Ccline, on_delete=models.CASCADE, related_name="ccline_m"
    )
    cc_line_f = models.ForeignKey(
        Ccline, on_delete=models.CASCADE, related_name="ccline_f"
    )
    sex = models.CharField(max_length=1, choices=Sex.choices)

    class Meta:
        ordering = ["experiment", "cc_line_id"]
        unique_together = [["experiment", "cc_line_id"]]
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_sex_valid",
                check=models.Q(sex__in=Sex.values),
            ),
        ]

    def __str__(self):
        return f"{self.cc_line_m} x {self.cc_line_f} -> {Sex(self.sex).label}"


class PhenotypeValue(models.Model):
    experiment_cc_line = models.ForeignKey(
        ExperimentCcline, on_delete=models.CASCADE, related_name="phenotype_values"
    )
    value = models.CharField(max_length=100)
    phenotype = models.ForeignKey(
        Phenotype,
        on_delete=models.CASCADE,
        related_name="phenotype_values",
    )

    class Meta:
        ordering = ["phenotype", "experiment_cc_line"]
        unique_together = [["experiment_cc_line", "phenotype"]]

    def __str__(self):
        return (
            f"{self.experiment_cc_line} | phenotype : {self.phenotype} = {self.value}"
        )

    def natural_key(self):
        return (self.experiment_cc_line, self.phenotype)
