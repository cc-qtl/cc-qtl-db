from datetime import datetime
import json
from tempfile import mkdtemp
from pathlib import Path
import time
import logging
from collections import defaultdict
import uuid

# Django
from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from requests.exceptions import HTTPError

from api.managers import (
    GalaxyInstanceManager,
    GalaxyUserManager,
    GalaxyWorkflowManager,
    AnalysisCategoryManager,
)

# bioblend
from bioblend import ConnectionError, TimeoutException
from bioblend import galaxy
from bioblend.galaxy.datasets import DatasetStateException


INVOCATION_TERMINAL_STATES = {"cancelled", "failed", "scheduled"}
DATASET_TERMINAL_STATES = {"ok", "empty", "error", "discarded", "failed_metadata"}
# Non-terminal states are: 'new', 'upload', 'queued', 'running', 'paused', 'setting_metadata'

log = logging.getLogger(__name__)


class AnalysisCategory(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(blank=True)

    objects = AnalysisCategoryManager()

    class Meta:
        ordering = ["name"]

    def natural_key(self):
        return (self.name,)


class GalaxyInstance(models.Model):
    name = models.CharField(max_length=200, unique=True)
    url = models.URLField(unique=True)
    description = models.TextField(blank=True)

    objects = GalaxyInstanceManager()

    class Meta:
        ordering = ["name"]

    def natural_key(self):
        return (self.name,)

    def __str__(self):
        return self.name


class GalaxyUser(models.Model):
    username = models.EmailField(max_length=254)
    api_key = models.CharField(max_length=254, blank=True)
    galaxy_instance = models.ForeignKey(GalaxyInstance, on_delete=models.DO_NOTHING)
    objects = GalaxyUserManager()

    class Meta:
        ordering = ["username", "galaxy_instance"]
        unique_together = [["username", "galaxy_instance"]]

    @property
    def bioblend_gi(self):
        """Bioblend object to interact with Galaxy instance."""
        if getattr(self, "_bioblend_gi", None) is None:
            self._bioblend_gi = self.get_bioblend_obj_gi()
        return self._bioblend_gi

    @bioblend_gi.setter
    def bioblend_gi(self, val):
        self._bioblend_gi = val

    def get_bioblend_obj_gi(self):
        """Retrieve bioblend object to interact with Galaxy instance."""

        return galaxy.GalaxyInstance(self.galaxy_instance.url, self.api_key)
        # return BioblendGalaxyInstance(self.galaxy_instance.url, self.api_key)

    def generate_galaxy_history_name(self):
        return str(uuid.uuid4())

    def create_history(self, name=None):
        if name is None:
            name = self.generate_galaxy_history_name()
        galaxy_history = self.bioblend_gi.histories.create_history(name)
        db_history = GalaxyHistory(
            name=name,
            galaxy_user=self,
            create_time=datetime.strptime(
                galaxy_history["create_time"], settings.GALAXY_TIME_FORMAT
            ),
            galaxy_state=galaxy_history["state"],
        )
        db_history.save()
        return db_history

    def natural_key(self):
        return (self.username, self.galaxy_instance)


class GalaxyHistory(models.Model):
    name = models.CharField(max_length=200, unique=True)
    create_time = models.DateTimeField()
    galaxy_state = models.CharField(max_length=100)
    galaxy_user = models.ForeignKey(GalaxyUser, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("GalaxyHistory")
        verbose_name_plural = _("GalaxyHistories")

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

    @property
    def status(self):
        return self.galaxy_user.bioblend_gi.histories.get_status(self.get_encoded_id())

    def get_encoded_id(self):
        gi = self.galaxy_user.bioblend_gi
        histories = gi.histories.get_histories(name=self.name)

        if len(histories) == 1:
            return histories[0]["id"]
        else:
            raise Exception(f"Multiple histories for {self.name}")

    def add_tags(self, tags):
        gi = self.galaxy_user.bioblend_gi
        encoded_id = self.get_encoded_id()
        for tag in tags:
            gi.histories.create_history_tag(encoded_id, tag=tag)

    def synchronize(self):
        if self.galaxy_state != "ok" or self.galaxy_state != "error":
            history = self.show_history()
            self.galaxy_state = history["state"]
            self.save()

    def get_datasets_endswith(self, pattern, extension):
        history_id = self.get_encoded_id()
        try:
            datasets = self.galaxy_user.bioblend_gi.datasets.get_datasets(
                history_id=history_id, extension=extension
            )
        except HTTPError:
            raise Exception(f"Do not find history : {history_id}")
        try:
            filtered_datasets = [ds for ds in datasets if ds["name"].endswith(pattern)]
            return filtered_datasets
        except StopIteration:
            raise Exception(f"Do not find the dataset : {pattern}")

    def get_dataset_endswith(self, pattern, extension):
        history_id = self.get_encoded_id()
        filtered_datasets = self.get_datasets_endswith(pattern, extension)
        if len(filtered_datasets) == 1:
            return self.galaxy_user.bioblend_gi.datasets.show_dataset(
                filtered_datasets[0]["id"]
            )
        elif len(filtered_datasets) > 1:
            raise Exception(
                f"Multiple datasets for pattern {pattern} in history {history_id}"
            )
        else:
            raise Exception(
                "No dataset available for pattern"
                + f"{pattern} in history {history_id}"
            )

    def download_dataset_in_tmp(self, dataset):
        history_id = self.get_encoded_id()
        galaxy_user = self.galaxy_user
        history = galaxy_user.bioblend_gi.histories.show_history(history_id)
        try:
            dirpath = mkdtemp(prefix="cc-qtl-")
            file_path = Path(dirpath) / Path(dataset["name"])
            galaxy_user.bioblend_gi.datasets.download_dataset(
                dataset["id"],
                file_path=file_path,
                use_default_filename=False,
                require_ok_state=True,
            )
        except HTTPError:
            raise Exception(f"Could not get the dataset : {dataset['name']}")
        except DatasetStateException as e:
            raise DatasetStateException(
                f"{str(e)}. The dataset name: {dataset['name']}. "
                + f"The history name : {history['name']}."
            )
        else:
            return file_path

    def upload_file(self, file_paths, file_type):
        datamap = dict()
        for i, file_path in enumerate(file_paths):
            upload_response = self.galaxy_user.bioblend_gi.tools.upload_file(
                file_path, self.get_encoded_id(), file_type=file_type
            )
            upload_data_id = upload_response["outputs"][0]["id"]
            datamap[i] = {"src": "hda", "id": upload_data_id}
            upload_job = upload_response["jobs"][0]
            upload_job_id = upload_job["id"]
            while upload_job["state"] not in ["ok"]:
                time.sleep(2)
                upload_job = self.galaxy_user.bioblend_gi.jobs.show_job(upload_job_id)
                if upload_job["state"] in ["error", "deleted", "discarded"]:
                    data = self.galaxy_user.bioblend_gi.datasets.show_dataset(
                        upload_data_id
                    )
                    raise Exception(
                        f"Error during Galaxy data upload job - name : "
                        f"{data['name']}, id : {upload_data_id}, "
                        f"error : {data['misc_info']}"
                    )
            # check uploaded dataset status
            data = self.galaxy_user.bioblend_gi.datasets.show_dataset(upload_data_id)
            if data["state"] not in ["ok"]:
                raise Exception(
                    f"Error during Galaxy data upload result - name : "
                    f"{data['name']}, id : {upload_data_id}, "
                    f"error : {data['misc_info']}"
                )
            Path(file_path).unlink()
        return datamap

    def show_history(self):
        history_id = self.get_encoded_id()
        galaxy_user = self.galaxy_user
        history = galaxy_user.bioblend_gi.histories.show_history(
            history_id, contents=False, types=["dataset"]
        )
        return history


class GalaxyWorkflow(models.Model):
    name = models.CharField(max_length=200)
    analysis_category = models.ForeignKey(AnalysisCategory, on_delete=models.DO_NOTHING)
    version = models.CharField(max_length=50)
    galaxy_user = models.ForeignKey(GalaxyUser, on_delete=models.CASCADE)

    objects = GalaxyWorkflowManager()

    class Meta:
        ordering = ["analysis_category", "name"]
        unique_together = [["name", "galaxy_user", "version"]]

    @property
    def details(self):
        gi = self.galaxy_user.bioblend_gi
        return gi.workflows.show_workflow(self.get_encoded_id())

    def natural_key(self):
        return (self.name, self.galaxy_user, self.version)

    def get_encoded_id(self):
        gi = self.galaxy_user.bioblend_gi

        try:
            workflows = gi.workflows.get_workflows(name=self.name)
            if len(workflows) == 1:
                return workflows[0]["id"]
            elif len(workflows) > 1:
                for wf in workflows:
                    tags_set = set(wf["tags"])
                    if f"v{self.version}" in tags_set:
                        return wf["id"]
                # try to find the one with the correct version
                raise Exception(f"Multiple workflows for {self.name}")
            else:
                raise Exception(f"No workflow found for {self.name}")
        except ConnectionError as e:
            raise e

    def invoke(self, datamap, params, history_db):
        wf_encoded_id = self.get_encoded_id()
        genome_scan_wf_metadata = self.galaxy_user.bioblend_gi.workflows.show_workflow(
            wf_encoded_id
        )
        try:
            workflow_invocation = (
                self.galaxy_user.bioblend_gi.workflows.invoke_workflow(
                    wf_encoded_id,
                    inputs=datamap,
                    params=params,
                    history_id=history_db.get_encoded_id(),
                )
            )
            return workflow_invocation
        except StopIteration:
            raise Exception(
                f"Do not find the workflow : {genome_scan_wf_metadata['name']}"
            )

    def get_tools(self):
        gi = self.galaxy_user.bioblend_gi

        def get_job_params(job_ids, gi):
            for step_id, job_id in job_ids:
                if job_id is not None:
                    yield (
                        step_id,
                        gi.tools.show_tool(job_id, io_details=True),
                    )

        try:
            wf_with_details = gi.workflows.show_workflow(self.get_encoded_id())
            wf_step_tool_ids = [
                (step_id, step["tool_id"])
                for step_id, step in wf_with_details["steps"].items()
            ]
            return dict(get_job_params(wf_step_tool_ids, gi))
        except ConnectionError as e:
            raise e

    def get_default_parameters(self):
        gi = self.galaxy_user.bioblend_gi
        wf = gi.workflows.show_workflow(self.get_encoded_id())
        return dict(
            [
                (
                    step_id,
                    dict(
                        [
                            (input_id, input_val)
                            for input_id, input_val in step["tool_inputs"].items()
                            if input_val is not None and type(input_val) is not dict
                        ]
                    ),
                )
                for step_id, step in wf["steps"].items()
                if step_id != "0"
            ]
        )


class GalaxyWorkflowInvocation(models.Model):
    class WorkflowInvocationStatus(models.TextChoices):
        SCHEDULED = ("scheduled", "Scheduled")
        RUNNING = ("running", "Running")
        PAUSED = ("paused", "Paused")
        ERROR = ("error", "Error")
        DONE = ("done", "Done")

    TERMINAL_STATE = {"cancelled", "failed"}

    galaxy_workflow = models.ForeignKey(GalaxyWorkflow, on_delete=models.CASCADE)
    galaxy_history = models.OneToOneField(GalaxyHistory, on_delete=models.CASCADE)
    galaxy_state = models.CharField(null=False, max_length=200)
    create_time = models.DateTimeField(null=True)
    status = models.CharField(
        max_length=10,
        choices=WorkflowInvocationStatus.choices,
        default=WorkflowInvocationStatus.SCHEDULED,
    )

    class Meta:
        verbose_name = _("GalaxyWorkflowInvocation")
        verbose_name_plural = _("GalaxyWorkflowInvocations")
        unique_together = [["galaxy_workflow", "galaxy_history"]]

    def __str__(self):
        return f"{self.galaxy_workflow} - {self.galaxy_history}"

    # @property
    # def status(self):
    #     return self.get_galaxy_invocation()["state"]

    def get_encoded_id(self):
        gi = self.galaxy_workflow.galaxy_user.bioblend_gi
        invocations = gi.invocations.get_invocations(
            workflow_id=self.galaxy_workflow.get_encoded_id(),
            history_id=self.galaxy_history.get_encoded_id(),
        )
        print(invocations)
        if len(invocations) == 1:
            return invocations[0]["id"]
        elif len(invocations) == 0:
            return None
        else:
            raise Exception(f"Multiple wf invocations for {self.galaxy_workflow}")

    @property
    def galaxy_invocation(self):
        """Galaxy object using bioblend."""
        if getattr(self, "_galaxy_invocation", None) is None:
            self._galaxy_invocation = self._get_galaxy_invocation()
        return self._galaxy_invocation

    def _get_galaxy_invocation(self):
        """Get galaxy object using bioblend."""
        workflow_invocation_id = self.get_encoded_id()

        if workflow_invocation_id is None:
            return None
        else:
            return (
                self.galaxy_workflow.galaxy_user.bioblend_gi.workflows.show_invocation(
                    workflow_id=self.galaxy_workflow.get_encoded_id(),
                    invocation_id=workflow_invocation_id,
                )
            )

    @property
    def percentage_done(self) -> float:
        """Retrieve percentage of jobs done for the invocation."""
        if self.status == self.WorkflowInvocationStatus.DONE:
            return 100.0

        workflow_invocation_id = self.get_encoded_id()
        if workflow_invocation_id:
            step_jobs_summary = self.galaxy_workflow.galaxy_user.bioblend_gi.invocations.get_invocation_step_jobs_summary(
                workflow_invocation_id
            )
        else:
            self.status = self.WorkflowInvocationStatus.SCHEDULED
            self.save()
            return 0

        count_states = defaultdict(int)
        if len(step_jobs_summary) >= 1:
            for step in step_jobs_summary:
                for key in step["states"].keys():
                    count_states[key] += 1
            percentage_done = count_states.get("ok", 0) / len(step_jobs_summary)
            if percentage_done == 1:
                self.status = self.WorkflowInvocationStatus.DONE
                self.save()
            elif "error" in count_states.keys():
                self.status = self.WorkflowInvocationStatus.ERROR
                self.save()
            elif "paused" in count_states.keys():
                self.status = self.WorkflowInvocationStatus.PAUSED
                self.save()
            else:
                self.status = self.WorkflowInvocationStatus.RUNNING
                self.save()
            return percentage_done * 100
        else:
            self.status = self.WorkflowInvocationStatus.SCHEDULED
            self.save()
            return 0

    def synchronize(self):
        """Synchronize data from Galaxy instance."""
        self.galaxy_history.synchronize()
        if self.status in {
            self.WorkflowInvocationStatus.DONE,
            self.WorkflowInvocationStatus.ERROR,
        }:
            return

        if self.galaxy_invocation is not None:
            self.galaxy_state = self.galaxy_invocation["state"]
            self.save()

    def get_galaxy_invocation(self):
        galaxy_user = self.galaxy_workflow.galaxy_user
        gi = galaxy_user.bioblend_gi
        invocation = self.galaxy_invocation

        def job_detail(steps):
            param_keys_to_remove = {
                "__workflow_invocation_uuid__",
                "chromInfo",
                "dbkey",
            }
            if invocation is not None:
                for step in invocation["steps"]:
                    if step["job_id"] is not None:
                        job = gi.jobs.show_job(step["job_id"], full_details=True)
                        tool = gi.tools.show_tool(job["tool_id"], io_details=True)
                        metrics = job["job_metrics"] if "job_metrics" in job else []
                        yield (
                            {
                                "order_index": step["order_index"],
                                "job_id": step["job_id"],
                                "step_id": step["order_index"],
                                "tool": tool,
                                "job": {
                                    "create_time": job["create_time"],
                                    "update_time": job["update_time"],
                                    "metrics": metrics,
                                    "state": job["state"],
                                    "params": dict(
                                        [
                                            (key, json.loads(param))
                                            for (key, param) in job["params"].items()
                                            if key not in param_keys_to_remove
                                        ]
                                    ),
                                },
                            }
                        )

        if invocation is not None:
            return {
                "invocation": invocation,
                "steps": list(job_detail(invocation["steps"])),
            }
        else:
            return None

    def get_workflow_job_parameters(self):
        invocation = self.get_galaxy_invocation()

        def job_detail(steps):
            if invocation is not None:
                for step in invocation["steps"]:
                    if step["job"]["params"] is not None:
                        # job = gi.jobs.show_job(step["job_id"])
                        yield (step["order_index"], step["job"]["params"])

        if invocation is not None:
            return dict(job_detail(invocation["steps"]))
        else:
            return None


class GalaxyDataset(models.Model):
    galaxy_history = models.ForeignKey(GalaxyHistory, on_delete=models.CASCADE)
    name = models.CharField(null=False, max_length=150)
    extension = models.CharField(null=False, max_length=50)
    galaxy_state = models.CharField(max_length=100, null=True)
    """State on the galaxy side."""

    class Meta:
        verbose_name = _("GalaxyDataset")
        verbose_name_plural = _("GalaxyDatasets")

    def __str__(self):
        return f"{self.id} - {self.name} - {self.galaxy_history}"

    def synchronize(self):
        """Synchronize state from Galaxy instance."""
        if self.galaxy_state not in DATASET_TERMINAL_STATES:
            dataset = self.get_galaxy_dataset()
            self.galaxy_state = dataset["state"]
            self.save()

    def get_galaxy_dataset(self):
        gi = self.galaxy_history.galaxy_user.bioblend_gi
        history_id = self.galaxy_history.get_encoded_id()
        datasets = gi.datasets.get_datasets(
            history_id=history_id, extension=self.extension, name=self.name
        )
        if len(datasets) == 1:
            dataset = datasets[0]
            return gi.datasets.show_dataset(dataset["id"])
        elif len(datasets) > 1:
            raise Exception(
                f"Multiple datasets for pattern {self.name} in history {history_id}"
            )
        else:
            raise Exception(
                "No dataset available for pattern"
                + f"{self.name} in history {history_id}"
            )
