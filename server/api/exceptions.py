from rest_framework.exceptions import APIException


class RessourceAlreadyExists(APIException):
    status_code = 409
    default_detail = "The ressource already exists."


class PhenotypeDoesNotExist(APIException):
    status_code = 409
    default_detail = "The ressource already exists."
