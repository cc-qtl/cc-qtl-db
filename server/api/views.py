# Pandas
# import pandas as pd

# Django
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.http.request import QueryDict
from django.shortcuts import get_object_or_404
from django.db import transaction, DataError
from django.db.models import ProtectedError
from django.db.utils import IntegrityError
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings


# Guardian
from guardian.shortcuts import assign_perm, get_perms
import pandas as pd

# DRF
from rest_framework import (
    views,
    viewsets,
    status,
    serializers,
    mixins,
    authentication,
    generics,
)
from rest_framework.permissions import IsAuthenticated


# from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import action


from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.exceptions import NotFound

# DRF guardian
from rest_framework_guardian import filters

from .exceptions import RessourceAlreadyExists

from bioblend import ConnectionError
from bioblend.galaxy.datasets import DatasetStateException


# Serializers
from .serializers import (
    ChangePasswordSerializer,
    CoefficientListSerializer,
    ExperimentLightSerializer,
    FounderSerializer,
    GalaxyHistorySerializer,
    GalaxyUserSerializer,
    GalaxyWorkflowSerializer,
    GroupSerializer,
    HaplotypeListSerializer,
    HaplotypeSerializer,
    LodScoreSerializer,
    LodScoreSerializerList,
    OntologySerializer,
    PeakGenesListSerializer,
    PeakGenesSerializer,
    PeakSerializer,
    PeakSerializerWithPheno,
    PhenotypeDetailsSerializer,
    PhenotypePatchSerializer,
    PhenotypeSerializer,
    ProjectGroupRoleSerializer,
    ProjectGroupSerializer,
    ProjectMemberSerializer,
    ProjectRoleSerializer,
    RoleGroupSerializer,
    SnpsAssociationListSerializer,
    SnpsAssociationSerializer,
    TopSnpsListSerializer,
    TopSnpsSerializer,
    UserSerializer,
    ProjectSerializer,
    TeamSerializer,
    RoleSerializer,
    CclineSerializer,
    NestedExperimentCclineSerializer,
    ExperimentSerializer,
    PhenotypeCategorySerializer,
    PhenotypeValueSerializer,
    AnalysisSerializer,
    CoefficientSerializer,
)

# Models
from api.models import (
    Coefficient,
    Founder,
    GalaxyUser,
    GalaxyWorkflow,
    Haplotype,
    LodScore,
    Ontology,
    Peak,
    PeakGene,
    Phenotype,
    Project,
    ProjectRole,
    Role,
    ProjectGroup,
    SnpsAssociation,
    Team,
    RoleGroup,
    Experiment,
    Ccline,
    ExperimentCcline,
    TopSnp,
    PhenotypeCategory,
    PhenotypeValue,
    Analysis,
)


# Permissions
from .permissions import CustomObjectPermissions, CustomPermissions, IsAdminOrIsSelf

PERMISSION_NAMES = ["add", "change", "delete", "view"]


# Settings
from django.conf import settings


# Create your views here.


User = get_user_model()


class ErrorDataResponse:
    def __init__(self, type, message, status):
        self.type = type
        self.message = message
        self.status = status

    def get_error_response(self):
        return Response(
            {"type": self.type, "message": self.message}, status=self.status
        )


galaxy_connection_error = ErrorDataResponse(
    "galaxy",
    f"Not able to reach GalaxyUser.objects.get(username=settings.GALAXY_USERNAME).galaxy_instance.name instance",
    status.HTTP_503_SERVICE_UNAVAILABLE,
)


class AppGroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.exclude(projectgroup__isnull=False).exclude(
        team__isnull=False
    )
    serializer_class = GroupSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.exclude(username__exact="AnonymousUser")
    serializer_class = UserSerializer
    permission_classes = [CustomPermissions]

    # permission_classes = [CustomObjectPermissions]
    # permission_classes = [DjangoModelPermissions]
    # permission_classes = [DjangoObjectPermissions]
    # filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request):
        current_user = User.objects.get(pk=request.user.id)
        queryset = self.get_queryset()
        serializer = self.get_serializer(
            queryset, many=True, context={"request": request}
        )
        data = serializer.data
        for user in data:
            member_permissions = set()
            user_db = User.objects.get(pk=user["id"])
            for perm in get_perms(current_user, user_db):
                member_permissions.add(perm)
            user["permissions"] = member_permissions
        return Response(data)

    def get_serializer_class(self):
        if self.action == "password_change":
            return ChangePasswordSerializer
        return self.serializer_class

    @action(
        detail=True,
        methods=["put"],
        name="Change Password",
        permission_classes=[IsAdminOrIsSelf],
    )
    def password_change(self, request, pk=None):
        """Change the user's password."""
        serializer = ChangePasswordSerializer(
            data=request.data, context={"request": request}
        )
        if serializer.is_valid():
            self.partial_update(request)
            return Response("Password updated successfully")
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CurrentUserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Get the id of the authenticated user
    """

    http_method_names = ["get"]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, format=None):
        user = User.objects.get(pk=request.user.id)
        serializer = UserSerializer(user)
        return Response(serializer.data)


class GalaxyWorkflowViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = GalaxyWorkflow.objects.filter(name="ccqtl-wf")
    serializer_class = GalaxyWorkflowSerializer
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    # def list(self, request):
    #     queryset = self.filter_queryset(self.get_queryset())


class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows projects to be viewed or edited.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    @action(detail=False)
    def count(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        return Response({"count": queryset.count()})

    @action(detail=True)
    def permissions(self, request, pk=None):
        current_user = User.objects.get(pk=request.user.id)
        permissions_set = set()
        permissions_set.update(get_perms(current_user, Project.objects.get(pk=pk)))
        for project_group in ProjectGroup.objects.filter(project=pk):
            permissions_set.update(get_perms(current_user, project_group))

        return Response(permissions_set)

    def perform_destroy(self, instance):
        gprs = ProjectGroup.objects.filter(project=instance.id)
        for gpr in gprs:
            group = gpr.group_ptr
            group_id = group.id
            group = Group.objects.get(pk=group_id)
            group.delete()
        instance.delete()


class ProjectMemberViewSet(
    # viewsets.ModelViewSet
    # mixins.UpdateModelMixin,
    # mixins.ListModelMixin,
    # viewsets.GenericViewSet,
    viewsets.ReadOnlyModelViewSet
):
    serializer_class = ProjectMemberSerializer

    def get_queryset(self):
        project_group_users = User.objects.filter(
            groups__projectgroup__project__id=self.kwargs["project_pk"]
        )
        teams_group_users = User.objects.filter(
            groups__team__projects__id=self.kwargs["project_pk"],
        )

        return project_group_users.union(teams_group_users)

    def list(self, request, project_pk=None):
        serializer = self.get_serializer(
            self.get_queryset(),
            many=True,
            context={"request": request, "project_pk": int(project_pk)},
        )
        data = serializer.data
        for member in data:
            member_permissions = set()
            current_user = User.objects.get(pk=member["id"])
            for perm in get_perms(current_user, Project.objects.get(pk=project_pk)):
                member_permissions.add(perm)

            # get all project groups
            project_groups = ProjectGroup.objects.filter(project=project_pk)

            for pg in project_groups:
                for perm in get_perms(current_user, pg):
                    member_permissions.add(perm)

            member["permissions"] = member_permissions
        return Response(data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        # instance = self.get_object()
        instance = User.objects.get(pk=kwargs.get("pk"))
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class CclineViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CclineSerializer
    queryset = Ccline.objects.all()


class FounderViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = FounderSerializer
    queryset = Founder.objects.all()


class ExperimentViewSet(viewsets.ModelViewSet):
    serializer_class = ExperimentLightSerializer
    queryset = Experiment.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def count(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        return Response({"count": queryset.count()})


class ProjectExperimentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows experiment to be viewed or edited.
    """

    serializer_class = ExperimentSerializer
    queryset = Experiment.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def create(self, request, *args, **kwargs):
        # raw_experiment_metadata = request.data.pop("experiment-metadata")
        up_file = request.data.pop("file")
        project = Project.objects.get(pk=kwargs["project_pk"])
        project_serializer = ProjectSerializer(project, context={"request": request})
        request.data["project"] = project_serializer.data["id"]
        # raw_experiment_metadata = self.initial_data["experiment-metadata"]
        # experiment_metadata = json.loads(raw_experiment_metadata[0])
        # column_exp_metadata = {md["colIndex"]: md for md in experiment_metadata}
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        experiment_df = pd.read_csv(up_file[0].file, sep=",|;", engine="python")
        phenotype_index_start = 3
        phenotype_columns = experiment_df.columns[phenotype_index_start:]
        experiment_df_dtypes = experiment_df.dtypes
        try:
            with transaction.atomic():
                # get or create all the phenotypes for this experiment
                phenotype_objects = [
                    Phenotype.objects.get_or_create(
                        name=phenotype_col,
                        primitive=Phenotype.get_primitive(
                            experiment_df_dtypes[phenotype_col]
                        ),
                    )[0]
                    for phenotype_col in phenotype_columns
                ]
                self.perform_create(serializer)
                experiment = serializer.instance

                # Add phenotypes to experiments
                for pheno_obj in phenotype_objects:
                    experiment.phenotypes.add(pheno_obj)

                for _, row in experiment_df.iterrows():
                    try:
                        cc_line_m = Ccline.objects.get(id_cc=row["Mother_Line"])
                    except KeyError:
                        raise serializers.ValidationError(
                            'The name of the second column must be : "Mother_Line"'
                        )
                    except Ccline.DoesNotExist:
                        raise serializers.ValidationError(
                            f"Ccline {row['Mother_Line']} does not exist."
                        )
                    except Exception as e:
                        raise serializer.ValidationError(e)
                    try:
                        cc_line_f = Ccline.objects.get(id_cc=row["Father_Line"])
                    except KeyError:
                        raise serializers.ValidationError(
                            'The name of the third column must be : "Father_Line"'
                        )
                    except Ccline.DoesNotExist:
                        raise serializers.ValidationError(
                            f"Ccline {row['Father_Line']} does not exist."
                        )
                    except Exception as e:
                        raise serializer.ValidationError(e)

                    try:
                        individual_id = row["Individual_ID"]
                    except KeyError:
                        raise serializers.ValidationError(
                            'The name of the first column must be : "Individual_ID"'
                        )
                    except Exception as e:
                        raise serializer.ValidationError(e)

                    try:
                        sex = row["Sex"]
                    except KeyError:
                        raise serializers.ValidationError(
                            'The name of the fourth column must be : "Sex"'
                        )
                    except Exception as e:
                        raise serializer.ValidationError(e)

                    experiment_cc_line = ExperimentCcline.objects.create(
                        experiment=experiment,
                        cc_line_id=individual_id,
                        cc_line_m=cc_line_m,
                        cc_line_f=cc_line_f,
                        sex=sex,
                    )

                    for idx, phenotype_col in enumerate(phenotype_columns):
                        PhenotypeValue.objects.create(
                            experiment_cc_line=experiment_cc_line,
                            value=row[phenotype_col],
                            phenotype=phenotype_objects[idx],
                        )

                project_roles = ProjectRole.objects.all()
                for project_role in project_roles:
                    project_group = ProjectGroup.objects.get(
                        project=project,
                        project_role=project_role,
                    )
                    permissions = project_role.permissions.filter(
                        content_type__model="experiment"
                    )
                    for perm in permissions:
                        assign_perm(perm, project_group)
                        assign_perm(perm, project_group, experiment)
        except IntegrityError as e:
            raise serializers.ValidationError(e)
        except DataError as e:
            raise serializers.ValidationError(e)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    # def get_object(self):
    #     obj = get_object_or_404(
    #         self.get_queryset(), pk=self.kwargs["pk"], project=self.kwargs["project_pk"]
    #     )
    #     self.check_object_permissions(self.request, obj)
    #     return obj

    def list(self, request, project_pk=None):
        queryset = Experiment.objects.filter(project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        serializer = ExperimentSerializer(
            filtered_qs, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None, project_pk=None):
        queryset = Experiment.objects.filter(pk=pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        experiment = get_object_or_404(filtered_qs, pk=pk)
        serializer = ExperimentSerializer(experiment, context={"request": request})
        return Response(serializer.data)

    @action(detail=True)
    def permissions(self, request, pk=None, project_pk=None):
        current_user = User.objects.get(pk=request.user.id)
        permissions = get_perms(
            current_user, Experiment.objects.get(pk=pk, project=project_pk)
        )
        return Response(permissions)

    @action(detail=True)
    def phenotype_types(self, request, pk=None, project_pk=None):
        return Response(self.get_object().get_experiment_phenotype_types())


class ExperimentCclineViewSet(viewsets.ModelViewSet):
    serializer_class = NestedExperimentCclineSerializer
    queryset = ExperimentCcline.objects.all()

    def list(self, request, project_pk=None, experiment_pk=None):
        queryset = ExperimentCcline.objects.filter(
            experiment__project=project_pk, experiment=experiment_pk
        )
        serializer = NestedExperimentCclineSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None, project_pk=None, experiment_pk=None):
        queryset = ExperimentCcline.objects.filter(
            pk=pk, experiment__project=project_pk, experiment=experiment_pk
        )
        experiment_ccline = get_object_or_404(queryset, pk=pk)
        serializer = NestedExperimentCclineSerializer(
            experiment_ccline, context={"request": request}
        )
        return Response(serializer.data)


# class PhenotypeCategoryResolveViewSet(
#     mixins.CreateModelMixin,
#     # mixins.ListModelMixin,
#     # mixins.RetrieveModelMixin,
#     viewsets.GenericViewSet,
# ):
#     queryset = PhenotypeCategory.objects.all()
#     serializer_class = PhenotypeCategorySerializer
#     permission_classes = [IsAuthenticated]

#     def create(self, request):
#         response = []
#         for phenotypeCategory in request.data:
#             try:
#                 var_cat_qs = self.get_queryset().get(
#                     name=phenotypeCategory["name"],
#                     # nature=phenotypeCategory["nature"],
#                     # location=phenotypeCategory["location"],
#                     # datatype=phenotypeCategory["datatype"],
#                     # dataclass=phenotypeCategory["dataclass"],
#                 )
#                 serializer = self.get_serializer(var_cat_qs)
#                 responseData = serializer.data
#                 responseData["colid"] = phenotypeCategory["colid"]
#                 response.append(responseData)

#             except Exception:
#                 response.append({"colid": phenotypeCategory["colid"]})
#         return Response(response)


class PhenotypeViewSet(viewsets.ModelViewSet):
    queryset = Phenotype.objects.all()
    serializer_class = PhenotypeSerializer
    permission_classes = [CustomPermissions]


class ProjectAnalysisPhenotypeViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PhenotypeSerializer
    queryset = Phenotype.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, project_pk=None, analysis_pk=None):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            serializer = PhenotypeSerializer(
                filtered_qs.get(pk=analysis_pk).phenotypes.all(),
                many=True,
                context={"request": request},
            )
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, pk=None, project_pk=None, analysis_pk=None):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(pk=pk)
            if phenotype_qs.exists():
                serializer = PhenotypeSerializer(
                    phenotype_qs.get(),
                    context={"request": request},
                )
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ProjectAnalysisPhenotypeLodScoreViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = LodScoreSerializer
    queryset = LodScore.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, project_pk=None, analysis_pk=None, phenotype_pk=None):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis_db = filtered_qs.get(pk=analysis_pk)
        if analysis_db:
            try:
                # load_lodscores_results(ids).apply_async().get()
                lodscore_qs = analysis_db.load_lod_score()
            except ConnectionError:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                exception_msg = str(e)
                if "significance-threshold.csv" in exception_msg:
                    print("No significance threshold")
                    pass
                else:
                    return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                if filtered_qs.count() == 0:
                    return Response(
                        "No load score found", status=status.HTTP_404_NOT_FOUND
                    )
                else:
                    serializer = LodScoreSerializerList(
                        lodscore_qs.filter(phenotype=phenotype_pk),
                        many=True,
                        context={"request": request},
                    )

                    return Response(serializer.data)

    def retrieve(
        self, request, pk=None, project_pk=None, analysis_pk=None, phenotype_pk=None
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis_db = filtered_qs.get(id=analysis_pk)
        if analysis_db:
            try:
                analysis_db.load_lod_score()
            except ConnectionError:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                exception_msg = str(e)
                if "significance-threshold.csv" in exception_msg:
                    print("No significance threshold")
                    pass
                else:
                    return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                    pk=phenotype_pk
                )
                if phenotype_qs.count() > 0:
                    phenotype = get_object_or_404(phenotype_qs, pk=phenotype_pk)
                    lodscore = get_object_or_404(
                        LodScore.objects.filter(
                            analysis=analysis_pk,
                            phenotype=phenotype.id,
                        ),
                        pk=pk,
                    )
                    serializer = LodScoreSerializer(
                        lodscore, context={"request": request}
                    )

                    return Response(serializer.data)
                else:
                    return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ProjectAnalysisPhenotypePeakViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PeakSerializer
    queryset = Peak.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, project_pk=None, analysis_pk=None, phenotype_pk=None):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                pk=phenotype_pk
            )
            if phenotype_qs.exists():
                peak_qs = Peak.objects.filter(
                    analysis=analysis_pk,
                    phenotype=get_object_or_404(phenotype_qs, pk=phenotype_pk),
                )
                serializer = PeakSerializer(
                    peak_qs, many=True, context={"request": request}
                )
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(
        self, request, pk=None, project_pk=None, analysis_pk=None, phenotype_pk=None
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                pk=phenotype_pk
            )
            if phenotype_qs.exists():
                peak = get_object_or_404(
                    Peak.objects.filter(
                        analysis=analysis_pk,
                        phenotype=get_object_or_404(phenotype_qs, pk=phenotype_pk).id,
                    ),
                    pk=pk,
                )
                serializer = PeakSerializer(peak, context={"request": request})
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ProjectAnalysisPhenotypePeakCoefficientViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CoefficientSerializer
    queryset = Coefficient.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(
        self,
        request,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis_db = filtered_qs.get(pk=analysis_pk)
        if analysis_db:
            try:
                peak_db = Peak.objects.get(pk=peak_pk, phenotype=phenotype_pk)
                coefficient_qs = peak_db.load_coefficient()
            except ConnectionError:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                return Response(str(e), status=status.HTTP_404_NOT_FOUND)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                if coefficient_qs.count() == 0:
                    return Response(
                        "No coefficient found", status=status.HTTP_404_NOT_FOUND
                    )
                else:
                    serializer = CoefficientListSerializer(
                        coefficient_qs, many=True, context={"request": request}
                    )
                    return Response(serializer.data)

    def retrieve(
        self,
        request,
        pk=None,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                pk=phenotype_pk
            )
            if phenotype_qs.exists():
                peak = get_object_or_404(
                    Peak.objects.filter(
                        analysis=analysis_pk,
                        phenotype=get_object_or_404(phenotype_qs, pk=phenotype_pk).id,
                    ),
                    pk=peak_pk,
                )
                print(peak)
                print(Coefficient.objects.filter(peak_coefficient=peak))
                print(pk)
                coefficient = get_object_or_404(
                    Coefficient.objects.filter(peak_coefficient=peak), pk=pk
                )
                serializer = CoefficientSerializer(
                    coefficient, context={"request": request}
                )
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ProjectAnalysisPhenotypePeakHaplotypeViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = HaplotypeSerializer
    queryset = Haplotype.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(
        self,
        request,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis_db = filtered_qs.get(pk=analysis_pk)

        if analysis_db:
            try:
                # load_haplotypes.apply_async(args=[ids]).get()
                peak_db = Peak.objects.get(pk=peak_pk, phenotype=phenotype_pk)
                haplotype_qs = peak_db.load_haplotypes()
            except ConnectionError:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                return Response(str(e), status=status.HTTP_404_NOT_FOUND)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                if haplotype_qs.count() == 0:
                    return Response(
                        "No haplotypes found", status=status.HTTP_404_NOT_FOUND
                    )

                else:
                    serializer = HaplotypeListSerializer(
                        haplotype_qs, many=True, context={"request": request}
                    )
                    return Response(serializer.data)

    def retrieve(
        self,
        request,
        pk=None,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                pk=phenotype_pk
            )
            if phenotype_qs.exists():
                peak = get_object_or_404(
                    Peak.objects.filter(
                        analysis=analysis_pk,
                        phenotype=get_object_or_404(phenotype_qs, pk=phenotype_pk).id,
                    ),
                    pk=peak_pk,
                )
                haplotype = get_object_or_404(
                    Haplotype.objects.filter(peak_haplotype=peak), pk=pk
                )
                serializer = HaplotypeSerializer(
                    haplotype, context={"request": request}
                )
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ProjectAnalysisPhenotypePeakSnpsAssociationViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = SnpsAssociationSerializer
    queryset = SnpsAssociation.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(
        self,
        request,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis_db = filtered_qs.get(pk=analysis_pk)

        if analysis_db:
            try:
                peak_db = Peak.objects.get(pk=peak_pk, phenotype=phenotype_pk)
                snps_association_qs = peak_db.load_snps_association()
            except ConnectionError:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                return Response(str(e), status=status.HTTP_404_NOT_FOUND)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                if snps_association_qs.count() == 0:
                    return Response(
                        "No snps association found",
                        status=status.HTTP_404_NOT_FOUND,
                    )
                else:
                    serializer = SnpsAssociationListSerializer(
                        snps_association_qs, many=True, context={"request": request}
                    )
                    return Response(serializer.data)

    def retrieve(
        self,
        request,
        pk=None,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                pk=phenotype_pk
            )
            if phenotype_qs.exists():
                peak = get_object_or_404(
                    Peak.objects.filter(
                        analysis=analysis_pk,
                        phenotype=get_object_or_404(phenotype_qs, pk=phenotype_pk).id,
                    ),
                    pk=peak_pk,
                )
                snps_association = get_object_or_404(
                    SnpsAssociation.objects.filter(peak_snps_association=peak), pk=pk
                )
                serializer = SnpsAssociationSerializer(
                    snps_association, context={"request": request}
                )
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ProjectAnalysisPhenotypePeakTopSnpsViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = TopSnpsSerializer
    queryset = TopSnp.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(
        self,
        request,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis_db = filtered_qs.get(pk=analysis_pk)
        if analysis_db:
            try:
                # load_top_snps.apply_async(args=[ids]).get()
                peak_db = Peak.objects.get(pk=peak_pk, phenotype=phenotype_pk)
                top_snps_qs = peak_db.load_top_snps()

            except ConnectionError:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                return Response(str(e), status=status.HTTP_404_NOT_FOUND)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                if top_snps_qs.count() == 0:
                    return Response(
                        "No top snps found",
                        status=status.HTTP_404_NOT_FOUND,
                    )
                else:
                    serializer = TopSnpsListSerializer(
                        top_snps_qs, many=True, context={"request": request}
                    )
                    return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(
        self,
        request,
        pk=None,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                pk=phenotype_pk
            )
            if phenotype_qs.exists():
                peak = get_object_or_404(
                    Peak.objects.filter(
                        analysis=analysis_pk,
                        phenotype=get_object_or_404(phenotype_qs, pk=phenotype_pk).id,
                    ),
                    pk=peak_pk,
                )
                top_snps = get_object_or_404(
                    TopSnp.objects.filter(peak_top_snp=peak), pk=pk
                )
                serializer = TopSnpsSerializer(top_snps, context={"request": request})
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ProjectAnalysisPhenotypePeakGenesViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PeakGenesSerializer
    queryset = PeakGene.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(
        self,
        request,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis_db = filtered_qs.get(pk=analysis_pk)

        if analysis_db:
            try:
                # load_peak_genes.apply_async(args=[ids]).get()
                peak_db = Peak.objects.get(pk=peak_pk, phenotype=phenotype_pk)
                genes_qs = peak_db.load_peak_genes()
            except ConnectionError:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                return Response(str(e), status=status.HTTP_404_NOT_FOUND)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                if genes_qs.count() == 0:
                    return Response(
                        "No genes found",
                        status=status.HTTP_404_NOT_FOUND,
                    )
                else:
                    serializer = PeakGenesListSerializer(
                        genes_qs, many=True, context={"request": request}
                    )
                    return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(
        self,
        request,
        pk=None,
        project_pk=None,
        analysis_pk=None,
        phenotype_pk=None,
        peak_pk=None,
    ):
        queryset = Analysis.objects.filter(pk=analysis_pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            phenotype_qs = filtered_qs.get(pk=analysis_pk).phenotypes.filter(
                pk=phenotype_pk
            )
            if phenotype_qs.exists():
                peak = get_object_or_404(
                    Peak.objects.filter(
                        analysis=analysis_pk,
                        phenotype=get_object_or_404(phenotype_qs, pk=phenotype_pk).id,
                    ),
                    pk=peak_pk,
                )
                genes = get_object_or_404(
                    PeakGene.objects.filter(peak_gene=peak), pk=pk
                )
                serializer = PeakGenesSerializer(genes, context={"request": request})
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class PhenotypeDetailsViewSet(viewsets.ModelViewSet):
    queryset = Phenotype.objects.all()
    serializer_class = PhenotypeDetailsSerializer
    permission_classes = [CustomPermissions]

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = PhenotypePatchSerializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

    @action(detail=False)
    def permissions(self, request, pk=None):
        current_user = request.user

        permissions_set = set(
            [
                perm
                for perm in PERMISSION_NAMES
                if current_user.has_perm(f"api.{perm}_phenotype")
            ]
        )
        return Response(permissions_set)


class PhenotypeCategoryViewSet(viewsets.ModelViewSet):
    queryset = PhenotypeCategory.objects.all()
    serializer_class = PhenotypeCategorySerializer
    permission_classes = [CustomPermissions]

    def perform_destroy(self, instance):
        try:
            instance.delete()
        except ProtectedError as e:
            raise serializers.ValidationError(
                f"The phenotype category {instance.name} describes uploaded experiment"
                + " phenotype. You cannot delete it !!"
            )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()

        if "ontologies" in request.data:
            with transaction.atomic():
                if type(request.data) is QueryDict:
                    req_ontologies = request.data.getlist("ontologies")
                else:
                    req_ontologies = request.data["ontologies"]
                if len(req_ontologies) > 0:
                    instance.ontologies.clear()
                for ro in req_ontologies:
                    try:
                        onto = Ontology.objects.get(pk=ro["id"])
                        instance.ontologies.add(onto)
                    except ObjectDoesNotExist as e:
                        raise NotFound(detail=e)
                    except Exception as e:
                        raise serializers.ValidationError(e)

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        ontologies = (
            request.data.pop("ontologies") if "ontologies" in request.data else []
        )
        try:
            with transaction.atomic():
                self.perform_create(serializer)
                phenotypeCategory = serializer.instance
                for ontology in ontologies:
                    ontoEntry = Ontology.objects.get(pk=ontology["id"])
                    phenotypeCategory.ontologies.add(ontoEntry)
        except ObjectDoesNotExist as e:
            raise NotFound(detail=e)
        except Exception as e:
            raise serializers.ValidationError(e)
        else:
            headers = self.get_success_headers(serializer.data)
            return Response(
                serializer.data, status=status.HTTP_201_CREATED, headers=headers
            )

    @action(detail=False)
    def permissions(self, request, pk=None):
        current_user = request.user
        permissions_set = set(
            [
                perm
                for perm in PERMISSION_NAMES
                if current_user.has_perm(f"api.{perm}_phenotypecategory")
            ]
        )
        return Response(permissions_set)


class OntologyViewSet(viewsets.ModelViewSet):
    queryset = Ontology.objects.all()
    serializer_class = OntologySerializer


class PhenotypeValueViewSet(viewsets.ModelViewSet):
    queryset = PhenotypeValue.objects.all()
    serializer_class = PhenotypeValueSerializer

    def get_queryset(self):
        return PhenotypeValue.objects.filter(
            project=self.kwargs["project_pk"], experiment=self.kwargs["experiment_pk"]
        )


class TeamViewSet(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    # permission_classes = [CustomObjectPermissions]
    # filter_backends = [filters.ObjectPermissionsFilter]


class ProjectRoleViewSet(viewsets.ModelViewSet):
    queryset = ProjectRole.objects.all()
    serializer_class = ProjectRoleSerializer


class ProjectGroupRoleViewSet(viewsets.ModelViewSet):
    queryset = ProjectGroup.objects.all()
    serializer_class = ProjectGroupRoleSerializer
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]


class ProjectGroupViewSet(viewsets.ModelViewSet):
    # queryset = ProjectGroup.objects.all()
    serializer_class = ProjectGroupSerializer
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def get_queryset(self):
        return ProjectGroup.objects.filter(project=self.kwargs["project_pk"])

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), pk=self.kwargs["pk"])
        self.check_object_permissions(self.request, obj)
        return obj

    def list(self, request, project_pk=None):
        # perm_queryset = get_objects_for_user(request.user, "api.view_analysis")
        queryset = ProjectGroup.objects.filter(project=project_pk)
        serializer = self.get_serializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None, project_pk=None):
        # perm_queryset = get_objects_for_user(request.user, "api.view_analysis")
        queryset = ProjectGroup.objects.filter(pk=pk, project=project_pk)
        instance = get_object_or_404(queryset, pk=pk)
        serializer = self.get_serializer(instance, context={"request": request})
        data = serializer.data
        return Response(data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        if "users" in request.data:
            with transaction.atomic():
                if type(request.data) is QueryDict:
                    req_users = request.data.getlist("users")
                else:
                    req_users = request.data["users"]
                users = User.objects.filter(id__in=req_users)
                # Remove users in all projectgroup that match the project
                # to ensuer a user can be in only one projectgroup related
                # to this project
                project_groups = ProjectGroup.objects.filter(project=instance.project)
                for u in users:
                    for pg in project_groups:
                        pg.user_set.remove(u)
                users_to_add = [user.id for user in users]
                # users_to_add = validated_data.pop("user_set")
                instance.user_set.add(*users_to_add)
        if "userToRemove" in request.data:
            with transaction.atomic():
                # Remove users from project group
                req_user_to_rm = request.data.get("userToRemove")
                user_to_remove = User.objects.get(id=req_user_to_rm)
                instance.user_set.remove(user_to_remove)

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class RoleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    # permission_classes = [CustomObjectPermissions]
    # filter_backends = [filters.ObjectPermissionsFilter]


class RoleGroupViewSet(viewsets.ModelViewSet):
    queryset = RoleGroup.objects.all()
    serializer_class = RoleGroupSerializer


class PeakViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PeakSerializerWithPheno
    queryset = Peak.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, project_pk=None, analysis_pk=None):
        queryset = self.peaks_per_analysis_queryset(project_pk, analysis_pk)
        if queryset.count() == 0:
            # Empty queryset,
            # try to load dataset from Galaxy
            analysis_db = Analysis.objects.get(id=analysis_pk)
            try:
                analysis_db.load_peaks()

            except ConnectionError as e:
                return galaxy_connection_error.get_error_response()
            except DatasetStateException as e:
                return Response(str(e), status=status.HTTP_404_NOT_FOUND)
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                if queryset.count() == 0:
                    return Response("No peak found", status=status.HTTP_404_NOT_FOUND)
                else:
                    return self.get_serialized_response(queryset, request)
        return self.get_serialized_response(queryset, request)

    def retrieve(self, request, pk=None, analysis_pk=None, project_pk=None):
        queryset = Peak.objects.filter(
            pk=pk, analysis=analysis_pk, analysis__project=project_pk
        )
        peak = get_object_or_404(queryset, pk=pk)
        serializer = PeakSerializerWithPheno(peak, context={"request": request})
        return Response(serializer.data)

    def get_serialized_response(self, queryset, request):
        serializer = PeakSerializerWithPheno(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def peaks_per_analysis_queryset(self, project_pk, analysis_pk):
        return Peak.objects.filter(
            analysis__project=project_pk,
            analysis=analysis_pk,
        )


class LodScoreViewSet(viewsets.ModelViewSet):
    serializer_class = LodScoreSerializer
    queryset = LodScore.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, project_pk=None, analysis_pk=None):
        queryset = LodScore.objects.filter(
            analysis__project=project_pk, analysis=analysis_pk
        )
        serializer = LodScoreSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None, analysis_pk=None, project_pk=None):
        queryset = LodScore.objects.filter(
            pk=pk, analysis=analysis_pk, analysis__project=project_pk
        )
        records = get_object_or_404(queryset, pk=pk)
        serializer = LodScoreSerializer(records, context={"request": request})
        return Response(serializer.data)


class GalaxyUserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = GalaxyUserSerializer
    queryset = GalaxyUser.objects.all()
    # permission_classes = [CustomObjectPermissions]
    # filter_backends = [filters.ObjectPermissionsFilter]

    @action(detail=False, methods=["get"])
    def create_history(self, request):
        galaxy_user = GalaxyUser.objects.get(username=settings.GALAXY_USERNAME)
        galaxy_history = galaxy_user.create_history()
        serializer = GalaxyHistorySerializer(galaxy_history)
        return Response(serializer.data)


class AnalysisViewSet(viewsets.ModelViewSet):
    serializer_class = AnalysisSerializer
    queryset = Analysis.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def count(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        return Response({"count": queryset.count()})


class ProjectAnalysisViewSet(viewsets.ModelViewSet):
    serializer_class = AnalysisSerializer
    queryset = Analysis.objects.all()
    permission_classes = [CustomObjectPermissions]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, project_pk=None):
        # perm_queryset = get_objects_for_user(request.user, "api.view_analysis")

        queryset = Analysis.objects.filter(project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        if filtered_qs.exists():
            serializer = AnalysisSerializer(
                filtered_qs, many=True, context={"request": request}
            )
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, pk=None, project_pk=None):
        queryset = Analysis.objects.filter(pk=pk, project=project_pk)
        filtered_qs = self.filter_queryset(queryset)
        analysis = get_object_or_404(filtered_qs, pk=pk)
        # if analysis.galaxy_workflow_invocation:
        analysis.synchronize()
        analysis.load_lod_score()
        analysis.load_peaks()
        serializer = AnalysisSerializer(analysis, context={"request": request})
        return Response(serializer.data)

    @action(detail=False)
    def permissions(self, request, pk=None, project_pk=None):
        current_user = request.user
        return Response(
            [
                perm.split(".")[1].split("_")[0]
                for perm in current_user.get_all_permissions()
                if perm.endswith("analysis")
            ]
        )

    # @action(detail=True)
    # def lodscores(self, request, pk=None, project_pk=None):
    #     # Get the lodscores
    #     queryset = LodScore.objects.filter(analysis=pk)

    #     # If no load score in databse
    #     if queryset.count() == 0:
    #         analysis_db = Analysis.objects.get(id=pk)
    #         # ids = (pk, analysis_db.galaxy_history.id, None)
    #         try:
    #             analysis_db.load_lodscores_results()
    #         except ConnectionError:
    #             return galaxy_connection_error.get_error_response()
    #         except DatasetStateException as e:
    #             exception_msg = str(e)
    #             if "significance-threshold.csv" in exception_msg:
    #                 print("No significance threshold")
    #                 pass
    #             else:
    #                 return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
    #         except Exception as e:
    #             return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
    #         else:
    #             if queryset.count() == 0:
    #                 return Response(
    #                     "No load score found", status=status.HTTP_404_NOT_FOUND
    #                 )
    #             # else:
    #             #     queryset = LodScore.objects.filter(analysis=pk)

    #     lodscores = LodScoreSerializer(queryset, many=True)
    #     return Response(lodscores.data)

    # @action(detail=True)
    # def haslodscores(self, request, pk=None, project_pk=None):

    #     lodscores_count = LodScore.objects.filter(analysis=pk).count()
    #     if lodscores_count > 0:
    #         return Response({"has_lodscores": True})
    #     else:
    #         return Response({"has_lodscores": False})

    # @action(detail=True)
    # def download_qtl2_data(self, request, pk=None, project_pk=None):
    #     queryset = Analysis.objects.get(id=pk)
    #     # download_formatted_qtl2_data()
    #     try:
    #         result = download_formatted_qtl2_data.apply_async(
    #             args=[queryset.galaxy_history.id]
    #         ).get()
    #     except ConnectionError:
    #         return galaxy_connection_error.get_error_response()
    #     except DatasetStateException as e:
    #         return Response(str(e), status=status.HTTP_404_NOT_FOUND)
    #     except Exception as e:
    #         return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
    #     else:
    #         return Response(
    #             {"archive_name": result, "project_id": project_pk, "analysis_id": pk}
    #         )

    @action(detail=True)
    def workflow_invocation_parameters(self, request, pk=None, project_pk=None):
        # if self.get_object().galaxy_workflow_invocation_id:
        try:
            wf_invocation = self.get_object().get_galaxy_invocation()
            wf_invocation["steps"].sort(key=lambda step: step["step_id"])
            return Response(wf_invocation)
        except ConnectionError:
            return galaxy_connection_error.get_error_response()
        # else:
        #     return Response(None)

    @action(detail=True)
    def workflow_job_parameters(self, request, pk=None, project_pk=None):
        return Response(self.get_object().get_workflow_job_parameters())


class WorkflowTools(views.APIView):
    authentication_classes = [JWTAuthentication, authentication.SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        wf_id = kwargs["wf_id"]
        wf_db = GalaxyWorkflow.objects.get(pk=wf_id)
        return Response(wf_db.get_tools())


class WorkflowDefaultParameters(views.APIView):
    authentication_classes = [JWTAuthentication, authentication.SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        wf_id = kwargs["wf_id"]
        wf_db = GalaxyWorkflow.objects.get(pk=wf_id)
        return Response(wf_db.get_default_parameters())


def get_job_params(job_ids, gi):
    for step_id, job_id in job_ids:
        if job_id is not None:
            yield (
                step_id,
                gi.tools.show_tool(job_id, io_details=True),
            )


class CcQtlWorkflows(views.APIView):
    authentication_classes = [JWTAuthentication, authentication.SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        gu = GalaxyUser.objects.get(username=settings.GALAXY_USERNAME)

        workflows = GalaxyWorkflow.objects.filter(name=settings.GALAXY_WF_NAME)
        serializer = GalaxyWorkflowSerializer(workflows, many=True)
        data = serializer.data

        return Response(data)
