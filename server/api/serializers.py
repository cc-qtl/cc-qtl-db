import pprint
import math

# Django
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.db import IntegrityError


# Django Rest Framework
from rest_framework import serializers


from guardian.shortcuts import assign_perm, get_perms

from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer


#
# , File
from .models import (
    Analysis,
    Ccline,
    Coefficient,
    Experiment,
    ExperimentCcline,
    Founder,
    GalaxyWorkflow,
    Haplotype,
    LodScore,
    Ontology,
    Peak,
    PeakGene,
    Phenotype,
    Project,
    ProjectGroup,
    ProjectRole,
    Role,
    RoleGroup,
    SnpsAssociation,
    Team,
    TopSnp,
    PhenotypeCategory,
    PhenotypeValue,
    GalaxyUser,
    GalaxyHistory,
)


PERMISSION_NAMES = ["add", "change", "delete", "view"]


pp = pprint.PrettyPrinter(indent=4)


User = get_user_model()


class ProjectGroupMembershipSerializer(serializers.ModelSerializer):
    project = serializers.SlugRelatedField(read_only=True, slug_field="project_name")
    project_role = serializers.SlugRelatedField(read_only=True, slug_field="name")

    class Meta:
        model = ProjectGroup
        fields = ("id", "project", "project_role")


class GroupMembershipSerializer(serializers.ModelSerializer):
    projectgroup = ProjectGroupMembershipSerializer(read_only=True)

    class Meta:
        model = Group
        fields = ("id", "name", "projectgroup")


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ("id", "name")


class UserSerializer(serializers.ModelSerializer):
    groups = GroupMembershipSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "password",
            "first_name",
            "last_name",
            "email",
            # "last_login",
            "groups",
            "is_staff",
            "is_active",
            "is_superuser",
        )
        extra_kwargs = {"password": {"write_only": True}}
        read_only_fields = ("is_superuser", "is_staff")

    def create(self, validated_data, **kwargs):
        password = validated_data.pop("password")
        user = super().create(validated_data, **kwargs)
        user.set_password(password)
        user.save()

        if "appGroup" in self.initial_data and self.initial_data["appGroup"]:
            app_group = self.initial_data.pop("appGroup")
            try:
                role_group = RoleGroup.objects.get(pk=app_group["id"])
            except RoleGroup.DoesNotExist:
                role_group = RoleGroup.objects.get(name__exact="app-user")
        else:
            role_group = RoleGroup.objects.get(name__exact="app-user")
        role_group.user_set.add(user)
        # permission user on object
        # this is necessary for current-user
        all_user_permissions = Permission.objects.filter(content_type__model="user")
        for perm in all_user_permissions:
            assign_perm(perm, user, user)

        # Assign permissions corresponding to the group to this new user
        # role_user_permissions = role_group.role.permissions.filter(
        #     content_type__model="user"
        # )
        # for perm in role_user_permissions:
        #     assign_perm(perm, role_group, user)

        return user


class ChangePasswordSerializer(serializers.Serializer):
    model = User

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(
        write_only=True,
        required=True,
        style={"input_type": "password", "placeholder": "Password"},
    )
    new_password = serializers.CharField(
        write_only=True,
        required=True,
        style={"input_type": "password", "placeholder": "Password"},
    )
    new_password_confirmation = serializers.CharField(
        write_only=True,
        required=True,
        style={"input_type": "password", "placeholder": "Password"},
    )

    class Meta:
        model = User
        fields = ("old_password", "new_password", "new_password_confirmation")

    def validate(self, attrs):
        if attrs["new_password"] != attrs["new_password_confirmation"]:
            raise serializers.ValidationError(
                {"password": "New passwords fields didn't match."}
            )
        return attrs

    def validate_old_password(self, value):
        user = self.context["request"].user
        if not user.check_password(value):
            raise serializers.ValidationError(
                {"old_password": "Old password is not correct"}
            )
        return value

    def update(self, instance, validated_data):
        instance.set_password(validated_data["new_password"])
        instance.save()
        return instance


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ("id", "name", "codename")


class OntologySerializer(serializers.ModelSerializer):
    class Meta:
        model = Ontology
        fields = ("id", "term_id", "description")


class PhenotypeCategorySerializer(serializers.ModelSerializer):
    ontologies = OntologySerializer(many=True, read_only=True)

    class Meta:
        model = PhenotypeCategory
        fields = (
            "id",
            "name",
            "description",
            "datatype",
            "dataclass",
            "nature",
            "location",
            "ontologies",
        )


class PhenotypeSerializer(serializers.ModelSerializer):
    categories = PhenotypeCategorySerializer(many=True, read_only=True)
    primitive_long = serializers.CharField(source="get_primitive_display")

    class Meta:
        model = Phenotype
        fields = ("id", "name", "primitive", "primitive_long", "categories")


class PhenotypeNoCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phenotype
        fields = ("id", "name", "primitive")


class ProjectExperimentSerializer(serializers.ModelSerializer):
    # parent_lookup_kwargs = {"project_pk": "project__pk"}
    phenotypes = PhenotypeSerializer(many=True, read_only=True)

    class Meta:
        model = Experiment
        fields = (
            # "url",
            "id",
            "name",
            "phenotypes"
            # "description",
            # "creation_date",
            # "cclines"
        )


# PhenotypeNoCategoriesSerializer


class ExperimentCclinePhenotypeValueSerializer(serializers.ModelSerializer):
    phenotype = PhenotypeSerializer(read_only=True)
    casted_value = serializers.SerializerMethodField()

    class Meta:
        model = PhenotypeValue
        fields = ("id", "casted_value", "phenotype")

    def get_casted_value(self, obj):
        casted_value = obj.phenotype.cast_to_python_primitive(obj.value)
        if type(casted_value) == float and math.isnan(casted_value):
            return None
        else:
            return casted_value


class ExperimentCclinePhenotypeValueToExportSerializer(
    ExperimentCclinePhenotypeValueSerializer
):
    phenotype = PhenotypeNoCategoriesSerializer(read_only=True)

    class Meta:
        model = PhenotypeValue
        fields = ("id", "value", "phenotype")


class NestedExperimentCclinePhenotypeValueSerializer(NestedHyperlinkedModelSerializer):
    parent_lookup_kwargs = {
        "experiment_cc_line_pk": "experiment_cc_line__pk",
        "project_pk": "experiment__project__pk",
    }
    phenotype = PhenotypeSerializer(read_only=True)
    casted_value = serializers.SerializerMethodField()

    class Meta:
        model = PhenotypeValue
        fields = ("id", "casted_value", "phenotype")

    def get_casted_value(self, obj):
        casted_value = obj.phenotype.cast_to_python_primitive(obj.value)
        if type(casted_value) == float and math.isnan(casted_value):
            return None
        else:
            return casted_value


class ExperimentExperimentCclineSerializer(serializers.ModelSerializer):
    # parent_lookup_kwargs = {
    #     "project_pk": "experiment__project__pk",
    #     "experiment_pk": "experiment__pk",
    # }

    class Meta:
        model = ExperimentCcline
        fields = (
            # "url",
            "id",
            "cc_line_id",
            "cc_line_m",
            "cc_line_f",
            "sex",
        )


class ProjectAnalysisSerializer(serializers.ModelSerializer):
    # parent_lookup_kwargs = {
    #     "project_pk": "project__pk",
    # }

    class Meta:
        model = Analysis
        fields = (
            "id",
            "name",
        )


class CclineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ccline
        fields = ("id", "id_cc", "funnel_code")


class NestedExperimentCclineSerializer(serializers.ModelSerializer):
    # parent_lookup_kwargs = {
    #     "project_pk": "experiment__project__pk",
    #     "experiment_pk": "experiment__pk",
    # }

    phenotype_values = NestedExperimentCclinePhenotypeValueSerializer(
        many=True,
        read_only=True,
    )
    cc_line_m = CclineSerializer(read_only=True)
    cc_line_f = CclineSerializer(read_only=True)

    class Meta:
        model = ExperimentCcline
        fields = (
            "cc_line_id",
            "cc_line_m",
            "cc_line_f",
            "sex",
            "phenotype_values",
        )


class ExperimentCclineSerializer(serializers.ModelSerializer):
    phenotype_values = ExperimentCclinePhenotypeValueSerializer(
        many=True,
        read_only=True,
    )
    cc_line_m = CclineSerializer(read_only=True)
    cc_line_f = CclineSerializer(read_only=True)

    class Meta:
        model = ExperimentCcline
        fields = (
            "cc_line_id",
            "cc_line_m",
            "cc_line_f",
            "sex",
            "phenotype_values",
        )


class ExperimentCclineToExportSerializer(ExperimentCclineSerializer):
    phenotype_values = ExperimentCclinePhenotypeValueToExportSerializer(
        many=True,
        read_only=True,
    )


class FounderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Founder
        fields = ("name", "short_name", "cc_designation", "color")


class ProjectRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectRole
        fields = ("id", "name", "priority")
        read_only_fields = ["id", "priority"]


class ProjectGroupMemberSerializer(serializers.ModelSerializer):
    project_role = ProjectRoleSerializer()

    class Meta:
        model = ProjectGroup
        fields = ("project_role", "project")


class ProjectGroupSerializer(NestedHyperlinkedModelSerializer):
    parent_lookup_kwargs = {
        "project_pk": "project__pk",
    }
    project_id = serializers.PrimaryKeyRelatedField(read_only=True)
    project_role = ProjectRoleSerializer(read_only=True)
    user_set = UserSerializer(many=True)

    class Meta:
        model = ProjectGroup
        fields = ("id", "project", "project_id", "name", "user_set", "project_role")
        read_only_fields = ["project"]


class TeamSerializer(serializers.ModelSerializer):
    project_role = ProjectRoleSerializer()

    class Meta:
        model = Team
        fields = ("id", "project_role", "name", "projects")
        read_only_fields = ["id", "priority"]


class ProjectMemberGroupsSerializer(serializers.ModelSerializer):
    projectgroup = ProjectGroupMemberSerializer()
    team = TeamSerializer()

    class Meta:
        model = Group
        fields = ("projectgroup", "team")


class ProjectMemberSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    username = serializers.CharField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    email = serializers.EmailField(read_only=True)

    groups = ProjectMemberGroupsSerializer(many=True)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        roles = []
        for group in data["groups"]:
            for k, v in group.items():
                if v is not None:
                    if k == "projectgroup":
                        if v["project"] == self.context["project_pk"]:
                            roles.append(v["project_role"])
                    if k == "team":
                        if self.context["project_pk"] in v["projects"]:
                            roles.append(v["project_role"])
        role = roles[0]
        for r in roles:
            if r["priority"] > role["priority"]:
                role = r
        data["role"] = role
        data.pop("groups", None)
        return data


class ProjectSerializer(serializers.ModelSerializer):
    project_manager = "manager"
    user_role = "manager"
    perm_name = "project"

    def create(self, validated_data, **kwargs):
        current_user = self.context["request"].user
        try:
            project = Project.objects.create(**validated_data, creator=current_user)
        except IntegrityError as e:
            msg = e.args[0]
            if "" in msg:
                raise serializers.ValidationError(
                    f"The project name \"{validated_data['project_name']}\" already exists. "
                    + "Please choose another one."
                )
            else:
                raise serializers.ValidationError(e)
        else:  # Add team permissions
            teams = Team.objects.filter(user__id=current_user.id)
            for team in teams:
                permissions = team.project_role.permissions.filter(
                    content_type__model="project"
                )
                team.projects.add(project)
                for project_perm in permissions:
                    assign_perm(project_perm, team, project)

            return project

    def update(self, instance, validated_data):
        instance.project_name = validated_data.get(
            "project_name", instance.project_name
        )
        instance.description = validated_data.get("description", instance.description)
        try:
            instance.save()
            # Update project groups name
            project_groups = ProjectGroup.objects.filter(project=instance)
            for project_group in project_groups:
                project_group.name = (
                    instance.project_name + "_" + str(project_group.project_role)
                )
                project_group.save()
            return instance
        except IntegrityError as e:
            msg = e.args[0]
            if "" in msg:
                raise serializers.ValidationError(
                    f"The project name \"{validated_data['project_name']}\" already exists. "
                    + "Please choose another one."
                )
            else:
                raise serializers.ValidationError(e)

    @classmethod
    def assign_user_role_permissions(cls, user, role, project):
        for perm in role.permissions.filter(content_type__model__exact="project"):
            assign_perm(perm, user, project)

    experiments = ProjectExperimentSerializer(many=True, read_only=True)
    analysis = ProjectAnalysisSerializer(many=True, read_only=True)
    teams = TeamSerializer(many=True, read_only=True)
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = Project
        fields = (
            "id",
            "project_name",
            "description",
            "creation_date",
            "experiments",
            "analysis",
            "teams",
            "permissions",
        )

    def get_permissions(self, obj):
        current_user = self.context["request"].user
        return get_perms(current_user, obj)


class ProjectPhenotypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = (
            "id",
            "project_name",
            # "description",
            # "creation_date",
            # "cclines"
        )
        read_only_fields = ["project_name"]


class ProjectGroupRoleSerializer(serializers.ModelSerializer):
    project = serializers.StringRelatedField(read_only=True)
    project_role = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = ProjectGroup
        fields = ("id", "project", "project_role")


class ExperimentLightSerializer(NestedHyperlinkedModelSerializer):
    parent_lookup_kwargs = {"project_pk": "project__pk"}
    project = ProjectSerializer(read_only=True)
    phenotypes = PhenotypeSerializer(many=True, read_only=True)

    class Meta:
        model = Experiment
        fields = (
            "id",
            "name",
            "description",
            "creation_date",
            "project",
            "phenotypes",
        )


class PhenotypeValueSerializer(NestedHyperlinkedModelSerializer):
    parent_lookup_kwargs = {
        "project_pk": "experiment_cc_line__experiment__project__pk",
        # "experiment_pk": "experiment_cc_line__experiment__pk",
        "experiment_cc_line_pk": "experiment_cc_line__pk",
    }
    phenotype = PhenotypeSerializer(read_only=True)

    class Meta:
        model = PhenotypeValue
        fields = ("id", "value", "phenotype")


class RoleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Role
        fields = ("id", "name", "priority")

    @staticmethod
    def getPermissionsCodename(role_name, content_type):
        role = Role.objects.get(name__exact=role_name)
        return (
            perm.codename
            for perm in role.permissions.filter(content_type__model__exact=content_type)
        )


class RoleGroupSerializer(serializers.ModelSerializer):
    group_ptr = GroupSerializer(read_only=True)
    role = RoleSerializer(read_only=True)

    class Meta:
        model = RoleGroup
        fields = ("id", "role", "group_ptr")


class AnalysisPeakSerializer(serializers.ModelSerializer):
    # parent_lookup_kwargs = {
    #     "project_pk": "analysis__project__pk",
    #     "analysis_pk": "analysis__pk",
    # }

    class Meta:
        model = Peak
        fields = (
            # "url",
            "id",
            "variable_name",
            "chromosome",
            "position",
            "p_value",
        )


class GalaxyWorkflowSerializer(serializers.ModelSerializer):
    class Meta:
        model = GalaxyWorkflow
        fields = ("id", "name", "analysis_category", "version", "details")


class GalaxyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = GalaxyUser
        fields = ("id", "username", "galaxy_instance")


class GalaxyHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = GalaxyHistory
        fields = ("id",)


class AnalysisSerializer(serializers.ModelSerializer):
    peaks = AnalysisPeakSerializer(
        many=True,
        read_only=True,
    )
    experiments = ExperimentLightSerializer(many=True, read_only=True)
    experiment_ids = serializers.PrimaryKeyRelatedField(
        source="experiments",
        queryset=Experiment.objects.all(),
        many=True,
        write_only=True,
    )
    project = ProjectSerializer(read_only=True)
    phenotypes = PhenotypeSerializer(many=True, read_only=True)
    phenotype_ids = serializers.PrimaryKeyRelatedField(
        source="phenotypes",
        queryset=Phenotype.objects.all(),
        many=True,
        write_only=True,
    )
    covariates = PhenotypeSerializer(many=True, read_only=True)
    # galaxy_workflow = GalaxyWorkflowSerializer()
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = Analysis
        fields = (
            "id",
            "name",
            "description",
            "galaxy_history",
            "galaxy_workflow",
            "creation_date",
            "peaks",
            "experiments",
            "experiment_ids",
            "project",
            "history_status",
            "galaxy_state",
            "status",
            "percentage_done",
            "phenotypes",
            "phenotype_ids",
            "covariates",
            "permissions",
            "tools_params",
        )
        read_only_fields = ["galaxy_state"]

    def get_permissions(self, obj):
        current_user = self.context["request"].user
        return get_perms(current_user, obj)

    def create(self, validated_data):
        project = Project.objects.get(pk=self.context["view"].kwargs["project_pk"])
        validated_data["project"] = project
        tools_params = validated_data["tools_params"]
        experiments_db = validated_data.pop("experiments")
        phenotypes_db = validated_data.pop("phenotypes")
        add_covar_name = tools_params["3"]["add_covar"].split(",")
        add_covar_db = Phenotype.objects.filter(name__in=add_covar_name)
        analysis = Analysis(**validated_data)
        analysis.save()

        analysis.experiments.set(experiments_db)
        analysis.phenotypes.set(phenotypes_db)
        analysis.covariates.set(add_covar_db)
        analysis.run_analysis()

        return analysis


class AnalysisModelSerializer(serializers.ModelSerializer):
    project = ProjectPhenotypeSerializer()

    class Meta:
        model = Analysis
        fields = (
            "id",
            "name",
            "description",
            "galaxy_history",
            "galaxy_workflow",
            # "galaxy_workflow_invocation_id",
            "creation_date",
            "project",
        )


class AnalysisReadOnlySerializer(serializers.ModelSerializer):
    project = ProjectPhenotypeSerializer(read_only=True)

    class Meta:
        model = Analysis
        fields = (
            "id",
            "name",
            "description",
            "galaxy_history",
            "galaxy_workflow",
            # "galaxy_workflow_invocation_id",
            "creation_date",
            "project",
        )
        read_only_fields = [
            "id",
            "name",
            "description",
            "galaxy_history",
            "galaxy_workflow_id",
            # "galaxy_workflow_invocation_id",
            "creation_date",
            "project",
        ]


class ExperimentSerializer(serializers.ModelSerializer):
    cclines = ExperimentExperimentCclineSerializer(
        many=True,
        read_only=True,
    )
    analysis_set = AnalysisSerializer(many=True, read_only=True)
    phenotypes = PhenotypeSerializer(many=True, read_only=True)
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = Experiment
        fields = (
            "id",
            "name",
            "project",
            "description",
            "creation_date",
            "cclines",
            "analysis_set",
            "phenotypes",
            "permissions",
        )

    def get_permissions(self, obj):
        current_user = self.context["request"].user
        return get_perms(current_user, obj)


class ExperimentMinimalSerializer(serializers.ModelSerializer):
    cclines_count = serializers.IntegerField(source="cclines.count", read_only=True)
    project = ProjectPhenotypeSerializer()
    phenotypes = PhenotypeSerializer(many=True, read_only=True)

    class Meta:
        model = Experiment
        fields = (
            "id",
            "name",
            "project",
            "description",
            "creation_date",
            "cclines_count",
            "phenotypes",
        )


class PhenotypePatchSerializer(serializers.ModelSerializer):
    categories = serializers.PrimaryKeyRelatedField(
        many=True, queryset=PhenotypeCategory.objects.all()
    )

    class Meta:
        model = Phenotype
        fields = (
            "id",
            "name",
            "categories",
        )


class PhenotypeDetailsSerializer(serializers.ModelSerializer):
    categories = PhenotypeCategorySerializer(many=True, read_only=True)
    primitive_long = serializers.CharField(source="get_primitive_display")
    # experiment_set = ExperimentMinimalSerializer(many=True, read_only=True)
    experiment_set = serializers.SerializerMethodField()
    # analysis_set = AnalysisModelSerializer(many=True, read_only=True)
    analysis_set = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = Phenotype
        fields = (
            "id",
            "name",
            "primitive",
            "primitive_long",
            "categories",
            "experiment_set",
            "analysis_set",
            "permissions",
        )
        read_only_fields = ["primitive"]

    def filter_on_permissions(self, qs, permission, serializer):
        current_user = self.context["request"].user
        id_to_keep = set()
        for exp in qs:
            permissions = get_perms(current_user, exp)
            permissions_set = set(permissions)
            if permission in permissions_set:
                id_to_keep.add(exp.id)
        serializer = serializer(
            instance=qs.filter(id__in=list(id_to_keep)),
            many=True,
            context=self.context,
        )
        return serializer.data

    # only display experiment the user have permissions
    def get_experiment_set(self, obj):
        qs = Experiment.objects.all().filter(phenotypes=obj)
        return self.filter_on_permissions(
            qs, "view_experiment", ExperimentMinimalSerializer
        )

    # only display analysis the user have permissions
    def get_analysis_set(self, obj):
        qs = Analysis.objects.all().filter(phenotypes=obj)
        return self.filter_on_permissions(
            qs, "view_analysis", AnalysisReadOnlySerializer
        )

    def get_permissions(self, obj):
        current_user = self.context["request"].user
        permissions_set = set(
            [
                perm
                for perm in PERMISSION_NAMES
                if current_user.has_perm(f"api.{perm}_phenotype")
            ]
        )

        return permissions_set


class LodScoreSerializerList(serializers.ModelSerializer):
    class Meta:
        model = LodScore
        fields = (
            "id",
            # "lod_scores",
            "significance_threshold",
        )


class LodScoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = LodScore
        fields = (
            "id",
            "lod_scores",
            "significance_threshold",
        )


class CoefficientListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coefficient
        fields = ("id",)


class CoefficientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coefficient
        fields = ("id", "coefficients")


class HaplotypeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Haplotype
        fields = ("id",)


class HaplotypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Haplotype
        fields = ("id", "haplotypes")


class SnpsAssociationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SnpsAssociation
        fields = ("id",)


class SnpsAssociationSerializer(serializers.ModelSerializer):
    class Meta:
        model = SnpsAssociation
        fields = ("id", "snps_associations")


class TopSnpsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopSnp
        fields = ("id",)


class TopSnpsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopSnp
        fields = ("id", "top_snps")


class PeakGenesListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeakGene
        fields = ("id",)


class PeakGenesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeakGene
        fields = ("id", "genes")


class PeakSerializer(serializers.ModelSerializer):
    parent_lookup_kwargs = {
        "project_pk": "analysis__project__pk",
        "analysis_pk": "phenotype__analysis__pk",
        "phenotype_pk": "phenotype__pk",
        "peak_pk": "peak__pk",
    }

    class Meta:
        model = Peak
        fields = (
            "id",
            "variable_name",
            "chromosome",
            "position",
            "lod",
            "ci_lo",
            "ci_hi",
            "p_value",
        )


class PeakSerializerWithPheno(serializers.ModelSerializer):
    class Meta:
        model = Peak
        fields = (
            "id",
            "variable_name",
            "chromosome",
            "position",
            "lod",
            "ci_lo",
            "ci_hi",
            "p_value",
            "phenotype",
        )
