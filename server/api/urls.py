from django.urls import path, include

# from rest_framework import routers

from rest_framework_nested import routers
from .views import (
    AnalysisViewSet,
    GalaxyUserViewSet,
    GalaxyWorkflowViewSet,
    ExperimentViewSet,
    FounderViewSet,
    OntologyViewSet,
    # LodScoreViewSet,
    PeakViewSet,
    PhenotypeDetailsViewSet,
    ProjectAnalysisPhenotypeLodScoreViewSet,
    ProjectAnalysisPhenotypePeakCoefficientViewSet,
    ProjectAnalysisPhenotypePeakGenesViewSet,
    ProjectAnalysisPhenotypePeakHaplotypeViewSet,
    ProjectAnalysisPhenotypePeakSnpsAssociationViewSet,
    ProjectAnalysisPhenotypePeakTopSnpsViewSet,
    ProjectAnalysisPhenotypePeakViewSet,
    ProjectAnalysisPhenotypeViewSet,
    ProjectGroupRoleViewSet,
    ProjectGroupViewSet,
    ProjectMemberViewSet,
    RoleGroupViewSet,
    # PhenotypeCategoryResolveViewSet,
    # ToolsParameters,
    # tools_parameters,
    UserViewSet,
    CurrentUserViewSet,
    TeamViewSet,
    ProjectViewSet,
    RoleViewSet,
    ProjectRoleViewSet,
    ProjectExperimentViewSet,
    PhenotypeCategoryViewSet,
    ExperimentCclineViewSet,
    CclineViewSet,
    ProjectAnalysisViewSet,
    WorkflowDefaultParameters,
    WorkflowTools,
    CcQtlWorkflows,
)


router = routers.DefaultRouter()
router.register(r"users", UserViewSet)
router.register(r"app-roles", RoleGroupViewSet)
router.register(r"current-user", CurrentUserViewSet, basename="current_user")
router.register(r"ccqtl-galaxy-workflow", GalaxyWorkflowViewSet)

router.register(r"roles", RoleViewSet)
router.register(r"project-roles", ProjectRoleViewSet)
router.register(r"project-groups", ProjectGroupRoleViewSet, basename="project_groups")
router.register(r"teams", TeamViewSet)
router.register(r"cclines", CclineViewSet)
router.register(r"founders", FounderViewSet)
# router.register(r"tools-parameters", ToolsParameters, basename="tools-parameters")
router.register(r"analysis", AnalysisViewSet)
router.register(r"experiments", ExperimentViewSet)
router.register(r"ontologies", OntologyViewSet)
router.register(r"phenotypes", PhenotypeDetailsViewSet)
router.register(r"phenotype-categories", PhenotypeCategoryViewSet)
router.register(r"galaxy-users", GalaxyUserViewSet)
# router.register(
#     r"phenotype-categories-resolve",
#     PhenotypeCategoryResolveViewSet,
#     basename="phenotype-categories-resolve",
# )


############
# Projects #
############
router.register(r"projects", ProjectViewSet)
project_router = routers.NestedSimpleRouter(router, r"projects", lookup="project")
project_router.register(r"experiments", ProjectExperimentViewSet, basename="experiment")
project_router.register(r"analysis", ProjectAnalysisViewSet, basename="analysis")
project_router.register(r"members", ProjectMemberViewSet, basename="projectmembers")
project_router.register(r"groups", ProjectGroupViewSet, basename="projectgroup")
###############
# Experiment
experiment_router = routers.NestedDefaultRouter(
    project_router, r"experiments", lookup="experiment"
)
experiment_router.register(r"cclines", ExperimentCclineViewSet)

####################
# Analysis
analysis_router = routers.NestedDefaultRouter(
    project_router, r"analysis", lookup="analysis"
)

analysis_router.register(
    r"phenotypes", ProjectAnalysisPhenotypeViewSet, basename="phenotype"
)

# Phenotypes router
phenotype_router = routers.NestedDefaultRouter(
    analysis_router, r"phenotypes", lookup="phenotype"
)

phenotype_router.register(
    r"lodscores", ProjectAnalysisPhenotypeLodScoreViewSet, basename="lodscore"
)
phenotype_router.register(
    r"peaks", ProjectAnalysisPhenotypePeakViewSet, basename="peak"
)

# Peaks router
peak_router = routers.NestedDefaultRouter(phenotype_router, r"peaks", lookup="peak")

peak_router.register(
    r"coefficients",
    ProjectAnalysisPhenotypePeakCoefficientViewSet,
    basename="coefficient",
)

peak_router.register(
    r"haplotypes",
    ProjectAnalysisPhenotypePeakHaplotypeViewSet,
    basename="haplotype",
)

peak_router.register(
    r"snps_association",
    ProjectAnalysisPhenotypePeakSnpsAssociationViewSet,
    basename="snps_association",
)

# top_snps
peak_router.register(
    r"top_snps",
    ProjectAnalysisPhenotypePeakTopSnpsViewSet,
    basename="top_snp",
)


# genes
peak_router.register(
    r"genes",
    ProjectAnalysisPhenotypePeakGenesViewSet,
    basename="gene",
)

analysis_router.register(r"peaks", PeakViewSet)
urlpatterns = [
    path("accounts/", include("django.contrib.auth.urls")),
    # path(
    #     "current-galaxy-workflow/",
    #     CurrentGalaxyWorkflowAPIView.as_view(),
    #     name="current-galaxy-workflow",
    # ),
    path("", include(router.urls)),
    path("", include(project_router.urls)),
    path("", include(experiment_router.urls)),
    path("", include(analysis_router.urls)),
    path("", include(phenotype_router.urls)),
    path("", include(peak_router.urls)),
    path(
        "workflow-default-parameters/<slug:wf_id>/",
        WorkflowDefaultParameters.as_view(),
        name="workflow-default-parameters",
    ),
    path(
        "workflow-tools/<slug:wf_id>/",
        WorkflowTools.as_view(),
        name="worklow-tools",
    ),
    path("cc-qtl-workflows/", CcQtlWorkflows.as_view(), name="cc-qtl-workflows"),
]
