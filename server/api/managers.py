from django.db import models


class DepartmentManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class ProjectRoleManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)

    def clear_permissions(self):
        for project_role in self.all():
            project_role.permissions.clear()


class RoleManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)

    def clear_permissions(self):
        for role in self.all():
            role.permissions.clear()


class MarkerManager(models.Manager):
    def get_by_natural_key(self, id_marker):
        return self.get(id_marker=id_marker)


class FounderManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class CCLineManager(models.Manager):
    def get_by_natural_key(self, id_cc):
        return self.get(id_cc=id_cc)


class OntologyManager(models.Manager):
    def get_by_natural_key(self, term_id):
        return self.get(term_id=term_id)


class PhenotypeCategoryManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class RoleGroupManager(models.Manager):
    def clear_permissions(self):
        for role_group in self.all():
            # clear the permissions
            role_group.permissions.clear()

    def add_permissions(self):
        for role_group in self.all():
            # Add permissions from role
            for perm in role_group.role.permissions.all():
                role_group.permissions.add(perm)

    def update_permissions(self):
        # Clear the permissions
        self.clear_permissions()
        self.add_permissions()


class GalaxyInstanceManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class GalaxyUserManager(models.Manager):
    def get_by_natural_key(self, username, galaxy_instance):
        return self.get(username=username, galaxy_instance=galaxy_instance)


class AnalysisCategoryManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class GalaxyWorkflowManager(models.Manager):
    def get_by_natural_key(self, name, galaxy_user, version):
        return self.get(name=name, version=version, galaxy_user=galaxy_user)


class ExperimentManager(models.Manager):
    def get_by_natural_key(self, name, project):
        return self.get(name=name, project=project)


class AnalysisManager(models.Manager):
    def get_by_natural_key(self, galaxy_workflow, galaxy_history):
        return self.get(galaxy_workflow=galaxy_workflow, galaxy_history=galaxy_history)
