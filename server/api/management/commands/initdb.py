from django.core.management.base import BaseCommand
from django.core.management import call_command

from api.models import Role, ProjectRole, RoleGroup


class Command(BaseCommand):
    help = "Fill the database with default data"

    def handle(self, *args, **options):
        # call_command

        # load the permissions
        call_command("loaddata", "api/fixtures/roles", verbosity=2)
        call_command("loaddata", "api/fixtures/groups", verbosity=2)
        # Clear permissions for role
        Role.objects.clear_permissions()

        # Clear permissions for project role
        ProjectRole.objects.clear_permissions()

        call_command("loaddata", "api/fixtures/role-permissions", verbosity=2)

        call_command(
            "loaddata", "api/fixtures/project-maintainer-permissions", verbosity=2
        )
        call_command("loaddata", "api/fixtures/project-reader-permissions", verbosity=2)
        call_command(
            "loaddata", "api/fixtures/project-manager-permissions", verbosity=2
        )

        call_command("loaddata", "api/fixtures/founders", verbosity=2)

        # load ccline
        call_command("loaddata", "api/fixtures/ccline", verbosity=2)

        # Load variable-type
        # call_command("loaddata", "api/fixtures/variable-data-type", verbosity=2)

        # load markers
        # call_command("loaddata", "api/fixtures/markers", verbosity=2)
        call_command("loaddata", "api/fixtures/analysis-category", verbosity=2)

        # call_command("loaddata", "api/fixtures/galaxy", verbosity=2)
        call_command("loaddata", "api/fixtures/ontology", verbosity=2)
        RoleGroup.objects.update_permissions()
