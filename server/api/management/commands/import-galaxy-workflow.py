from django.core.management.base import BaseCommand
from django.conf import settings


from api.models import AnalysisCategory, GalaxyInstance, GalaxyUser, GalaxyWorkflow


class Command(BaseCommand):
    help = "Init galaxy user and workflow"

    def handle(self, *args, **options):
        # call_command

        gi, _ = GalaxyInstance.objects.get_or_create(
            name=settings.GALAXY_NAME,
            url=settings.GALAXY_BASE_URL,
            description=settings.GALAXY_DESCRIPTION,
        )
        gu, _ = GalaxyUser.objects.get_or_create(
            username=settings.GALAXY_USERNAME,
            api_key=settings.GALAXY_API_KEY,
            galaxy_instance=gi,
        )
        ac = AnalysisCategory.objects.get(name="ccqtl")
        gw, created = GalaxyWorkflow.objects.get_or_create(
            name=settings.GALAXY_WF_NAME,
            version=settings.GALAXY_WF_VERSION,
            galaxy_user=gu,
            analysis_category=ac,
        )

        # check if workflow exists
        workflows = gu.bioblend_gi.workflows.get_workflows(name=gw.name)
        wf_tags_set_list = [set(wf["tags"]) for wf in workflows]
        wf_tags_set = set().union(*wf_tags_set_list)
        if f"v{gw.version}" not in wf_tags_set:
            print(f"import workflow to galaxy instance: {gi.name} -> {gi.url}")
            gu.bioblend_gi.workflows.import_workflow_from_local_path(
                f"/app/api/galaxy-workflow/{gw.name}_{gw.version}.ga"
            )
        else:
            print(
                f"the workflow already available in galaxy instance: {gi.name} -> {gi.url}"
            )
