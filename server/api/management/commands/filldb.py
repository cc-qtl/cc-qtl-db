from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from api.factories import (
    SuperuserFactory,
    USER_PASSWORD,
    ProjectFactory,
    UserStaffFactory,
)
from rest_framework.test import APIClient

from api.tests.utils import authenticate

from api.models import Project, ProjectGroup, ProjectRole

User = get_user_model()


class Command(BaseCommand):
    help = "Fill the database with default data"

    def handle(self, *args, **options):
        # load the permissions

        # return

        client = APIClient()
        superuser = SuperuserFactory(username="admin")
        superuser.set_password(USER_PASSWORD)
        # delete all projects
        for project in Project.objects.all():
            authenticate(client, superuser, USER_PASSWORD)
            client.delete("/api/projects/" + str(project.id) + "/")

        # previous_user = None

        projects = ProjectFactory.build_batch(4)
        # create roles
        for i, user_staff in enumerate(UserStaffFactory.build_batch(4)):
            project = projects[i]
            username = f"user-{i}"
            print(
                f"{ user_staff.first_name} {user_staff.last_name}"
                + f" project manager of '{project}'"
            )
            # project_name = username + "-project"
            authenticate(client, superuser, USER_PASSWORD)
            users_qs = User.objects.filter(username__exact=username)
            for user in users_qs:
                authenticate(client, superuser, USER_PASSWORD)
                client.delete("/api/users/" + str(user.id) + "/")

            response = client.post(
                "/api/users/",
                data={
                    "username": username,
                    "first_name": user_staff.first_name,
                    "last_name": user_staff.last_name,
                    "is_staff": True,
                    "is_active": True,
                    "email": user_staff.email,
                    "password": USER_PASSWORD,
                },
            )
            user = response.data
            user = User.objects.get(pk=user["id"])
            # previous_user = user
            user.set_password(USER_PASSWORD)
            user.save()

            # add user to a team
            # if i < 2:
            #     team = Team.objects.get(pk=team1.id)
            #     team.user_set.add(user)
            # else:
            #     team = Team.objects.get(pk=team2.id)
            #     team.user_set.add(user)

            # delete project if exists

            # create a project for each user
            authenticate(client, user, USER_PASSWORD)
            client.post(
                "/api/projects/",
                data={
                    "project_name": project.project_name,
                    "description": project.description,
                    "creation_date": project.creation_date,
                },
            )

            # add reader to project
            if i % 2 == 1:
                index = i - 1
                project_db = Project.objects.get(
                    project_name__exact=projects[index].project_name
                )
                project_role_reader = ProjectRole.objects.get(name="reader")
                project_reader_group = ProjectGroup.objects.get(
                    project=project_db, project_role=project_role_reader
                )
                authenticate(client, superuser, USER_PASSWORD)
                users_to_add = [user.id]
                client.patch(
                    f"/api/projects/{project_db.id}/groups/{project_reader_group.id}/",
                    {"users": users_to_add},
                )
                print(
                    f"{user.first_name} {user.last_name}"
                    + f" is member of {project_reader_group} for project {project_db}"
                )
