import unidecode
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from api.factories import (
    USER_PASSWORD,
    UserFactory,
)
from rest_framework.test import APIClient

from api.tests.utils import authenticate
from api.models import RoleGroup

# from api.models import Project


User = get_user_model()


class Command(BaseCommand):
    help = "create useraccount for workshop user"

    def handle(self, *args, **options):
        users = [
            {"fn": "Xavier", "ln": "Montagutelli"},
            {"fn": "Axelle", "ln": "Brulport"},
            {"fn": "Christian", "ln": "Vosshenrich"},
            {"fn": "Etienne", "ln": "Simon-Loriere"},
            {"fn": "Franck", "ln": "Bourgade"},
            {"fn": "Françoise", "ln": "Dromer"},
            {"fn": "Gérard", "ln": "Eberl"},
            {"fn": "James", "ln": "Di Santo"},
            {"fn": "Jean", "ln": "Jaubert"},
            {"fn": "Lucie", "ln": "Dupuis"},
            {"fn": "Marie", "ln": "Bourdon"},
            {"fn": "Marion", "ln": "Rincel"},
            {"fn": "Pascal", "ln": "Campagne"},
            {"fn": "Rachel", "ln": "Torchet"},
            {"fn": "Sylvie", "ln": "Van Der Werf"},
            {"fn": "Thomas", "ln": "Bourgeron"},
            {"fn": "Victoire", "ln": "Baillet"},
        ]
        users_with_username = [
            UserFactory.build(
                username=u["username"], first_name=u["fn"], last_name=u["ln"]
            )
            for u in [
                dict(
                    u,
                    username=f"{unidecode.unidecode(u['fn'][0].lower())}"
                    + f"{unidecode.unidecode(u['ln'].lower().replace(' ', ''))}",
                )
                for u in users
            ]
        ]
        client = APIClient()
        superuser = User.objects.get(username="admin")
        authenticate(client, superuser, USER_PASSWORD)
        for build_user in users_with_username:
            db_user = User.objects.filter(username=build_user.username)
            appGroup = RoleGroup.objects.get(name="app-manager")
            # db_user.set_password(user.username)
            # db_user.save()
            if not db_user.exists():
                response = client.post(
                    "/api/users/",
                    data={
                        "username": build_user.username,
                        "first_name": build_user.first_name,
                        "last_name": build_user.last_name,
                        "is_staff": build_user.is_staff,
                        "is_active": build_user.is_active,
                        "email": build_user.email,
                        "password": USER_PASSWORD,
                        "appGroup": {"id": appGroup.id, "name": appGroup.name},
                    },
                    format="json",
                )
                user = response.data
                user = User.objects.get(pk=user["id"])
                user.set_password(build_user.username)
                user.save()
                print(f"User {build_user.username} has been created")
            else:
                print(f"User {build_user.username} already exists")
