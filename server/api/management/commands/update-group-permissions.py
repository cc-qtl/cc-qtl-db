from django.core.management.base import BaseCommand
from django.core.management import call_command

from api.models import Project, ProjectGroup, Role, RoleGroup


class Command(BaseCommand):
    help = "Fill the database with default data"

    def handle(self, *args, **options):
        print("test")

        # this will remove all role permissions
        # and load the new ones
        call_command("initdb", verbosity=2)

        # get all the Role Group and update the permissions
        # RoleGroup.objects.update_permissions()
        # for pg in ProjectGroup.objects.all():
        #     pg.clear_permissions()
        #     pg.add_permissions()
        # # if remove, remove function as well
        # ProjectGroup.objects.update_projectgroup_permissions()
        Project.objects.update_permissions()
