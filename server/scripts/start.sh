#!/bin/bash

if [ -z $PORT ]; then PORT=8000; fi # Need to fix to get value from .env file

python manage.py collectstatic --no-input
python manage.py makemigrations
python manage.py migrate --no-input
python manage.py initdb
# python manage.py filldb
#python manage.py runserver 0.0.0.0:${PORT}
gunicorn --bind "0.0.0.0:${PORT}" server.wsgi:application

# daphne -p ${PORT} -b 0.0.0.0 server.asgi:application
