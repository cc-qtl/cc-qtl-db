import json
import pandas as pd

df = pd.read_csv('../../../data/ontology_mappingsInMPD_ALL.csv')
model='api.ontology'

def row_to_entry(row):
    return {
        "model": model,
            "fields": {
                "term_id" : row["term_id"],
                "description": row["description"]
            }
        }

fixture_dict = [ row_to_entry(row) for index, row in df.iterrows()]

fixture_json = json.dumps(fixture_dict, indent = 2)
with open('../../../data/ontology_mappingsInMPD_ALL.json', 'w') as f:
    f.write(fixture_json)